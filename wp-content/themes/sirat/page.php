/*
Theme Name: Sirat
Theme URI: https://www.vwthemes.com/themes/free-multipurpose-wordpress-theme/
Author: VW Themes
Author URI: https://www.vwthemes.com/
Description: Sirat is a multipurpose theme known primarily for its simplicity apart from being clean, user-friendly as well as finely organised making it a very good choice when it comes to WordPress themes relevant for various business purposes like a blog, portfolio, business website and WooCommerce storefront with a beautiful & professional design. The theme comes with a new header layout and there are more options of sidebar layout available for post and pages. Apart from that, there are more width options as well as preload options available. Another important thing with this particular WordPress theme is that it is lightweight and can be extended to the limit one likes. It also has the availability of the enable and disable button. With some of its exemplary features like WooCommerce, retina ready, clean code, CTA, bootstrap framework, customization options, mobile friendliness etc, This WP theme has content limit option and is also accompanied with blog post settings.  Another important characteristic with it is the beauty it has and also the design that is immensely professional. Sirat is a fast theme and is accompanied with some of the best SEO practices in the online world. You have the option of customization a mentioned earlier making it extremely beneficial for personal portfolio. Apart from all this, Sirat is modern, luxurious, stunning as well as animated making it a fine option for the businesses where creativity is involved and it also has scroll top layout. Siart is translation ready and supports AR_ARABIC, DE_GERMAN, ES_SPANISH, FR_FRENCH, IT_ITALIAN, RU_RUSSIAN, ZH_CHINESE, TR_TURKISH languages. It fits creative business, small businesses restaurants, medical shops, startups, corporate businesses, online agencies and firms, portfolios, ecommerce. Sirat has banner opacity and colour changing feature included. Demo: https://www.vwthemes.net/vw-sirat/
Version:  0.4.8
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl.html
Text Domain: sirat
Tags: left-sidebar, right-sidebar, one-column, two-columns, three-columns, four-columns, grid-layout, custom-colors, custom-background, custom-logo, custom-menu, custom-header, editor-style, featured-images, footer-widgets, sticky-post, full-width-template, theme-options, post-formats, translation-ready, threaded-comments, rtl-language-support, blog, portfolio, e-commerce

Sirat WordPress Theme has been created by VW Themes(vwthemes.com), 2019.
Sirat WordPress Theme is released under the terms of GNU GPL
*/

/* Basic Style */

* {
    margin: 0;
    padding: 0;
    outline: none;
}

body {
    margin: 0;
    padding: 0;
    -ms-word-wrap: break-word;
    word-wrap: break-word;
    position: relative;
    background: #fff;
    font-size: 14px;
    color: #000000;
    font-family: 'Heebo', sans-serif;
}

img {
    margin: 0;
    padding: 0;
    border: none;
    max-width: 100%;
    height: auto;
}

section img {
    max-width: 100%;
}

h1,
h2,
h3,
h4,
h5,
h6 {
    font-family: 'Saira', sans-serif;
    margin: 0;
    padding: 10px 0;
    color: #121212;
    font-weight: bold;
}

p {
    margin: 0 0 15px;
    padding: 0;
    letter-spacing: 1px;
}

a {
    color: #3c3568;
}

a:focus,
a:hover {
    text-decoration: none !important;
}

a:focus,
input[type="submit"]:focus {
    outline: 1px dotted #000;
    border-bottom: 1px solid #000;
    text-decoration: none !important;
}

#footer a:focus,
.main-menu-navigation .sub-menu>li>a:focus {
    outline: 1px dotted #fff;
    border-bottom: 1px solid #fff;
}

ul {
    margin: 0 0 0 15px;
    padding: 0;
}

code {
    color: #3c3568;
}

:hover {
    -webkit-transition-duration: 1s;
    -moz-transition-duration: 1s;
    -o-transition-duration: 1s;
    transition-duration: 1s;
}

.clear {
    clear: both;
}

input[type='submit']:hover {
    cursor: pointer;
}

.center {
    text-align: center;
    margin-bottom: 40px;
}

.middle-align {
    margin: 0 auto;
    padding: 3em 0 0;
}

.wp-caption {
    margin: 0;
    padding: 0;
    font-size: 13px;
    max-width: 100%;
}

.wp-caption-text {
    margin: 0;
    padding: 0;
}

/* Text meant only for screen readers. */
.screen-reader-text {
    border: 0;
    clip: rect(1px, 1px, 1px, 1px);
    clip-path: inset(50%);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
    word-wrap: normal !important;
}

.screen-reader-text:focus {
    background-color: #eee;
    clip: auto !important;
    clip-path: none;
    color: #444;
    display: block;
    font-size: 1em;
    height: auto;
    left: 5px;
    line-height: normal;
    padding: 15px 23px 14px;
    text-decoration: none;
    top: 5px;
    width: auto;
    z-index: 100000;
    /* Above WP toolbar. */
}

.sticky {
    margin: 0;
    padding: 0;
}

.sticky .post-main-box h3 {
    background: url(assets/images/pin.png) no-repeat scroll 0 0px;
    padding-left: 40px;
}

.gallery-caption {
    margin: 0;
    padding: 0;
}

.alignleft,
img.alignleft {
    display: inline;
    float: left;
    margin-right: 20px;
    margin-top: 4px;
    margin-bottom: 10px;
    padding: 0;
    border: 1px solid #bbb;
    padding: 5px;
}

.alignright,
img.alignright {
    display: inline;
    float: right;
    border: 1px solid #bbb;
    padding: 5px;
    margin-bottom: 10px;
    margin-left: 25px;
}

.aligncenter,
img.aligncenter {
    clear: both;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 0;
    border: solid 1px #bbb;
    padding: 5px;
}

.alignnone,
img.alignnone {
    border: solid 1px #bbb;
    padding: 5px;
}

.comment-list .comment-content ul {
    list-style: none;
    margin-left: 15px;
}

.comment-list .comment-content ul li {
    margin: 5px;
}

#respond {
    clear: both;
}

.toggleMenu {
    display: none;
}

.bypostauthor {
    margin: 0;
    padding: 0;
}

input[type="text"],
input[type="email"],
input[type="phno"],
input[type="password"],
textarea {
    border: 1px solid #bcbcbc;
    width: 100%;
    font-size: 16px;
    padding: 10px 10px;
    margin: 0 0 23px 0;
    height: auto;
}

textarea {
    height: 111px;
}

input[type="submit"] {
    text-align: center;
    text-transform: uppercase;
    font-size: 12px;
    padding: 18px 30px;
    background: #1c2242;
    font-weight: bold;
    color: #121212;
    letter-spacing: 1px;
    border: none;
}

/*Sticky Header*/
.header-fixed {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background: #fff;
    z-index: 999999;
}

/* --------TOP BAR --------- */
.top-bar h3.custom_title {
    display: none;
}

.top-bar .custom-social-icons {
    text-align: right;
    padding: 15px 0;
}

.top-bar .custom-social-icons i {
    color: #121212;
    margin-right: 10px;
}

.top-bar .custom-social-icons i:hover {
    color: #121212;
}

.top-bar {
    background: #1c2242;
}

.top-bar p {
    font-size: 14px;
    color: #121212;
    padding: 15px 0;
    margin-bottom: 0px;
}

.top-bar i {
    margin-right: 8px;
    color: #121212;
}

.middle-header {
    padding: 15px 0;
    border-bottom: solid 3px #1c2242;
}

/*.page-template-custom-home-page .middle-header {
  border: none;  
}*/

/* ------------ LOGO CSS ----------- */
.logo .site-title a {
    color: #121212;
    font-weight: bold;
}

.logo .site-title {
    font-size: 30px;
    padding: 0;
    letter-spacing: 1px;
    margin-bottom: 0;
}

p.site-description {
    font-weight: 500;
    letter-spacing: 1px;
    font-style: italic;
    font-size: 13px;
    margin-bottom: 0;
    color: #000000;
}

/*----------------- SEARCH POPUP ----------------*/
.serach_inner label {
    width: 80%;
}

.serach_outer {
    position: fixed;
    width: 100%;
    height: 100%;
    overflow: hidden;
    transition: 0.5s;
    z-index: 999;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.8);
    display: none;
}

.serach_inner {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
}

.closepop {
    position: absolute;
    top: 20%;
    transform: translateY(-50%);
    width: 100%;
    right: 0;
    text-align: right;
    margin-right: 28px;
}

.serach_outer i {
    font-size: 24px;
    padding: 4px 0 0 0;
    color: #bbb;
    cursor: pointer;
}

.serach_inner form.search-form {
    display: table;
    padding: 0;
    border-radius: 0;
    border-bottom: solid 1px #999;
    margin: 0 auto;
    max-width: 700px;
    width: 100%;
}

.serach_inner input.search-submit {
    padding: 10px 25px;
    background: url(assets/images/search.png) no-repeat scroll 0 8px;
    font-size: 13px;
    text-align: center;
    float: right;
    text-indent: -9999px;
    border: none;
    margin-top: 6px;
}

.serach-page input.search-field {
    border: 1px solid #ccc;
    padding: 9px;
}

button.search-submit {
    border: 0;
    background: transparent;
}

input.search-field {
    border: none;
    padding: 4px 0;
    background: transparent;
    width: 100%;
    font-size: 20px;
    font-style: italic;
    color: #999
}

.search-box i {
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    padding: 18px 14px;
    background: #7cb240;
}

.search-box {
    text-align: center;
}

/* --------- MENUS CSS --------- */
#mySidenav {
    text-align: center;
    display: inline;
}

ul.list_nav {
    margin: 0 auto;
    text-align: center;
    display: block;
}

.main-menu-navigation ul {
    list-style: none;
    margin: 0;
    padding-left: 0;
}

.main-menu-navigation li {
    padding: 16px 12px;
    display: inline-block;
}

.main-menu-navigation a {
    display: block;
    color: #252525;
    font-size: 14px;
    font-weight: 500;
    letter-spacing: 1px;
    padding: 3px;
}

.main-menu-navigation ul ul {
    display: none;
    position: absolute;
    background: #252525;
    min-width: 215px;
    z-index: 9999;
}

.main-menu-navigation ul ul ul {
    left: 100%;
    top: 0;
}

.main-menu-navigation ul ul a {
    color: #fff;
    border: none;
    padding: 10px;
}

.main-menu-navigation ul ul a:hover {
    background: #1c2242;
}

.main-menu-navigation ul ul li {
    float: none;
    display: block;
    text-align: left;
    border-left: none;
    border-right: none !important;
    padding: 0;
}

.main-menu-navigation ul ul li:last-child {
    border-bottom: 0;
}

.main-menu-navigation ul li:hover>ul {
    display: block;
}

.main-menu-navigation .current_page_item>a,
.main-menu-navigation .current-menu-item>a,
.main-menu-navigation .current_page_ancestor>a {
    color: #121212;
    background: #1c2242;
}

.main-menu-navigation a:hover {
    color: #121212;
    background: #1c2242;
}

.main-menu-navigation .menu>ul>li.highlight {
    background-color: #006ccc;
}

.main-menu-navigation .menu>ul>li.highlight:hover {
    background: transparent;
}

.main-menu-navigation ul ul li:hover>a {
    color: #121212;
}

/*--------------- SUPERFISH MENU ---------------------------*/

.sf-arrows .sf-with-ul:after {
    content: '';
    position: absolute;
    right: 0;
    margin-top: -2px;
    height: 0;
    width: 0;
    border-top-color: #e1e1e1;
}

.sf-arrows ul .sf-with-ul:after {
    margin-top: -5px;
    margin-right: -3px;
    border-color: transparent;
    border-left-color: #888;
}

.sf-arrows ul li>.sf-with-ul:focus:after,
.sf-arrows ul li:hover>.sf-with-ul:after,
.sf-arrows ul .sfHover>.sf-with-ul:after {
    border-left-color: #333;
}

.toggle-nav,
a.closebtn {
    display: none;
}

/* --------- SLIDER--------- */
#slider {
    max-width: 100%;
    margin: auto;
    padding: 0;
    background: #121212;
    border-bottom: solid 5px #1c2242;
}

#slider img {
    width: 100%;
    opacity: 0.3;
}

#slider .carousel-item-next,
#slider .carousel-item-prev,
#slider .carousel-item.active {
    display: block !important;
}

#slider .carousel-control-prev-icon,
#slider .carousel-control-next-icon {
    background-image: none;
    font-size: 15px;
    color: #fff;
    border: solid 2px;
    padding: 12px 18px;
    border-radius: 50%;
}

#slider .carousel-control-prev-icon:hover,
#slider .carousel-control-next-icon:hover {
    background: #1c2242;
    border-color: #1c2242;
}

#slider .carousel-control-prev,
#slider .carousel-control-next {
    opacity: unset !important;
    width: 8%;
}

#slider .carousel-control-next-icon,
#slider .carousel-control-prev-icon {
    width: auto;
    height: auto;
}

#slider .carousel-caption {
    top: 50%;
    transform: translateY(-50%);
    right: 45%;
    left: 10%;
    text-align: left;
}

#slider .inner_carousel {
    border-left: solid 4px #1c2242;
    padding-bottom: 1px;
    padding-left: 10px;
}

#slider .inner_carousel h1 {
    font-size: 45px;
    /*text-align: left;*/
    color: #fff;
    margin-bottom: 0;
    letter-spacing: 1px;
    font-weight: bold;
    text-transform: uppercase;
}

#slider .inner_carousel p {
    color: #fff;
    font-size: 16px;
    letter-spacing: 0;
    line-height: 2;
}

.more-btn {
    margin: 25px 0;
}

/* --------- BUTTON CSS ------- */

.top-btn a,
.more-btn a {
    padding: 14px 20px;
    font-size: 12px;
    font-weight: bold;
    color: #121212;
    border-radius: 30px;
    background: #1c2242;
    letter-spacing: 1px;
}

.top-btn a:hover,
.more-btn a:hover {
    background: #121212;
    color: #fff;
}

/* --------- SERVICES SECTION ------- */

#serv-section {
    padding: 3% 0;
}

.heading-box h2 {
    font-size: 30px;
    letter-spacing: 1px;
    text-transform: uppercase;
    padding: 0;
    font-weight: 600;
    border-left: solid 4px #1c2242;
    padding-left: 10px;
}

.heading-box p {
    margin-bottom: 0;
}

.heading-box {
    padding-bottom: 20px;
}

.serv-box {
    padding: 20px;
    border-radius: 20px;
    box-shadow: 0 0px 10px 2px #eee;
    margin-bottom: 15px;
}

.serv-box h4 {
    font-size: 35px 20px;
    font-weight: 500;
}

.serv-box a i {
    font-size: 25px;
}

.serv-box:hover {
    background: #1c2242;
}

.serv-box:hover h4,
.serv-box:hover p,
.serv-box:hover a i {
    color: #121212;
}

/* ABOUT US */

.box {
    overflow: hidden;
    position: relative;
    text-align: center;
}

.box img {
    height: auto;
    border-radius: 50%;
}

.box .inner-content {
    width: 100%;
    height: 50%;
    text-align: center;
    color: #121212;
    position: absolute;
    top: 65%;
    left: 50%;
    z-index: 2;
    transform: translate(-50%, 50%) scale(0);
    transition: all 0.3s ease 0.2s;
    padding: 0 2em;
}

.box .inner-content:after {
    content: "";
    width: 250px;
    height: 250px;
    border-radius: 50%;
    background: #1c2242;
    position: absolute;
    top: 20%;
    left: 50%;
    z-index: -1;
    transform: translate(-50%, -50%);
    opacity: 0.9;
}

.box:hover .inner-content {
    transform: translate(-50%, -50%) scale(1);
    transition: all 0.3s ease 0s;
}

.box .title {
    font-size: 22px;
    font-weight: 600;
    text-transform: uppercase;
    color: #121212;
    padding-top: 0;
}

.box .post {
    font-size: 14px;
}

.about-btn a {
    background: #121212;
    color: #fff;
    padding: 6px 12px;
    font-size: 12px;
    border-radius: 30px;
    font-weight: bold;
}

iframe,
.entry-audio audio {
    width: 100%;
}

/* --------- FOOTER ------- */
#footer a {
    color: #fff;
}

#footer a:focus {
    outline: 1px dotted #fff;
    border-bottom: 1px solid #fff;
}

#footer .custom-social-icons {
    text-align: left;
}

#footer .tagcloud a {
    border: solid 1px #7e8a9e;
    color: #7e8a9e;
    font-size: 14px !important;
    padding: 6px 12px;
    display: inline-block;
    margin-right: 5px;
    margin-bottom: 5px;
}

#footer .tagcloud a:hover {
    background: #1c2242;
    color: #121212;
}

#footer li a:hover {
    color: #1c2242;
}

#footer ins span,
#footer .tagcloud a {
    color: #7e8a9e;
}

.copyright img {
    text-align: right;
}

#footer .widget {
    padding: 15px 0;
}

#footer input[type="submit"] {
    margin-top: 0;
    padding: 10px 9px;
    font-size: 15px;
    width: 100%;
    color: #121212;
}

#footer label {
    width: 100%;
}

#footer .search-form .search-field {
    width: 100%;
    border: solid 1px #7e8a9e;
    padding: 10px;
    background: transparent;
}

#footer caption,
#sidebar caption {
    font-weight: bold;
    color: #7e8a9e;
    font-size: 20px;
}

#footer table,
#footer th,
#footer td {
    border: 1px solid #7e8a9e;
    text-align: center;
}

#footer td,
#sidebar td {
    padding: 9px;
    color: #7e8a9e;
}

#footer th {
    text-align: center;
    padding: 10px;
    color: #7e8a9e;
}

#footer li a {
    color: #7e8a9e;
    font-size: 14px;
}

#footer ul li {
    list-style: none;
    padding: 5px 0;
}

#footer ul {
    margin: 0;
}

#sidebar select,
#footer select {
    padding: 10px 5px;
    width: 100%;
}

.sub-title {
    margin: 5em 0 0 0;
    line-height: 2em;
}

#footer {
    color: #7e8a9e;
    text-align: left;
    background: #121212;
}

#footer h3 {
    color: #1c2242;
    font-size: 25px;
    padding: 0 0 17px 0;
    letter-spacing: 1px;
}

#footer h3:after {
    border-bottom: solid 3px #1c2242;
    width: 15%;
    padding-bottom: 5px;
    content: "";
    display: block;
}

#footer p {
    font-size: 14px;
}

.navigate ul {
    list-style: none;
}

.copyright a {
    color: #121212;
}

.copyright p {
    margin-bottom: 0;
    color: #121212;
    font-size: 14px;
    font-weight: 500;
}

#footer-2 {
    background: #1c2242;
    text-align: center;
    padding: 20px 0;
}

.scrollup {
    width: 50px;
    height: 50px;
    position: fixed;
    bottom: 50px;
    right: 100px;
    font-size: 25px;
    padding: 6px;
    display: none;
    border: solid 1px #7cb240;
    border-radius: 50%;
}

.scrollup.left {
    left: 100px;
}

.scrollup.center {
    left: 50%;
    right: 50%;
}

a.scrollup {
    color: #7cb240;
}

a.scrollup span {
    display: none;
}

/* --------- Blog Page ------- */
article p a {
    text-decoration: underline;
}

#our-services iframe {
    width: 100%;
}

h1.page-title {
    margin-bottom: 1%;
    margin-top: 0;
}

#our-services {
    margin-top: 0;
}

.post-main-box {
    margin-bottom: 5%;
    padding: 20px;
    border: 1px solid #e1e1e1;
    box-shadow: 0px 15px 35px -15px #e1e1e1
}

.post-main-box h3 {
    font-size: 20px;
    color: #121212;
    letter-spacing: 1px;
    text-align: left;
    margin-top: 0;
    padding-top: 0;
    font-weight: 600;
}

.post-main-box h3 a {
    color: #000000;
}

.post-main-box h3 a:hover {
    color: #1c2242;
}

.new-text p {
    line-height: 26px;
    margin-top: 15px;
    text-align: justify;
}

.post-info {
    font-size: 12px;
    letter-spacing: 1px;
}

.single-post .post-info {
    padding: 10px 0;
    border-top: 1px solid;
    border-bottom: 1px solid;
    margin-bottom: 15px;
}

.post-info span {
    margin-right: 1%;
}

.post-info hr {
    width: 10%;
    border-top: solid 2px #000000;
    margin-bottom: 0;
}

.post-main-box:hover h3 {
    color: #1c2242;
}

.entry-content a {
    color: #114fb2;
    text-decoration: underline;
}

.entry-date a,
.entry-author a {
    color: #000000;
}

.post-info i {
    margin-right: 5px;
}

.tags {
    padding-top: 20px;
}

.tags a {
    text-decoration: none !important;
    font-weight: 400;
    border-radius: 3px;
    color: #121212 !important;
    background: #f8f8f8;
    border: 1px solid #ccc;
    box-shadow: 0 1px 0 rgba(0, 0, 0, .08);
    text-shadow: none;
    line-height: 23px;
    padding: 2px 10px 2px 10px;
    margin: 5px;
}

/* --------- POST --------- */
h2#reply-title {
    text-align: left;
    margin-bottom: 0;
    font-size: 30px;
    text-transform: none;
}

.bradcrumbs {
    padding: 30px 0;
}

.bradcrumbs a {
    background: #f1f1f1;
    color: #000;
    padding: 5px 15px;
    margin-right: 5px;
}

.bradcrumbs a:hover {
    background: #ffd34e;
}

.bradcrumbs span {
    background: #dddddd;
    color: #000;
    padding: 5px 15px;
}

/* Comments BOX */
#comments ol.children img {
    width: 30px;
}

#comments ol {
    list-style: none;
}

#comments {
    margin-top: 30px;
}

#comments h2.comments-title {
    font-size: 20px;
    font-weight: bold;
    border-top: 2px dotted #7e7e7e;
    padding: 40px 0 0 0;
}

#comments h3.comment-reply-title {
    font-size: 20px;
    font-weight: bold;
    clear: both;
}

#comments input[type="text"],
#comments input[type="email"],
#comments textarea {
    width: 100%;
    padding: 12px 10px;
    font-size: 18px;
    margin: 20px 0 0 0;
}

#comments input[type="submit"] {
    background: #1c2242;
    color: #121212;
    padding: 10px 20px;
    font-weight: 800;
    font-size: 12px;
}

#comments a.comment-reply-link {
    color: #000;
    background: #cecece;
    padding: 5px 15px;
}

#comments a.comment-reply-link:hover {
    background: #ddd;
}

#comments a time {
    color: #000;
    font-weight: bold;
    font-size: 12px;
}

#content-vw #comments ol li {
    width: 100%;
    background: none;
    list-style: none;
    margin: 0 0 20px;
    padding: 20px;
    background: #eee;
    display: inline-block;
}

#content-vw #comments ul {
    margin: 0 0 0 40px;
}

#comments .reply {
    float: right;
}

#comments .comment-author {
    width: 100%;
}

#comments .comment-metadata {
    width: 90%;
    float: left;
    padding: 1% 0 0 0;
}

#comments .comment-content {
    width: 90%;
    float: left;
    margin-left: 7%;
}

#comments p {
    width: 100%;
    float: left;
}

/* Sidebar */
#sidebar .custom-social-icons {
    text-align: center;
}

#sidebar .custom-social-icons i,
#footer .custom-social-icons i {
    background: #1c2242;
    width: 40px;
    text-align: center;
    height: 40px;
    padding: 10px;
    margin-bottom: 10px;
    margin-right: 10px;
    line-height: 1.5;
    color: #121212;
}

#sidebar .custom-social-icons i:hover {
    background: #121212;
    color: #fff;
}

#footer .custom-social-icons i:hover {
    background: #fff;
    color: #1c2242;
}

#sidebar td#prev a {
    color: #000000;
}

#sidebar caption {
    font-weight: bold;
    color: #121212;
    font-size: 20px;
}

#sidebar table,
#sidebar th,
#sidebar td {
    border: 1px solid #e1e1e1;
    text-align: center;
}

#sidebar td {
    padding: 7px;
    color: #000000;
}

#sidebar th {
    text-align: center;
    padding: 5px;
    color: #000000;
}

#sidebar select,
.woocommerce .woocommerce-ordering select {
    padding: 10px 5px;
    border: solid 1px #e1e1e1;
    color: #000000;
    background: transparent;
}

#sidebar form {
    text-align: center;
}

#sidebar h3 {
    font-size: 20px;
    display: table;
    color: #121212;
    padding: 10px 15px;
    margin: -45px auto 10px auto;
    background: #1c2242;
    font-weight: 600;
}

#sidebar input[type="search"] {
    padding: 15px;
    font-size: 15px;
    margin-bottom: 10px;
    color: #000000;
    border: solid 1px #e1e1e1;
    width: 100%;
}

#sidebar label {
    margin-bottom: 0;
    width: 100%;
}

#sidebar input[type="submit"] {
    color: #121212;
    padding: 15px;
    font-weight: 600;
    font-size: 14px;
    width: 100%;
    letter-spacing: 1px;
}

#sidebar .widget {
    padding: 20px;
    border: solid 1px #e1e1e1;
    margin-bottom: 50px;
    box-shadow: 0px 15px 35px -15px #e1e1e1;
}

#sidebar ul {
    list-style: none;
    margin: 0;
    padding: 0;
}

#sidebar ul li {
    color: #000000;
    font-size: 14px;
    border-bottom: 1px solid #e1e1e1;
    line-height: 43px;
}

#sidebar ul li a {
    color: #000000;
    letter-spacing: 1px;
    font-size: 14px;
}

#sidebar ul li a:hover {
    color: #1c2242;
}

#sidebar .tagcloud a {
    border: solid 1px #e1e1e1;
    color: #000000;
    font-size: 14px !important;
    padding: 10px 18px;
    display: inline-block;
    margin-right: 5px;
    margin-bottom: 5px;
}

#sidebar .tagcloud a:hover {
    background: #1c2242;
    color: #121212;
}

.nav-previous {
    clear: both;
}

.nav-previous,
.nav-next {
    float: left;
    width: 50%;
}

.nav-next {
    text-align: right;
    font-size: 18px;
}

.nav-previous {
    text-align: left;
    font-size: 18px;
}

/*------------ PAGE CSS -------------*/
.error404,
.error404 p {
    text-align: center !important;
}

#content-vw p {
    text-align: justify;
    line-height: 25px;
    font-size: 14px;
    margin: 0 0 20px 0;
}

#content-vw h1 {
    font-size: 50px;
}

#content-vw h2 {
    font-size: 32px;
    padding: 0;
    margin: 0 0 25px;
}

#content-vw h3 {
    font-size: 36px;
    padding: 0;
    margin: 0 0 25px;
}

#content-vw h4 {
    font-size: 20px;
    padding: 0;
    margin: 0 0 25px;
}

#content-vw h5 {
    margin: 0 0 15px;
    font-size: 18px;
    padding: 0;
}

#content-vw h6 {
    margin: 0 0 15px;
    font-size: 17px;
    padding: 0;
}

#content-vw ul {
    list-style: none;
    margin: 0 0 0 10px;
    padding: 10px 0 0;
    font-size: 16px;
}

#content-vw ol {
    list-style: none;
    margin: 0 0 0 15px;
    padding: 10px 0 0 0;
    font-size: 16px;
    counter-reset: myCounter;
}

#content-vw ol li ol li {
    margin: 0 0 20px 0;
    padding: 0 0 0 35px;
}

#content-vw ol li {
    padding: 0 0 0 0;
    margin: 0 0 20px 0;
}

.read-moresec {
    margin: 2% 0;
}

/*------------- PAGINATION CSS --------------*/
.pagination {
    clear: both;
    position: relative;
    font-size: 14px;
    line-height: 13px;
    float: right;
    font-weight: bold;
    width: 100%;
}

.pagination span,
.pagination a {
    display: block;
    float: left;
    margin: 2px 5px 2px 0;
    padding: 10px 15px;
    text-decoration: none;
    width: auto;
    color: #fff;
    background: #121212;
    -webkit-transition: background .15s ease-in-out;
    -moz-transition: background .15s ease-in-out;
    -ms-transition: background .15s ease-in-out;
    -o-transition: background .15s ease-in-out;
    transition: background .15s ease-in-out;
}

.pagination a:hover {
    background: #1c2242;
    color: #121212;
}

.pagination .current {
    padding: 10px 15px;
    background: #1c2242;
    font-weight: bold;
    color: #121212;
}

#content-vw .pagination ul {
    margin: 0;
    clear: both;
}

#content-vw .pagination ul li {
    margin: 0;
}

.post-navigation {
    clear: both;
}

.post-navigation {
    clear: both;
}

.nav-previous {
    float: left;
    width: 50%;
}

.post-navigation .current .screen-reader-text {
    position: absolute !important;
}

.post-navigation a {
    color: #121212;
    display: block;
    padding: 1.75em 0;
}

.post-navigation span {
    display: block;
}

.post-navigation .meta-nav {
    color: #686868;
    font-size: 13px;
    letter-spacing: 0.076923077em;
    line-height: 1.6153846154;
    margin-bottom: 0.5384615385em;
    text-transform: uppercase;
}

.post-navigation .post-title {
    display: inline;
    font-size: 15px;
    font-weight: 700;
    line-height: 1.2173913043;
    text-rendering: optimizeLegibility;
}

.post-navigation a:hover .post-title,
.post-navigation a:focus .post-title {
    color: #1c2242;
}

.post-navigation:before {
    right: 0;
}

.post-navigation:after {
    right: 54px;
}

.post-navigation a:hover,
.post-navigation a:focus {
    color: #1c2242;
}

.post-navigation .nav-links {
    position: relative;
}

.post-navigation .nav-links:before,
.post-navigation .nav-links:after {
    color: #fff;
    font-size: 32px;
    line-height: 51px;
    opacity: 0.3;
    position: absolute;
    width: 52px;
    z-index: 1;
}

.post-navigation .page-numbers {
    display: inline-block;
    letter-spacing: 0.013157895em;
    line-height: 1;
    margin: 0 0.7368421053em 0 -0.7368421053em;
    padding: 0.8157894737em 0.7368421053em 0.3947368421em;
    text-transform: uppercase;
}

.post-navigation .current {
    display: inline-block;
    font-weight: 700;
}

.post-navigation .prev,
.post-navigation .next {
    background-color: #1a1a1a;
    color: #fff;
    display: inline-block;
    height: 52px;
    margin: 0;
    overflow: hidden;
    padding: 0;
    position: absolute;
    top: 0;
    width: 52px;
    z-index: 2;
}

.post-navigation .prev:before,
.post-navigation .next:before {
    font-size: 32px;
    height: 53px;
    line-height: 52px;
    position: relative;
    width: 53px;
}

.post-navigation .prev:hover,
.post-navigation .prev:focus,
.post-navigation .next:hover,
.post-navigation .next:focus {
    background-color: #007acc;
    color: #fff;
}

.post-navigation .prev:focus,
.post-navigation .next:focus {
    outline: 0;
}

.post-navigation .prev {
    right: 54px;
}

/*------------- WOOCOMMERCE CSS --------------*/
.woocommerce ul.products li.product .button,
a.checkout-button.button.alt.wc-forward {
    margin-top: 1em;
    font-size: 12px;
    letter-spacing: 1px;
    text-transform: uppercase;
    font-weight: bold;
}

.woocommerce .star-rating {
    margin: 0 auto 10px !important;
}

.woocommerce span.onsale {
    background: #121212;
    padding: 0;
    border-radius: 0;
}

.products li {
    box-shadow: 3px 3px 10px 2px #ddd;
    padding: 10px !important;
    text-align: center;
}

h2.woocommerce-loop-product__title,
.woocommerce div.product .product_title {
    color: #121212;
    letter-spacing: 1px;
    margin-bottom: 10px !important;
}

.woocommerce ul.products li.product .price,
.woocommerce div.product p.price,
.woocommerce div.product span.price {
    color: #000000;
    font-size: 15px
}

.woocommerce div.product .product_title,
.woocommerce div.product p.price,
.woocommerce div.product span.price {
    margin-bottom: 5px;
}

.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt {
    background: #1c2242;
    color: #121212;
    padding: 17px;
    border-radius: 0;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 12px;
    letter-spacing: 1px;
}

.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.woocommerce #respond input#submit.alt:hover,
.woocommerce a.button.alt:hover,
.woocommerce button.button.alt:hover,
.woocommerce input.button.alt:hover {
    background: #121212;
    color: #fff;
}

.woocommerce .quantity .qty {
    width: 7em;
    padding: 12px;
    border: solid 1px #121212;
    color: #121212;
}

.col-1,
.col-2 {
    max-width: 100%;
}

nav.woocommerce-MyAccount-navigation ul {
    list-style: none;
}

nav.woocommerce-MyAccount-navigation ul li {
    background: #1c2242;
    padding: 10px;
    margin-bottom: 10px;
    box-shadow: 2px 2px 0 0 #121212;
    font-weight: bold;
}

nav.woocommerce-MyAccount-navigation ul li a {
    color: #121212;
}

span.woocommerce-input-wrapper,
.checkout label {
    width: 100%;
}

.woocommerce .woocommerce-ordering select {
    padding: 5px;
    font-size: 12px;
}

span.posted_in {
    display: block;
}

.woocommerce div.product div.images .woocommerce-product-gallery__image:nth-child(n+2) {
    width: 22%;
    display: inline-block;
    margin: 5px;
}

.woocommerce-message,
.woocommerce-info {
    border-top-color: #121212;
}

.woocommerce-message::before,
.woocommerce-info::before {
    color: #121212;
}

.related.products ul li {
    width: 30.75% !important;
}

nav.woocommerce-MyAccount-navigation ul li a,
.woocommerce-MyAccount-content a,
.woocommerce-info a,
.woocommerce-privacy-policy-text a,
td.product-name a,
a.shipping-calculator-button {
    text-decoration: none !important;
}

.woocommerce-MyAccount-content a,
.woocommerce-info a,
.woocommerce-privacy-policy-text a,
td.product-name a,
a.shipping-calculator-button {
    color: #1c2242;
}

/*----------- PRELOADER ----------*/
.spinner-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(255, 255, 255, 0.9);
    z-index: 999999;
}

.spinner {
    position: relative;
    top: 35%;
    left: 0%;
    text-align: center;
}

.spinner i {
    position: absolute;
    top: 48%;
    left: 48%;
    font-size: 50px;
    font-weight: bold;
    color: #000;
    -webkit-animation: turn 1.5s linear infinite;
    -o-animation: turn 1.5s linear infinite;
    animation: turn 1.5s linear infinite;
}

/*------------------RESPONSIVE MEIDA-------------------*/

@media screen and (max-width: 1000px) {

    .menubar .nav ul li a:focus,
    a.closebtn.mobile-menu:focus {
        outline: 1px dotted #fff;
        border-bottom: 1px solid #fff;
    }

    .toggle-nav {
        display: none;
        position: relative;
        /*float: left;*/
        right: 15px;
        height: 30px;
        width: 30px;
        cursor: pointer;
        margin: 0 auto;
        z-index: 999999;
        text-align: right;
        right: 0;
    }

    .toggle-nav i {
        font-size: 32px;
        color: #000;
        /*margin: 10px 0px;*/
    }

    .toggle-nav button {
        background: transparent;
        border: none;
    }

    .sidenav {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 9999999;
        top: 0;
        right: 0;
        background-color: #252525;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
        display: block;
        color: #121212;
        border: none;
        background: transparent;
    }

    .sidenav #site-navigation {
        width: 100%;
    }

    .toggle-nav span {
        font-size: 30px;
        cursor: pointer;
        text-align: right;
    }

    .toggle-nav {
        display: block;
    }

    .toggle i.fa.fa-bars,
    .toggle i.fa.fa-times {
        float: right;
        color: black;
    }

    .main-menu-navigation ul ul li {
        display: block;
    }

    .menubar .nav ul li {
        border-top: 1px #303030 solid;
        display: block;
    }

    .nav ul li a {
        padding: 10px;
        display: block;
        color: #121212;
        border: none;
    }

    .nav ul li ul,
    .nav ul li ul ul {
        display: block !important;
    }

    .nav ul li ul li a:before {
        content: "\00BB \00a0";
    }

    .nav ul li ul li a {
        padding-left: 20px !important;
    }

    .nav ul li ul li ul li a {
        padding-left: 30px !important;
    }

    .nav ul li ul,
    .nav ul li ul ul {
        display: block !important;
        opacity: 1 !important;
    }

    .main-menu-navigation ul ul {
        position: static;
        width: 100%;
    }

    .main-menu-navigation li {
        padding: 0;
    }

    .main-header-box {
        padding-bottom: 15px;
    }

    #mySidenav {
        text-align: left;
        display: inline;
    }
}

@media screen and (max-width:720px) {

    #slider .inner_carousel,
    .search-box,
    .logo,
    .top-bar .custom-social-icons,
    .top-bar {
        text-align: center;
    }

    .top-bar p {
        padding: 5px 0;
    }

    .custom-social-icons {
        padding: 15px 0;
    }

    .logo {
        padding: 30px 0;
    }

    #slider .carousel-control-prev,
    #slider .carousel-control-next {
        width: auto;
    }

    #slider .inner_carousel {
        border: none;
    }

    #slider .carousel-caption {
        padding: 0;
        right: 10%;
        left: 10%;
    }

    #slider .inner_carousel h1 {
        font-size: 15px;
        text-align: center;
        padding: 0;
    }

    .box .inner-content:after {
        width: 250px;
        height: 250px;
    }

    #slider .inner_carousel p {
        display: none;
    }
}

@media screen and (min-width: 768px) and (max-width: 992px) {
    #slider .inner_carousel p {
        font-size: 12px;
    }

    #slider .inner_carousel h1 {
        font-size: 30px;
    }

    #header .nav ul li a {
        border: none;
    }
}

@media screen and (min-width: 1024px) and (max-width: 1199px) {
    .box .inner-content:after {
        width: 250px;
        height: 250px;
    }
}

/* safari */
a[href^=tel] {
    color: inherit;
    text-decoration: none;
}
