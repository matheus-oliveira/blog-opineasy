<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '82O/s:n4VEFA_;;V }yT1.Pvl|N3WB^95q?SSR]|,:`XA2i!rV#QSjeDwaS<=&?0' );
define( 'SECURE_AUTH_KEY',  '/na<([pGB!D{$Ky|CqO>lput=S2l7_(]pU|9NC+a1Zq)S|f+B$oHZ(mV5Z*.3p]i' );
define( 'LOGGED_IN_KEY',    'hPD1h|=GJ6y2(nQa!(,2T4>/j_ILjiYFv_~Z]9_0dO:9h.RBdX{|a=(0!5l1TAJO' );
define( 'NONCE_KEY',        ']2p0`]U+dR4+pEy=UvA{jtGPe6._!nh7;l{5XMTFC=UY48tQ.y.6bRynSEOB@2Gv' );
define( 'AUTH_SALT',        'P>D*,V#~n%U=Wc~Ot(s4W,1Znm*fSN><Ktt}xj@%TkqF9H84w3Ml5I;5A|$?M(k+' );
define( 'SECURE_AUTH_SALT', 'l#Cfj/VjB_u-rTC#[RY)-Ug%u/~[A96*Ug9KwMhQ#f{Zyxt6vcE %1Cy{K>{X0;3' );
define( 'LOGGED_IN_SALT',   'bx@0qD|>C*5T4_11u![+MUo#>w5e.rC@)0rfB1w25S=qwU{gx+(;=&}RlL{66ml)' );
define( 'NONCE_SALT',       'L(G9d^;D/zJ9b-Yvq`hKKMtq;@be@gH*[w48W.tiXIr*2,[JJH`bSRnomrnrY0Qb' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
