-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 18-Set-2019 às 20:00
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `blog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_action_logs`
--

CREATE TABLE `blog_action_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_language_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `ip_address` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.0.0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_action_logs`
--

INSERT INTO `blog_action_logs` (`id`, `message_language_key`, `message`, `log_date`, `extension`, `user_id`, `item_id`, `ip_address`) VALUES
(1, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2019-09-05 15:40:47', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(2, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":90,\"title\":\"Menu Principal do Blog\",\"extension_name\":\"Menu Principal do Blog\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=90\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 90, 'COM_ACTIONLOGS_DISABLED'),
(3, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":91,\"title\":\"Menu do Autor\",\"extension_name\":\"Menu do Autor\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=91\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 91, 'COM_ACTIONLOGS_DISABLED'),
(4, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":92,\"title\":\"Distribui\\u00e7\\u00e3o\",\"extension_name\":\"Distribui\\u00e7\\u00e3o\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=92\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 92, 'COM_ACTIONLOGS_DISABLED'),
(5, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":93,\"title\":\"Artigos Arquivados\",\"extension_name\":\"Artigos Arquivados\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=93\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 93, 'COM_ACTIONLOGS_DISABLED'),
(6, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":94,\"title\":\"Artigos mais lidos\",\"extension_name\":\"Artigos mais lidos\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=94\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 94, 'COM_ACTIONLOGS_DISABLED'),
(7, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":95,\"title\":\"Artigos mais antigos\",\"extension_name\":\"Artigos mais antigos\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=95\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 95, 'COM_ACTIONLOGS_DISABLED'),
(8, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":96,\"title\":\"Menu Inferior\",\"extension_name\":\"Menu Inferior\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=96\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 96, 'COM_ACTIONLOGS_DISABLED'),
(9, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":97,\"title\":\"Pesquisa\",\"extension_name\":\"Pesquisa\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=97\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 97, 'COM_ACTIONLOGS_DISABLED'),
(10, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":98,\"title\":\"Imagem\",\"extension_name\":\"Imagem\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=98\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 98, 'COM_ACTIONLOGS_DISABLED'),
(11, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":99,\"title\":\"Marcadores Populares\",\"extension_name\":\"Marcadores Populares\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=99\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 99, 'COM_ACTIONLOGS_DISABLED'),
(12, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":100,\"title\":\"Itens Semelhantes\",\"extension_name\":\"Itens Semelhantes\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=100\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 100, 'COM_ACTIONLOGS_DISABLED'),
(13, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":101,\"title\":\"Informa\\u00e7\\u00f5es do Site\",\"extension_name\":\"Informa\\u00e7\\u00f5es do Site\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=101\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 101, 'COM_ACTIONLOGS_DISABLED'),
(14, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":102,\"title\":\"Not\\u00edcias de Lan\\u00e7amento\",\"extension_name\":\"Not\\u00edcias de Lan\\u00e7amento\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:45:45', 'com_modules.module', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(15, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-05 15:50:37', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(16, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":16,\"title\":\"Login Form\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=16\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-05 15:50:37', 'com_modules.module', 932, 16, 'COM_ACTIONLOGS_DISABLED'),
(17, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-05 15:52:07', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(18, 'PLG_ACTIONLOG_JOOMLA_USER_LOGIN_FAILED', '{\"action\":\"login\",\"id\":\"932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"}', '2019-09-06 10:59:06', 'com_users', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(19, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"}', '2019-09-06 10:59:16', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(20, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"}', '2019-09-06 11:25:41', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(21, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2019-09-06 11:26:17', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(22, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10004,\"name\":\"System - Helix Ultimate Framework\",\"extension_name\":\"System - Helix Ultimate Framework\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:27:42', 'com_installer', 932, 10004, 'COM_ACTIONLOGS_DISABLED'),
(23, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_TEMPLATE\",\"id\":10005,\"name\":\"ltsocialcompany\",\"extension_name\":\"ltsocialcompany\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:27:42', 'com_installer', 932, 10005, 'COM_ACTIONLOGS_DISABLED'),
(24, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10003,\"name\":\"LT Social Company Joomla! template\",\"extension_name\":\"LT Social Company Joomla! template\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:27:42', 'com_installer', 932, 10003, 'COM_ACTIONLOGS_DISABLED'),
(25, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":7,\"title\":\"O que \\u00e9 Lorem Ipsum?\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=7\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:36:05', 'com_content.article', 932, 7, 'COM_ACTIONLOGS_DISABLED'),
(26, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_ADDED', '{\"action\":\"add\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":8,\"title\":\"O que \\u00e9 Lorem Ipsum?\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=8\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:37:16', 'com_content.article', 932, 8, 'COM_ACTIONLOGS_DISABLED'),
(27, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_STYLE\",\"id\":9,\"title\":\"ltsocialcompany - Padr\\u00e3o\",\"extension_name\":\"ltsocialcompany - Padr\\u00e3o\",\"itemlink\":\"index.php?option=com_templates&task=style.edit&id=9\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 11:38:24', 'com_templates.style', 932, 9, 'COM_ACTIONLOGS_DISABLED'),
(28, 'PLG_ACTIONLOG_JOOMLA_COMPONENT_CONFIG_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_COMPONENT_CONFIG\",\"id\":\"10\",\"title\":\"com_installer\",\"extension_name\":\"com_installer\",\"itemlink\":\"index.php?option=com_config&task=component.edit&extension_id=10\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:07:25', 'com_config.component', 932, 10, 'COM_ACTIONLOGS_DISABLED'),
(29, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":932,\"title\":\"Super User\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":932,\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:08:29', 'com_users', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(30, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-06 12:24:26', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(31, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__content\"}', '2019-09-06 12:34:17', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(32, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_ARTICLE\",\"id\":8,\"title\":\"O que \\u00e9 Lorem Ipsum?\",\"itemlink\":\"index.php?option=com_content&task=article.edit&id=8\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:34:17', 'com_content.article', 932, 8, 'COM_ACTIONLOGS_DISABLED'),
(33, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 12:39:22', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(34, 'PLG_ACTIONLOG_JOOMLA_EXTENSION_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":\"10004\",\"name\":\"System - Helix Ultimate Framework\",\"extension_name\":\"System - Helix Ultimate Framework\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:40:00', 'com_installer', 932, 10004, 'COM_ACTIONLOGS_DISABLED'),
(35, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"}', '2019-09-06 12:49:21', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(36, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_OUT', '{\"action\":\"logout\",\"id\":\"932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_SITE\"}', '2019-09-06 12:49:52', 'com_users', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(37, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:50:20', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(38, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 12:50:20', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(39, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:50:45', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(40, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:51:43', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(41, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:52:04', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(42, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:52:23', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(43, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:53:10', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(44, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 12:53:43', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(45, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":104,\"title\":\"Acesso de Autor\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=104\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:53:43', 'com_menus.item', 932, 104, 'COM_ACTIONLOGS_DISABLED'),
(46, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 12:53:47', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(47, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UNPUBLISHED', '{\"action\":\"unpublish\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":103,\"title\":\"Sobre\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=103\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:53:47', 'com_menus.item', 932, 103, 'COM_ACTIONLOGS_DISABLED'),
(48, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MODULE\",\"id\":100,\"title\":\"Itens Semelhantes\",\"extension_name\":\"Itens Semelhantes\",\"itemlink\":\"index.php?option=com_modules&task=module.edit&id=100\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:54:51', 'com_modules.module', 932, 100, 'COM_ACTIONLOGS_DISABLED'),
(49, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-06 12:54:51', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(50, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-06 12:56:02', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(51, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__modules\"}', '2019-09-06 12:56:19', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(52, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 12:57:04', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(53, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 12:57:04', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(54, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2019-09-06 13:16:05', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(55, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 13:39:25', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(56, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2019-09-06 14:04:03', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(57, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 14:05:47', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(58, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 14:05:47', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(59, 'PLG_SYSTEM_ACTIONLOGS_CONTENT_UPDATED', '{\"action\":\"update\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_MENU_ITEM\",\"id\":102,\"title\":\"Blog\",\"itemlink\":\"index.php?option=com_menus&task=item.edit&id=102\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 14:07:24', 'com_menus.item', 932, 102, 'COM_ACTIONLOGS_DISABLED'),
(60, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 14:08:44', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(61, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__menu\"}', '2019-09-06 14:09:37', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED'),
(62, 'PLG_ACTIONLOG_JOOMLA_PLUGIN_INSTALLED', '{\"action\":\"install\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_PLUGIN\",\"id\":10006,\"name\":\"plg_installer_webinstaller\",\"extension_name\":\"plg_installer_webinstaller\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\"}', '2019-09-06 14:18:17', 'com_installer', 932, 10006, 'COM_ACTIONLOGS_DISABLED'),
(63, 'PLG_ACTIONLOG_JOOMLA_USER_LOGGED_IN', '{\"action\":\"login\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"app\":\"PLG_ACTIONLOG_JOOMLA_APPLICATION_ADMINISTRATOR\"}', '2019-09-06 15:08:54', 'com_users', 932, 0, 'COM_ACTIONLOGS_DISABLED'),
(64, 'PLG_ACTIONLOG_JOOMLA_USER_CHECKIN', '{\"action\":\"checkin\",\"type\":\"PLG_ACTIONLOG_JOOMLA_TYPE_USER\",\"id\":\"932\",\"title\":\"Opineasy\",\"itemlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"userid\":\"932\",\"username\":\"Opineasy\",\"accountlink\":\"index.php?option=com_users&task=user.edit&id=932\",\"table\":\"#__extensions\"}', '2019-09-06 15:09:19', 'com_checkin', 932, 932, 'COM_ACTIONLOGS_DISABLED');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_action_logs_extensions`
--

CREATE TABLE `blog_action_logs_extensions` (
  `id` int(10) UNSIGNED NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_action_logs_extensions`
--

INSERT INTO `blog_action_logs_extensions` (`id`, `extension`) VALUES
(1, 'com_banners'),
(2, 'com_cache'),
(3, 'com_categories'),
(4, 'com_config'),
(5, 'com_contact'),
(6, 'com_content'),
(7, 'com_installer'),
(8, 'com_media'),
(9, 'com_menus'),
(10, 'com_messages'),
(11, 'com_modules'),
(12, 'com_newsfeeds'),
(13, 'com_plugins'),
(14, 'com_redirect'),
(15, 'com_tags'),
(16, 'com_templates'),
(17, 'com_users'),
(18, 'com_checkin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_action_logs_users`
--

CREATE TABLE `blog_action_logs_users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `notify` tinyint(1) UNSIGNED NOT NULL,
  `extensions` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_action_logs_users`
--

INSERT INTO `blog_action_logs_users` (`user_id`, `notify`, `extensions`) VALUES
(932, 0, '[\"com_content\",\"com_banners\",\"com_categories\",\"com_config\",\"com_contact\",\"com_checkin\",\"com_newsfeeds\",\"com_modules\",\"com_installer\",\"com_messages\",\"com_menus\",\"com_media\",\"com_plugins\",\"com_redirect\",\"com_tags\",\"com_templates\",\"com_users\",\"com_cache\"]');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_action_log_config`
--

CREATE TABLE `blog_action_log_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_action_log_config`
--

INSERT INTO `blog_action_log_config` (`id`, `type_title`, `type_alias`, `id_holder`, `title_holder`, `table_name`, `text_prefix`) VALUES
(1, 'article', 'com_content.article', 'id', 'title', '#__content', 'PLG_ACTIONLOG_JOOMLA'),
(2, 'article', 'com_content.form', 'id', 'title', '#__content', 'PLG_ACTIONLOG_JOOMLA'),
(3, 'banner', 'com_banners.banner', 'id', 'name', '#__banners', 'PLG_ACTIONLOG_JOOMLA'),
(4, 'user_note', 'com_users.note', 'id', 'subject', '#__user_notes', 'PLG_ACTIONLOG_JOOMLA'),
(5, 'media', 'com_media.file', '', 'name', '', 'PLG_ACTIONLOG_JOOMLA'),
(6, 'category', 'com_categories.category', 'id', 'title', '#__categories', 'PLG_ACTIONLOG_JOOMLA'),
(7, 'menu', 'com_menus.menu', 'id', 'title', '#__menu_types', 'PLG_ACTIONLOG_JOOMLA'),
(8, 'menu_item', 'com_menus.item', 'id', 'title', '#__menu', 'PLG_ACTIONLOG_JOOMLA'),
(9, 'newsfeed', 'com_newsfeeds.newsfeed', 'id', 'name', '#__newsfeeds', 'PLG_ACTIONLOG_JOOMLA'),
(10, 'link', 'com_redirect.link', 'id', 'old_url', '#__redirect_links', 'PLG_ACTIONLOG_JOOMLA'),
(11, 'tag', 'com_tags.tag', 'id', 'title', '#__tags', 'PLG_ACTIONLOG_JOOMLA'),
(12, 'style', 'com_templates.style', 'id', 'title', '#__template_styles', 'PLG_ACTIONLOG_JOOMLA'),
(13, 'plugin', 'com_plugins.plugin', 'extension_id', 'name', '#__extensions', 'PLG_ACTIONLOG_JOOMLA'),
(14, 'component_config', 'com_config.component', 'extension_id', 'name', '', 'PLG_ACTIONLOG_JOOMLA'),
(15, 'contact', 'com_contact.contact', 'id', 'name', '#__contact_details', 'PLG_ACTIONLOG_JOOMLA'),
(16, 'module', 'com_modules.module', 'id', 'title', '#__modules', 'PLG_ACTIONLOG_JOOMLA'),
(17, 'access_level', 'com_users.level', 'id', 'title', '#__viewlevels', 'PLG_ACTIONLOG_JOOMLA'),
(18, 'banner_client', 'com_banners.client', 'id', 'name', '#__banner_clients', 'PLG_ACTIONLOG_JOOMLA'),
(19, 'application_config', 'com_config.application', '', 'name', '', 'PLG_ACTIONLOG_JOOMLA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_assets`
--

CREATE TABLE `blog_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_assets`
--

INSERT INTO `blog_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 167, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(8, 1, 17, 40, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(9, 1, 41, 42, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 43, 44, 1, 'com_installer', 'com_installer', '{\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0,\"8\":1}}'),
(11, 1, 45, 48, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1}}'),
(12, 1, 49, 50, 1, 'com_login', 'com_login', '{}'),
(13, 1, 51, 52, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 53, 54, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 55, 56, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 57, 66, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1}}'),
(17, 1, 67, 68, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 69, 132, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1}}'),
(19, 1, 133, 136, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(20, 1, 137, 138, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1}}'),
(21, 1, 139, 140, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1}}'),
(22, 1, 141, 142, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 143, 144, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1}}'),
(24, 1, 145, 148, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1}}'),
(26, 1, 149, 150, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 19, 2, 'com_content.category.2', 'Uncategorised', '{}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{}'),
(30, 19, 134, 135, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{}'),
(32, 24, 146, 147, 2, 'com_users.category.7', 'Uncategorised', '{}'),
(33, 1, 151, 152, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 153, 154, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{}'),
(35, 1, 155, 156, 1, 'com_tags', 'com_tags', '{}'),
(36, 1, 157, 158, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 159, 160, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 161, 162, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 70, 71, 2, 'com_modules.module.1', 'Main Menu', '{}'),
(40, 18, 72, 73, 2, 'com_modules.module.2', 'Login', '{}'),
(41, 18, 74, 75, 2, 'com_modules.module.3', 'Popular Articles', '{}'),
(42, 18, 76, 77, 2, 'com_modules.module.4', 'Recently Added Articles', '{}'),
(43, 18, 78, 79, 2, 'com_modules.module.8', 'Toolbar', '{}'),
(44, 18, 80, 81, 2, 'com_modules.module.9', 'Quick Icons', '{}'),
(45, 18, 82, 83, 2, 'com_modules.module.10', 'Logged-in Users', '{}'),
(46, 18, 84, 85, 2, 'com_modules.module.12', 'Admin Menu', '{}'),
(47, 18, 86, 87, 2, 'com_modules.module.13', 'Admin Submenu', '{}'),
(48, 18, 88, 89, 2, 'com_modules.module.14', 'User Status', '{}'),
(49, 18, 90, 91, 2, 'com_modules.module.15', 'Title', '{}'),
(50, 18, 92, 93, 2, 'com_modules.module.16', 'Login Form', '{}'),
(51, 18, 94, 95, 2, 'com_modules.module.17', 'Breadcrumbs', '{}'),
(52, 18, 96, 97, 2, 'com_modules.module.79', 'Multilanguage status', '{}'),
(53, 18, 98, 99, 2, 'com_modules.module.86', 'Joomla Version', '{}'),
(54, 16, 58, 59, 2, 'com_menus.menu.1', 'Main Menu', '{}'),
(55, 18, 100, 101, 2, 'com_modules.module.87', 'Sample Data', '{}'),
(56, 1, 163, 164, 1, 'com_privacy', 'com_privacy', '{}'),
(57, 1, 165, 166, 1, 'com_actionlogs', 'com_actionlogs', '{}'),
(58, 18, 102, 103, 2, 'com_modules.module.88', 'Latest Actions', '{}'),
(59, 18, 104, 105, 2, 'com_modules.module.89', 'Privacy Dashboard', '{}'),
(60, 11, 46, 47, 2, 'com_languages.language.2', 'Português do Brasil (pt-BR)', '{}'),
(61, 8, 20, 33, 2, 'com_content.category.8', 'Blog', '{}'),
(62, 8, 34, 39, 2, 'com_content.category.9', 'Ajuda', '{}'),
(63, 62, 35, 36, 3, 'com_content.article.1', 'Sobre', '{}'),
(64, 62, 37, 38, 3, 'com_content.article.2', 'Trabalhando em seu Site', '{}'),
(65, 61, 21, 22, 3, 'com_content.article.3', 'Bem-vindo ao seu blog', '{}'),
(66, 61, 23, 24, 3, 'com_content.article.4', 'Sobre sua página principal', '{}'),
(67, 61, 25, 26, 3, 'com_content.article.5', 'Seus Módulos', '{}'),
(68, 61, 27, 28, 3, 'com_content.article.6', 'Seu Tema', '{}'),
(69, 16, 60, 61, 2, 'com_menus.menu.2', 'Menu Principal do Blog', '{}'),
(70, 16, 62, 63, 2, 'com_menus.menu.3', 'Menu do Autor', '{}'),
(71, 16, 64, 65, 2, 'com_menus.menu.4', 'Menu Inferior', '{}'),
(72, 18, 106, 107, 2, 'com_modules.module.90', 'Menu Principal do Blog', '{}'),
(73, 18, 108, 109, 2, 'com_modules.module.91', 'Menu do Autor', '{}'),
(74, 18, 110, 111, 2, 'com_modules.module.92', 'Distribuição', '{}'),
(75, 18, 112, 113, 2, 'com_modules.module.93', 'Artigos Arquivados', '{}'),
(76, 18, 114, 115, 2, 'com_modules.module.94', 'Artigos mais lidos', '{}'),
(77, 18, 116, 117, 2, 'com_modules.module.95', 'Artigos mais antigos', '{}'),
(78, 18, 118, 119, 2, 'com_modules.module.96', 'Menu Inferior', '{}'),
(79, 18, 120, 121, 2, 'com_modules.module.97', 'Pesquisa', '{}'),
(80, 18, 122, 123, 2, 'com_modules.module.98', 'Imagem', '{}'),
(81, 18, 124, 125, 2, 'com_modules.module.99', 'Marcadores Populares', '{}'),
(82, 18, 126, 127, 2, 'com_modules.module.100', 'Itens Semelhantes', '{}'),
(83, 18, 128, 129, 2, 'com_modules.module.101', 'Informações do Site', '{}'),
(84, 18, 130, 131, 2, 'com_modules.module.102', 'Notícias de Lançamento', '{}'),
(85, 61, 29, 30, 3, 'com_content.article.7', 'O que é Lorem Ipsum?', '{}'),
(86, 61, 31, 32, 3, 'com_content.article.8', 'O que é Lorem Ipsum?', '{}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_associations`
--

CREATE TABLE `blog_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_banners`
--

CREATE TABLE `blog_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT 0,
  `impmade` int(11) NOT NULL DEFAULT 0,
  `clicks` int(11) NOT NULL DEFAULT 0,
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT 0,
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT -1,
  `track_clicks` tinyint(4) NOT NULL DEFAULT -1,
  `track_impressions` tinyint(4) NOT NULL DEFAULT -1,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_banner_clients`
--

CREATE TABLE `blog_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT 0,
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT -1,
  `track_clicks` tinyint(4) NOT NULL DEFAULT -1,
  `track_impressions` tinyint(4) NOT NULL DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_banner_tracks`
--

CREATE TABLE `blog_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lft` int(11) NOT NULL DEFAULT 0,
  `rgt` int(11) NOT NULL DEFAULT 0,
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 15, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 932, '2019-09-05 15:27:58', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 61, 1, 11, 12, 1, 'blog', 'com_content', 'Blog', 'blog', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 932, '2019-09-05 15:45:43', 0, '2019-09-05 15:45:43', 0, '*', 1),
(9, 62, 1, 13, 14, 1, 'ajuda', 'com_content', 'Ajuda', 'ajuda', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 932, '2019-09-05 15:45:43', 0, '2019-09-05 15:45:43', 0, '*', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_contact_details`
--

CREATE TABLE `blog_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `catid` int(11) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Set if contact is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_content`
--

CREATE TABLE `blog_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_content`
--

INSERT INTO `blog_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`, `note`) VALUES
(1, 63, 'Sobre', 'sobre', '<p>Aqui se fala um pouco sobre este blog e a pessoa que o escreve.</p> <p>Quando estiver logado, será possível editar esta página, clicando no ícone de edição.</p>', '23', 1, 9, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 1, 1, '', '', 1, 4, '{}', 0, '*', '', ''),
(2, 64, 'Trabalhando em seu Site', 'trabalhando-em-seu-site', '<p>Algumas dicas básicas para trabalhar no seu site.</p><ul><li>Joomla! tem um \'front-end\', que é o que se vê agora, e um \'Administrador\' ou \'back-end\' que é onde se faz o trabalho mais avançado de criação do site, como configurar menus e decidir quais módulos mostrar. Você precisa acessar o administrador separadamente usando o mesmo nome de usuário e senha que você usou para acessar para esta área do site.</li> <li>Uma das primeiras coisas que você provavelmente vai querer fazer é alterar o título do site, a descrição e adicionar uma logomarca. Para fazer isso clique em Configurações de Tema (Template Settings) no Menu do Usuário (User Menu). </li><li>Para alterar a descrição do site, título do navegador e outros itens, clique em Configurações do Site (Site Settings).</li><li>Opções de configuração mais avançadas estão disponíveis no administrador.</li><li>Para mudar completamente a aparência do seu site, você pode instalar um novo modelo. No menu Extensões clique Gerenciar &gt; Instalar. Existem muitos modelos grátis ou pagos disponíveis para Joomla.</li><li>Como você já viu, pode-se controlar quem pode ver as diferentes áreas do site. Ao trabalhar com módulos, artigos ou weblinks e definir o Nível de Acesso como Registered permitirá apenas aos usuários logados vê-los</li><li>Quando criar um novo artigo ou outro tipo de conteúdo você também pode salvá-lo como Publicado ou Não Publicado. Os Não Publicados não poderão ser vistos pelos visitantes, mas você poderá vê-los.</li><li>No administrador há botões de ajuda em todas as páginas que fornecem informações detalhadas sobre as funções da página.</li><li>Você aprenderá muito mais sobre como trabalhar com o Joomla no <a href=\'https://docs.joomla.org/\'> site de documentação do Joomla</a> e conseguirá ajuda de outros usuários no <a href=\'https://forum.joomla.org/\'> Fórum Joomla</a>.</li></ul>', '23', 1, 9, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 2, 1, '', '', 3, 1, '{}', 0, '*', '', ''),
(3, 65, 'Bem-vindo ao seu blog', 'bem-vindo-ao-seu-blog', '<p>Este é um exemplo de publicação de blog.</p><p>Se você acessar o site (o link Login de Autor está bem no final desta página), poderá editar este artigo e todos os outros existentes. Você também poderá criar um novo artigo e fazer outras alterações no site.</p><p>À medida que adicionar e modificar artigos, verá como seu site muda e como pode personalizá-lo de várias maneiras.</p><p>Vá em frente, não há como estragá-lo.</p>', '23', 1, 8, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 3, 4, '', '', 1, 1, '{}', 0, '*', '', ''),
(4, 66, 'Sobre sua página principal', 'sobre-sua-pagina-principal', '<p>Sua página inicial está configurada para exibir quatro artigos mais recentes da categoria blog em uma coluna. Depois aparecem links para os dois artigos anteriores. Você pode mudar a quantidade editando as configurações de opções de conteúdo na aba blog na administração do seu site. Existe um link para a administração do seu site no menu superior.</p><p>Se quiser que sua publicação no blog seja dividida em duas partes, introdução e depois o contetúdo completo separado, use o botão Leia Mais para inserir uma quebra.</p>', '<p>Na página completa, você verá tanto o conteúdo introdutório como o resto do artigo. Você pode mudar as configurações para ocultar a introdução, se quiser.</p><p></p><p></p><p></p>', 1, 8, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 4, 5, '', '', 1, 1, '{}', 0, '*', '', ''),
(5, 67, 'Seus Módulos', 'seus-modulos', '<p>Seu site já possui alguns módulos de uso comum preconfigurados. Eles incluem:</p><ul>\n <li> Imagem, que mostra uma imagem sob o menu. Este é um módulo personalizado que se pode editar para mudar a imagem.</li>\n <li> Artigos - Mais Lidas, que lista artigos com base no número de vezes que foram lidos.</li>\n <li> Artigos - Arquivados, que enumera artigos artigos por mês de publicação.</li>\n <li>Distribuição de Feeds, que cria uma distribuição de feed para a página onde é exibido..</li>\n <li>Marcadores - Populares, que é exibido se a marcação for usada nos artigos. Basta inserir um marcador no campo Marcadores ao editar.</li></ul><p>Cada um desses módulos tem muitas opções que você pode experimentar no Gerenciador de Módulos na administração do seu site. Mover o mouse sobre um módulo e clicar no ícone de edição o levará para uma tela de edição deste módulo. Lembre sempre de salvar e fechar qualquer módulo que editar.</p>\n <p>O Joomla! também inclui muitos outros módulos que você pode incorporar ao seu site. À medida em que desenvolve seu site, é possível incluir mais módulos que podem ser encontrados no <a href=\'https://extensions.joomla.org/\'>Diretório de Extensões Joomla (Joomla Extensions Directory)</a>.</p>', '23', 1, 8, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 5, 3, '', '', 1, 1, '{}', 0, '*', '', ''),
(6, 68, 'Seu Tema', 'seu-tema', '<p>Temas controlam a aparência do seu site.</p><p>Este blog está instalado com o tema Protostar.</p><p>Você pode editar as opções clicando no link Trabalhando no Seu Site, Configurações de Tema no menu superior (visível quando você entrar).</p><p>Por exemplo, você pode mudar a cor de fundo do site, cor de destaque, título, descrição e fonte de caracteres.</p><p>Mais opções estão disponíveis na administração do site. Você também pode instalar um novo tema usando o gerenciador de extensões.</p>', '23', 1, 8, '2019-09-05 15:45:43', 932, '', '2019-09-05 15:45:43', 0, 0, '0000-00-00 00:00:00', '2019-09-05 15:45:43', '0000-00-00 00:00:00', '', '{}', '{}', 6, 2, '', '', 1, 2, '{}', 0, '*', '', ''),
(7, 85, 'O que é Lorem Ipsum?', 'o-que-e-lorem-ipsum', '<div>\r\n<p><strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>\r\n</div>\r\n<div>\r\n<h2>Porque nós o usamos?</h2>\r\n<p>É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p>\r\n</div>\r\n<p> </p>\r\n<div>\r\n<h2>De onde ele vem?</h2>\r\n<p>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.</p>\r\n<p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>\r\n</div>\r\n<div>\r\n<h2>Onde posso conseguí-lo?</h2>\r\n<p>Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.</p>\r\n</div>', '', 1, 8, '2019-09-06 11:36:05', 932, '', '2019-09-06 11:36:05', 0, 0, '0000-00-00 00:00:00', '2019-09-06 11:36:05', '0000-00-00 00:00:00', '{}', '{}', '{\"helix_ultimate_image\":\"\",\"helix_ultimate_article_format\":\"standard\",\"helix_ultimate_audio\":\"\",\"helix_ultimate_gallery\":\"\",\"helix_ultimate_video\":\"\"}', 1, 1, '', '', 1, 0, '{}', 0, '*', '', ''),
(8, 86, 'O que é Lorem Ipsum?', 'o-que-e-lorem-ipsum-2', '<div>\r\n<p><strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>\r\n</div>\r\n<div>\r\n<h2>Porque nós o usamos?</h2>\r\n<p>É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p>\r\n</div>\r\n<p> </p>\r\n<div>\r\n<h2>De onde ele vem?</h2>\r\n<p>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.</p>\r\n<p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>\r\n</div>\r\n<div>\r\n<h2>Onde posso conseguí-lo?</h2>\r\n<p>Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.</p>\r\n</div>', '', 0, 8, '2019-09-06 11:37:16', 932, '', '2019-09-06 11:37:16', 0, 0, '0000-00-00 00:00:00', '2019-09-06 11:37:16', '0000-00-00 00:00:00', '{}', '{}', '{\"helix_ultimate_image\":\"\",\"helix_ultimate_article_format\":\"standard\",\"helix_ultimate_audio\":\"\",\"helix_ultimate_gallery\":\"\",\"helix_ultimate_video\":\"\"}', 1, 0, '', '', 1, 0, '{}', 0, '*', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_contentitem_tag_map`
--

CREATE TABLE `blog_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_content_frontpage`
--

CREATE TABLE `blog_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_content_rating`
--

CREATE TABLE `blog_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT 0,
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_content_types`
--

CREATE TABLE `blog_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_content_types`
--

INSERT INTO `blog_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\", \"note\":\"note\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"ordering\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(4, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(6, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(9, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(11, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(12, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(13, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_core_log_searches`
--

CREATE TABLE `blog_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_extensions`
--

CREATE TABLE `blog_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT 0 COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `protected` tinyint(3) NOT NULL DEFAULT 0,
  `manifest_cache` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT 0,
  `state` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_extensions`
--

INSERT INTO `blog_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"contact_layout\":\"_:default\",\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_tags\":\"1\",\"show_info\":\"1\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_image\":\"1\",\"show_misc\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"articles_display_num\":\"10\",\"show_profile\":\"0\",\"show_user_custom_fields\":[\"-1\"],\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"0\",\"maxLevel\":\"-1\",\"show_subcat_desc\":\"1\",\"show_empty_categories\":\"0\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_subcat_desc_cat\":\"1\",\"show_empty_categories_cat\":\"0\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"0\",\"show_pagination_limit\":\"0\",\"show_headings\":\"1\",\"show_image_heading\":\"0\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"initial_sort\":\"ordering\",\"captcha\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"0\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_feed_link\":\"1\",\"sef_advanced\":0,\"sef_ids\":0,\"custom_fields_enable\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '{\"show_jed_info\":\"1\",\"cachetimeout\":\"6\",\"minimum_stability\":\"4\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"pt-BR\",\"site\":\"pt-BR\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"search_phrases\":\"1\",\"search_areas\":\"1\",\"show_date\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"0\",\"upload_limit\":\"10\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css,scss,sass\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"0\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"10\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"12\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"0\",\"useractivation\":\"2\",\"mail_to_admin\":\"1\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"enabled\":\"0\",\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_autosuggest\":\"1\",\"show_suggested_query\":\"1\",\"show_explained_query\":\"1\",\"show_advanced\":\"1\",\"show_advanced_tips\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"sort_order\":\"relevance\",\"sort_direction\":\"desc\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stem\":\"1\",\"stemmer\":\"snowball\",\"enable_logging\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '{\"updatesource\":\"default\",\"customurl\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_description\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"January 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(35, 0, 'com_privacy', 'component', 'com_privacy', '', 1, 1, 1, 1, '{\"name\":\"com_privacy\",\"type\":\"component\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"COM_PRIVACY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacy\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(36, 0, 'com_actionlogs', 'component', 'com_actionlogs', '', 1, 1, 1, 1, '{\"name\":\"com_actionlogs\",\"type\":\"component\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"COM_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"ip_logging\":0,\"csv_delimiter\":\",\",\"loggable_extensions\":[\"com_banners\",\"com_cache\",\"com_categories\",\"com_checkin\",\"com_config\",\"com_contact\",\"com_content\",\"com_installer\",\"com_media\",\"com_menus\",\"com_messages\",\"com_modules\",\"com_newsfeeds\",\"com_plugins\",\"com_redirect\",\"com_tags\",\"com_templates\",\"com_users\"]}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"c3fd13db957d7e34d1a9f0dc748e012f\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(318, 0, 'mod_sampledata', 'module', 'mod_sampledata', '', 1, 1, 1, 0, '{\"name\":\"mod_sampledata\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"MOD_SAMPLEDATA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sampledata\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(319, 0, 'mod_latestactions', 'module', 'mod_latestactions', '', 1, 1, 1, 0, '{\"name\":\"mod_latestactions\",\"type\":\"module\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"MOD_LATESTACTIONS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latestactions\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `blog_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(320, 0, 'mod_privacy_dashboard', 'module', 'mod_privacy_dashboard', '', 1, 1, 1, 0, '{\"name\":\"mod_privacy_dashboard\",\"type\":\"module\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"MOD_PRIVACY_DASHBOARD_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_privacy_dashboard\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.40.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2019\",\"author\":\"Tiny Technologies, Inc\",\"copyright\":\"Tiny Technologies, Inc\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"https:\\/\\/www.tiny.cloud\",\"version\":\"4.5.11\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"configuration\":{\"toolbars\":{\"2\":{\"toolbar1\":[\"bold\",\"underline\",\"strikethrough\",\"|\",\"undo\",\"redo\",\"|\",\"bullist\",\"numlist\",\"|\",\"pastetext\"]},\"1\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"formatselect\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"code\",\"|\",\"hr\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"pastetext\",\"preview\"]},\"0\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"styleselect\",\"|\",\"formatselect\",\"fontselect\",\"fontsizeselect\",\"|\",\"searchreplace\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"image\",\"|\",\"code\",\"|\",\"forecolor\",\"backcolor\",\"|\",\"fullscreen\",\"|\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"emoticons\",\"media\",\"hr\",\"ltr\",\"rtl\",\"|\",\"cut\",\"copy\",\"paste\",\"pastetext\",\"|\",\"visualchars\",\"visualblocks\",\"nonbreaking\",\"blockquote\",\"template\",\"|\",\"print\",\"preview\",\"codesample\",\"insertdatetime\",\"removeformat\"]}},\"setoptions\":{\"2\":{\"access\":[\"1\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"1\":{\"access\":[\"6\",\"2\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"0\":{\"access\":[\"7\",\"4\",\"8\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}}},\"sets_amount\":3,\"html_height\":\"550\",\"html_width\":\"750\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1567765169}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '{\"mode\":1,\"lastrun\":1567769185,\"unique_id\":\"736b58b0892fce8aa79d46c542fe41b0002f05a5\",\"interval\":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}', '{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(479, 0, 'plg_sampledata_blog', 'plugin', 'blog', 'sampledata', 0, 1, 1, 0, '{\"name\":\"plg_sampledata_blog\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"PLG_SAMPLEDATA_BLOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"blog\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(480, 0, 'plg_system_sessiongc', 'plugin', 'sessiongc', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sessiongc\",\"type\":\"plugin\",\"creationDate\":\"February 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.6\",\"description\":\"PLG_SYSTEM_SESSIONGC_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sessiongc\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(481, 0, 'plg_fields_repeatable', 'plugin', 'repeatable', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_repeatable\",\"type\":\"plugin\",\"creationDate\":\"April 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_FIELDS_REPEATABLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"repeatable\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(482, 0, 'plg_content_confirmconsent', 'plugin', 'confirmconsent', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_confirmconsent\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_CONTENT_CONFIRMCONSENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"confirmconsent\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(483, 0, 'PLG_SYSTEM_ACTIONLOGS', 'plugin', 'actionlogs', 'system', 0, 1, 1, 0, '{\"name\":\"PLG_SYSTEM_ACTIONLOGS\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"actionlogs\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(484, 0, 'PLG_ACTIONLOG_JOOMLA', 'plugin', 'joomla', 'actionlog', 0, 1, 1, 0, '{\"name\":\"PLG_ACTIONLOG_JOOMLA\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_ACTIONLOG_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(485, 0, 'plg_system_privacyconsent', 'plugin', 'privacyconsent', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_privacyconsent\",\"type\":\"plugin\",\"creationDate\":\"April 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_PRIVACYCONSENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacyconsent\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `blog_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(486, 0, 'plg_system_logrotation', 'plugin', 'logrotation', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_logrotation\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_SYSTEM_LOGROTATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logrotation\"}', '{\"lastrun\":1567697353}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(487, 0, 'plg_privacy_user', 'plugin', 'user', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_user\",\"type\":\"plugin\",\"creationDate\":\"May 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(488, 0, 'plg_quickicon_privacycheck', 'plugin', 'privacycheck', 'quickicon', 0, 1, 1, 0, '{\"name\":\"plg_quickicon_privacycheck\",\"type\":\"plugin\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_QUICKICON_PRIVACYCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"privacycheck\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(489, 0, 'plg_user_terms', 'plugin', 'terms', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_terms\",\"type\":\"plugin\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_USER_TERMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"terms\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(490, 0, 'plg_privacy_contact', 'plugin', 'contact', 'privacy', 0, 0, 1, 0, '{\"name\":\"plg_privacy_contact\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(491, 0, 'plg_privacy_content', 'plugin', 'content', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_content\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(492, 0, 'plg_privacy_message', 'plugin', 'message', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_message\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_MESSAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"message\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(493, 0, 'plg_privacy_actionlogs', 'plugin', 'actionlogs', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_actionlogs\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_ACTIONLOGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"actionlogs\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(494, 0, 'plg_captcha_recaptcha_invisible', 'plugin', 'recaptcha_invisible', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha_invisible\",\"type\":\"plugin\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_INVISIBLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha_invisible\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(495, 0, 'plg_privacy_consents', 'plugin', 'consents', 'privacy', 0, 1, 1, 0, '{\"name\":\"plg_privacy_consents\",\"type\":\"plugin\",\"creationDate\":\"July 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.0\",\"description\":\"PLG_PRIVACY_CONSENTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"consents\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 0, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 0, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"August 2019\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.11\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"August 2019\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.11\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"August 2019\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2019 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.11\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"August 2019\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.9.11.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 10002, 'PortugusdoBrasilpt-BR', 'language', 'pt-BR', '', 0, 1, 0, 0, '{\"name\":\"Portugu\\u00eas do Brasil (pt-BR)\",\"type\":\"language\",\"creationDate\":\"Agosto de 2019\",\"author\":\"Projeto Joomla!\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters. Todos os direitos reservados.\",\"authorEmail\":\"helvecio.dasilva@community.joomla.org\",\"authorUrl\":\"http:\\/\\/brasil.joomla.com\",\"version\":\"3.9.11.1\",\"description\":\"pt-BR site language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 10002, 'PortugusdoBrasilpt-BR', 'language', 'pt-BR', '', 1, 1, 0, 0, '{\"name\":\"Portugu\\u00eas do Brasil (pt-BR)\",\"type\":\"language\",\"creationDate\":\"Agosto de 2019\",\"author\":\"Projeto Joomla!\",\"copyright\":\"Copyright (C) 2005-2019 Open Source Matters. Todos os direitos reservados.\",\"authorEmail\":\"helvecio.dasilva@community.joomla.org\",\"authorUrl\":\"http:\\/\\/brasil.joomla.com\",\"version\":\"3.9.11.1\",\"description\":\"Brazilian Portuguese Administrator language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 0, 'Português do Brasil (pt-BR)', 'package', 'pkg_pt-BR', '', 0, 1, 1, 0, '{\"name\":\"Portugu\\u00eas do Brasil (pt-BR)\",\"type\":\"package\",\"creationDate\":\"Julho de 2019\",\"author\":\"Projeto Joomla!\",\"copyright\":\"Copyright (C) 2005 - 2019 Open Source Matters, Inc. Todos os direitos reservados.\",\"authorEmail\":\"helvecio.dasilva@community.joomla.org\",\"authorUrl\":\"http:\\/\\/brasil.joomla.com\",\"version\":\"3.9.11.1\",\"description\":\"<div style=\\\"text-align: left;\\\">\\n <h2>Tradu\\u00e7\\u00e3o Portugu\\u00eas Brasileiro (pt-BR) para Joomla! 3.9.11 instalada com sucesso!<\\/h2>\\n <h3>Vers\\u00e3o 3.9.11.1<\\/h3>\\n <p>Por favor, informe qualquer problema ou assunto relacionado encontrado, na p\\u00e1gina <a href=\\\"https:\\/\\/www. facebook. com\\/groups\\/traduzjoomla\\/\\\" target=\\\"_blank\\\">Grupo Tradu\\u00e7\\u00f5es Joomla pt-BR<\\/a> no Facebook.<\\/p>\\n <p>Traduzido pela <em>Equipe de Tradu\\u00e7\\u00e3o Portugu\\u00eas Brasileiro<\\/em>.<\\/p>\\n<\\/div>\",\"group\":\"\",\"filename\":\"pkg_pt-BR\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 0, 'System - Helix Ultimate Framework', 'plugin', 'helixultimate', 'system', 0, 1, 1, 0, '{\"name\":\"System - Helix Ultimate Framework\",\"type\":\"plugin\",\"creationDate\":\"Feb 2018\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2018 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.1.1\",\"description\":\"Helix Ultimate Framework - Joomla Template Framework by JoomShaper\",\"group\":\"\",\"filename\":\"helixultimate\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 0, 'ltsocialcompany', 'template', 'ltsocialcompany', '', 0, 1, 1, 0, '{\"name\":\"ltsocialcompany\",\"type\":\"template\",\"creationDate\":\"August 2019\",\"author\":\"ltheme.com\",\"copyright\":\"Copyright (C) ltheme.com. All rights reserved.\",\"authorEmail\":\"support@ltheme.com\",\"authorUrl\":\"http:\\/\\/www.ltheme.com\",\"version\":\"2.0\",\"description\":\"LT Social Company - Joomla Social Company website template\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 0, 'plg_installer_webinstaller', 'plugin', 'webinstaller', 'installer', 0, 1, 1, 0, '{\"name\":\"plg_installer_webinstaller\",\"type\":\"plugin\",\"creationDate\":\"28 April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2013 - 2019 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.0.1\",\"description\":\"PLG_INSTALLER_WEBINSTALLER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"webinstaller\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_fields`
--

CREATE TABLE `blog_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_fields_categories`
--

CREATE TABLE `blog_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_fields_groups`
--

CREATE TABLE `blog_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `access` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_fields_values`
--

CREATE TABLE `blog_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_filters`
--

CREATE TABLE `blog_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` text NOT NULL,
  `params` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links`
--

CREATE TABLE `blog_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 1,
  `state` int(5) DEFAULT 1,
  `access` int(5) DEFAULT 0,
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT 0,
  `sale_price` double UNSIGNED NOT NULL DEFAULT 0,
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links`
--

INSERT INTO `blog_finder_links` (`link_id`, `url`, `route`, `title`, `description`, `indexdate`, `md5sum`, `published`, `state`, `access`, `language`, `publish_start_date`, `publish_end_date`, `start_date`, `end_date`, `list_price`, `sale_price`, `type_id`, `object`) VALUES
(1, 'index.php?option=com_content&view=article&id=8', 'index.php?option=com_content&view=article&id=8:o-que-e-lorem-ipsum-2&catid=8', 'O que é Lorem Ipsum?', ' Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker. Porque nós o usamos? É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero). De onde ele vem? Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32. O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914. Onde posso conseguí-lo? Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc. ', '2019-09-06 09:34:20', '0991312eddd18efef36517fff229ff0a', 1, 0, 1, '*', '2019-09-06 11:37:16', '0000-00-00 00:00:00', '2019-09-06 11:37:16', '0000-00-00 00:00:00', 0, 0, 4, 0x4f3a31393a2246696e646572496e6465786572526573756c74223a31393a7b733a31313a22002a00656c656d656e7473223b613a32363a7b733a323a226964223b733a313a2238223b733a353a22616c696173223b733a32313a226f2d7175652d652d6c6f72656d2d697073756d2d32223b733a373a2273756d6d617279223b733a333532303a223c6469763e0d0a3c703e3c7374726f6e673e4c6f72656d20497073756d3c2f7374726f6e673ec2a0c3a92073696d706c65736d656e746520756d612073696d756c61c3a7c3a36f20646520746578746f20646120696e64c3ba7374726961207469706f6772c3a166696361206520646520696d70726573736f732c20652076656d2073656e646f207574696c697a61646f206465736465206f2073c3a963756c6f205856492c207175616e646f20756d20696d70726573736f7220646573636f6e68656369646f207065676f7520756d612062616e64656a61206465207469706f732065206f7320656d626172616c686f7520706172612066617a657220756d206c6976726f206465206d6f64656c6f73206465207469706f732e204c6f72656d20497073756d20736f6272657669766575206ec3a36f2073c3b320612063696e636f2073c3a963756c6f732c20636f6d6f2074616d62c3a96d20616f2073616c746f2070617261206120656469746f7261c3a7c3a36f20656c657472c3b46e6963612c207065726d616e6563656e646f20657373656e6369616c6d656e746520696e616c74657261646f2e20536520706f70756c6172697a6f75206e612064c3a9636164612064652036302c207175616e646f2061204c65747261736574206c616ec3a76f7520646563616c7175657320636f6e74656e646f20706173736167656e73206465204c6f72656d20497073756d2c2065206d61697320726563656e74656d656e7465207175616e646f20706173736f7520612073657220696e7465677261646f206120736f6674776172657320646520656469746f7261c3a7c3a36f20656c657472c3b46e69636120636f6d6f20416c64757320506167654d616b65722e3c2f703e0d0a3c2f6469763e0d0a3c6469763e0d0a3c68323e506f72717565206ec3b373206f207573616d6f733f3c2f68323e0d0a3c703ec38920756d206661746f20636f6e68656369646f20646520746f646f732071756520756d206c6569746f72207365206469737472616972c3a120636f6d206f20636f6e7465c3ba646f20646520746578746f206c6567c3ad76656c20646520756d612070c3a167696e61207175616e646f2065737469766572206578616d696e616e646f20737561206469616772616d61c3a7c3a36f2e20412076616e746167656d2064652075736172204c6f72656d20497073756d20c3a92071756520656c652074656d20756d6120646973747269627569c3a7c3a36f206e6f726d616c206465206c65747261732c20616f20636f6e7472c3a172696f2064652022436f6e7465c3ba646f20617175692c20636f6e7465c3ba646f2061717569222c2066617a656e646f20636f6d2071756520656c652074656e686120756d612061706172c3aa6e6369612073696d696c6172206120646520756d20746578746f206c6567c3ad76656c2e204d7569746f7320736f66747761726573206465207075626c696361c3a7c3a36f206520656469746f7265732064652070c3a167696e6173206e6120696e7465726e65742061676f7261207573616d204c6f72656d20497073756d20636f6d6f20746578746f2d6d6f64656c6f2070616472c3a36f2c206520756d612072c3a17069646120627573636120706f7220276c6f72656d20697073756d27206d6f737472612076c3a172696f732077656273697465732061696e646120656d20737561206661736520646520636f6e73747275c3a7c3a36f2e2056c3a1726961732076657273c3b56573206e6f76617320737572676972616d20616f206c6f6e676f20646f7320616e6f732c206576656e7475616c6d656e746520706f722061636964656e74652c206520c3a0732076657a65732064652070726f70c3b37369746f2028696e6a6574616e646f2068756d6f722c206520636f6973617320646f2067c3aa6e65726f292e3c2f703e0d0a3c2f6469763e0d0a3c703ec2a03c2f703e0d0a3c6469763e0d0a3c68323e4465206f6e646520656c652076656d3f3c2f68323e0d0a3c703e416f20636f6e7472c3a172696f20646f207175652073652061637265646974612c204c6f72656d20497073756d206ec3a36f20c3a92073696d706c65736d656e746520756d20746578746f2072616e64c3b46d69636f2e20436f6d206d616973206465203230303020616e6f732c2073756173207261c3ad7a657320706f64656d2073657220656e636f6e74726164617320656d20756d61206f627261206465206c697465726174757261206c6174696e6120636cc3a17373696361206461746164612064652034352041432e2052696368617264204d63436c696e746f636b2c20756d2070726f666573736f72206465206c6174696d20646f2048616d7064656e2d5379646e657920436f6c6c656765206e612056697267696e69612c20706573717569736f7520756d6120646173206d616973206f627363757261732070616c617672617320656d206c6174696d2c20636f6e73656374657475722c206f7269756e646120646520756d6120706173736167656d206465204c6f72656d20497073756d2c20652c2070726f637572616e646f20706f7220656e7472652063697461c3a7c3b565732064612070616c61767261206e61206c69746572617475726120636cc3a173736963612c20646573636f6272697520612073756120696e6475626974c3a176656c206f726967656d2e204c6f72656d20497073756d2076656d20646173207365c3a7c3b5657320312e31302e3332206520312e31302e333320646f202264652046696e6962757320426f6e6f72756d206574204d616c6f72756d2220284f732045787472656d6f7320646f2042656d206520646f204d616c292c2064652043c3ad6365726f2c206573637269746f20656d2034352041432e2045737465206c6976726f20c3a920756d207472617461646f2064652074656f72696120646120c3a974696361206d7569746f20706f70756c6172206e6120c3a9706f63612064612052656e617363656ec3a7612e2041207072696d65697261206c696e6861206465204c6f72656d20497073756d2c20224c6f72656d20497073756d20646f6c6f722073697420616d65742e2e2e222076656d20646520756d61206c696e6861206e61207365c3a7c3a36f20312e31302e33322e3c2f703e0d0a3c703e4f2074726563686f2070616472c3a36f206f726967696e616c206465204c6f72656d20497073756d2c20757361646f206465736465206f2073c3a963756c6f205856492c20657374c3a120726570726f64757a69646f2061626169786f2070617261206f7320696e74657265737361646f732e205365c3a7c3b5657320312e31302e3332206520312e31302e3333206465202264652046696e6962757320426f6e6f72756d206574204d616c6f72756d222064652043696365726f2074616d62c3a96d20666f72616d20726570726f64757a696461732061626169786f20656d2073756120666f726d61206578617461206f726967696e616c2c2061636f6d70616e68616461206461732076657273c3b565732070617261206f20696e676cc3aa73206461207472616475c3a7c3a36f20666569746120706f7220482e205261636b68616d20656d20313931342e3c2f703e0d0a3c2f6469763e0d0a3c6469763e0d0a3c68323e4f6e646520706f73736f20636f6e73656775c3ad2d6c6f3f3c2f68323e0d0a3c703e4578697374656d206d7569746173207661726961c3a7c3b5657320646973706f6ec3ad7665697320646520706173736167656e73206465204c6f72656d20497073756d2c206d61732061206d61696f72696120736f6672657520616c67756d207469706f20646520616c74657261c3a7c3a36f2c2073656a6120706f7220696e736572c3a7c3a36f20646520706173736167656e7320636f6d2068756d6f722c206f752070616c617672617320616c656174c3b37269617320717565206ec3a36f207061726563656d206e656d20756d20706f75636f20636f6e76696e63656e7465732e20536520766f63c3aa2070726574656e6465207573617220756d6120706173736167656d206465204c6f72656d20497073756d2c2070726563697361207465722063657274657a6120646520717565206ec3a36f2068c3a120616c676f20656d62617261c3a76f736f206573637269746f206573636f6e6469646f206e6f206d65696f20646f20746578746f2e20546f646f73206f732067657261646f726573206465204c6f72656d20497073756d206e6120696e7465726e65742074656e64656d206120726570657469722070656461c3a76f7320707265646566696e69646f7320636f6e666f726d65206e6563657373c3a172696f2c2066617a656e646f206465737465206f207072696d6569726f2067657261646f72206465204c6f72656d20497073756d20617574c3aa6e7469636f20646120696e7465726e65742e20456c652075736120756d20646963696f6ec3a172696f20636f6d206d616973206465203230302070616c617672617320656d204c6174696d20636f6d62696e61646f20636f6d20756d2070756e6861646f206465206d6f64656c6f732064652065737472757475726120646520667261736573207061726120676572617220756d204c6f72656d20497073756d20636f6d2061706172c3aa6e6369612072617a6fc3a176656c2c206c6976726520646520726570657469c3a7c3b565732c20696e736572c3a7c3b565732064652068756d6f722c2070616c6176726173206ec3a36f206361726163746572c3ad7374696361732c206574632e3c2f703e0d0a3c2f6469763e223b733a343a22626f6479223b733a303a22223b733a363a22696d61676573223b733a323a227b7d223b733a353a226361746964223b733a313a2238223b733a31303a22637265617465645f6279223b733a333a22393332223b733a31363a22637265617465645f62795f616c696173223b733a303a22223b733a383a226d6f646966696564223b733a31393a22323031392d30392d30362031313a33373a3136223b733a31313a226d6f6469666965645f6279223b733a313a2230223b733a363a22706172616d73223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a333a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a36393a7b733a31343a2261727469636c655f6c61796f7574223b733a393a225f3a64656661756c74223b733a31303a2273686f775f7469746c65223b733a313a2231223b733a31313a226c696e6b5f7469746c6573223b733a313a2231223b733a31303a2273686f775f696e74726f223b733a313a2231223b733a31333a2273686f775f63617465676f7279223b733a313a2231223b733a31333a226c696e6b5f63617465676f7279223b733a313a2231223b733a32303a2273686f775f706172656e745f63617465676f7279223b733a313a2230223b733a32303a226c696e6b5f706172656e745f63617465676f7279223b733a313a2230223b733a31313a2273686f775f617574686f72223b733a313a2231223b733a31313a226c696e6b5f617574686f72223b733a313a2230223b733a31363a2273686f775f6372656174655f64617465223b733a313a2230223b733a31363a2273686f775f6d6f646966795f64617465223b733a313a2230223b733a31373a2273686f775f7075626c6973685f64617465223b733a313a2231223b733a32303a2273686f775f6974656d5f6e617669676174696f6e223b733a313a2231223b733a393a2273686f775f766f7465223b733a313a2230223b733a31333a2273686f775f726561646d6f7265223b733a313a2231223b733a31393a2273686f775f726561646d6f72655f7469746c65223b733a313a2231223b733a31343a22726561646d6f72655f6c696d6974223b733a333a22313030223b733a31303a2273686f775f69636f6e73223b733a313a2231223b733a31353a2273686f775f7072696e745f69636f6e223b733a313a2231223b733a31353a2273686f775f656d61696c5f69636f6e223b733a313a2230223b733a393a2273686f775f68697473223b733a313a2231223b733a31313a2273686f775f6e6f61757468223b733a313a2230223b733a32333a2273686f775f7075626c697368696e675f6f7074696f6e73223b733a313a2231223b733a32303a2273686f775f61727469636c655f6f7074696f6e73223b733a313a2231223b733a31323a22736176655f686973746f7279223b733a313a2231223b733a31333a22686973746f72795f6c696d6974223b693a31303b733a32353a2273686f775f75726c735f696d616765735f66726f6e74656e64223b733a313a2230223b733a32343a2273686f775f75726c735f696d616765735f6261636b656e64223b733a313a2231223b733a373a2274617267657461223b693a303b733a373a2274617267657462223b693a303b733a373a2274617267657463223b693a303b733a31313a22666c6f61745f696e74726f223b733a343a226c656674223b733a31343a22666c6f61745f66756c6c74657874223b733a343a226c656674223b733a31353a2263617465676f72795f6c61796f7574223b733a363a225f3a626c6f67223b733a31393a2273686f775f63617465676f72795f7469746c65223b733a313a2230223b733a31363a2273686f775f6465736372697074696f6e223b733a313a2230223b733a32323a2273686f775f6465736372697074696f6e5f696d616765223b733a313a2230223b733a383a226d61784c6576656c223b733a313a2231223b733a32313a2273686f775f656d7074795f63617465676f72696573223b733a313a2230223b733a31363a2273686f775f6e6f5f61727469636c6573223b733a313a2231223b733a31363a2273686f775f7375626361745f64657363223b733a313a2231223b733a32313a2273686f775f6361745f6e756d5f61727469636c6573223b733a313a2230223b733a32313a2273686f775f626173655f6465736372697074696f6e223b733a313a2231223b733a31313a226d61784c6576656c636174223b733a323a222d31223b733a32353a2273686f775f656d7074795f63617465676f726965735f636174223b733a313a2230223b733a32303a2273686f775f7375626361745f646573635f636174223b733a313a2231223b733a32353a2273686f775f6361745f6e756d5f61727469636c65735f636174223b733a313a2231223b733a32303a226e756d5f6c656164696e675f61727469636c6573223b733a313a2231223b733a31383a226e756d5f696e74726f5f61727469636c6573223b733a313a2234223b733a31313a226e756d5f636f6c756d6e73223b733a313a2232223b733a393a226e756d5f6c696e6b73223b733a313a2234223b733a31383a226d756c74695f636f6c756d6e5f6f72646572223b733a313a2230223b733a32343a2273686f775f73756263617465676f72795f636f6e74656e74223b733a313a2230223b733a32313a2273686f775f706167696e6174696f6e5f6c696d6974223b733a313a2231223b733a31323a2266696c7465725f6669656c64223b733a343a2268696465223b733a31333a2273686f775f68656164696e6773223b733a313a2231223b733a31343a226c6973745f73686f775f64617465223b733a313a2230223b733a31313a22646174655f666f726d6174223b733a303a22223b733a31343a226c6973745f73686f775f68697473223b733a313a2231223b733a31363a226c6973745f73686f775f617574686f72223b733a313a2231223b733a31313a226f7264657262795f707269223b733a353a226f72646572223b733a31313a226f7264657262795f736563223b733a353a227264617465223b733a31303a226f726465725f64617465223b733a393a227075626c6973686564223b733a31353a2273686f775f706167696e6174696f6e223b733a313a2232223b733a32333a2273686f775f706167696e6174696f6e5f726573756c7473223b733a313a2231223b733a31343a2273686f775f666565645f6c696e6b223b733a313a2231223b733a31323a22666565645f73756d6d617279223b733a313a2230223b733a32393a2268656c69785f756c74696d6174655f61727469636c655f666f726d6174223b733a383a227374616e64617264223b7d733a31343a22002a00696e697469616c697a6564223b623a313b733a393a22736570617261746f72223b733a313a222e223b7d733a373a226d6574616b6579223b733a303a22223b733a383a226d65746164657363223b733a303a22223b733a383a226d65746164617461223b4f3a32343a224a6f6f6d6c615c52656769737472795c5265676973747279223a333a7b733a373a22002a0064617461223b4f3a383a22737464436c617373223a303a7b7d733a31343a22002a00696e697469616c697a6564223b623a313b733a393a22736570617261746f72223b733a313a222e223b7d733a373a2276657273696f6e223b733a313a2231223b733a383a226f72646572696e67223b733a313a2230223b733a383a2263617465676f7279223b733a343a22426c6f67223b733a393a226361745f7374617465223b733a313a2231223b733a31303a226361745f616363657373223b733a313a2231223b733a343a22736c7567223b733a32333a22383a6f2d7175652d652d6c6f72656d2d697073756d2d32223b733a373a22636174736c7567223b733a363a22383a626c6f67223b733a363a22617574686f72223b733a31303a2253757065722055736572223b733a363a226c61796f7574223b733a373a2261727469636c65223b733a373a22636f6e74657874223b733a31393a22636f6d5f636f6e74656e742e61727469636c65223b733a343a2270617468223b733a34363a222f6a6f6f6d6c612f696e6465782e7068702f626c6f672f382d6f2d7175652d652d6c6f72656d2d697073756d2d32223b733a31303a226d657461617574686f72223b4e3b7d733a31353a22002a00696e737472756374696f6e73223b613a353a7b693a313b613a333a7b693a303b733a353a227469746c65223b693a313b733a383a227375627469746c65223b693a323b733a323a226964223b7d693a323b613a323a7b693a303b733a373a2273756d6d617279223b693a313b733a343a22626f6479223b7d693a333b613a383a7b693a303b733a343a226d657461223b693a313b733a31303a226c6973745f7072696365223b693a323b733a31303a2273616c655f7072696365223b693a333b733a373a226d6574616b6579223b693a343b733a383a226d65746164657363223b693a353b733a31303a226d657461617574686f72223b693a363b733a363a22617574686f72223b693a373b733a31363a22637265617465645f62795f616c696173223b7d693a343b613a323a7b693a303b733a343a2270617468223b693a313b733a353a22616c696173223b7d693a353b613a313a7b693a303b733a383a22636f6d6d656e7473223b7d7d733a31313a22002a007461786f6e6f6d79223b613a343a7b733a343a2254797065223b613a313a7b733a373a2241727469636c65223b4f3a32373a224a6f6f6d6c615c434d535c4f626a6563745c434d534f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a373a2241727469636c65223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a363a22417574686f72223b613a313a7b733a31303a2253757065722055736572223b4f3a32373a224a6f6f6d6c615c434d535c4f626a6563745c434d534f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a31303a2253757065722055736572223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a2243617465676f7279223b613a313a7b733a343a22426c6f67223b4f3a32373a224a6f6f6d6c615c434d535c4f626a6563745c434d534f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a343a22426c6f67223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d733a383a224c616e6775616765223b613a313a7b733a313a222a223b4f3a32373a224a6f6f6d6c615c434d535c4f626a6563745c434d534f626a656374223a343a7b733a31303a22002a005f6572726f7273223b613a303a7b7d733a353a227469746c65223b733a313a222a223b733a353a227374617465223b693a313b733a363a22616363657373223b693a313b7d7d7d733a333a2275726c223b733a34363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d38223b733a353a22726f757465223b733a37363a22696e6465782e7068703f6f7074696f6e3d636f6d5f636f6e74656e7426766965773d61727469636c652669643d383a6f2d7175652d652d6c6f72656d2d697073756d2d322663617469643d38223b733a353a227469746c65223b733a32313a224f2071756520c3a9204c6f72656d20497073756d3f223b733a31313a226465736372697074696f6e223b733a333336333a22204c6f72656d20497073756d20c3a92073696d706c65736d656e746520756d612073696d756c61c3a7c3a36f20646520746578746f20646120696e64c3ba7374726961207469706f6772c3a166696361206520646520696d70726573736f732c20652076656d2073656e646f207574696c697a61646f206465736465206f2073c3a963756c6f205856492c207175616e646f20756d20696d70726573736f7220646573636f6e68656369646f207065676f7520756d612062616e64656a61206465207469706f732065206f7320656d626172616c686f7520706172612066617a657220756d206c6976726f206465206d6f64656c6f73206465207469706f732e204c6f72656d20497073756d20736f6272657669766575206ec3a36f2073c3b320612063696e636f2073c3a963756c6f732c20636f6d6f2074616d62c3a96d20616f2073616c746f2070617261206120656469746f7261c3a7c3a36f20656c657472c3b46e6963612c207065726d616e6563656e646f20657373656e6369616c6d656e746520696e616c74657261646f2e20536520706f70756c6172697a6f75206e612064c3a9636164612064652036302c207175616e646f2061204c65747261736574206c616ec3a76f7520646563616c7175657320636f6e74656e646f20706173736167656e73206465204c6f72656d20497073756d2c2065206d61697320726563656e74656d656e7465207175616e646f20706173736f7520612073657220696e7465677261646f206120736f6674776172657320646520656469746f7261c3a7c3a36f20656c657472c3b46e69636120636f6d6f20416c64757320506167654d616b65722e20506f72717565206ec3b373206f207573616d6f733f20c38920756d206661746f20636f6e68656369646f20646520746f646f732071756520756d206c6569746f72207365206469737472616972c3a120636f6d206f20636f6e7465c3ba646f20646520746578746f206c6567c3ad76656c20646520756d612070c3a167696e61207175616e646f2065737469766572206578616d696e616e646f20737561206469616772616d61c3a7c3a36f2e20412076616e746167656d2064652075736172204c6f72656d20497073756d20c3a92071756520656c652074656d20756d6120646973747269627569c3a7c3a36f206e6f726d616c206465206c65747261732c20616f20636f6e7472c3a172696f2064652022436f6e7465c3ba646f20617175692c20636f6e7465c3ba646f2061717569222c2066617a656e646f20636f6d2071756520656c652074656e686120756d612061706172c3aa6e6369612073696d696c6172206120646520756d20746578746f206c6567c3ad76656c2e204d7569746f7320736f66747761726573206465207075626c696361c3a7c3a36f206520656469746f7265732064652070c3a167696e6173206e6120696e7465726e65742061676f7261207573616d204c6f72656d20497073756d20636f6d6f20746578746f2d6d6f64656c6f2070616472c3a36f2c206520756d612072c3a17069646120627573636120706f7220276c6f72656d20697073756d27206d6f737472612076c3a172696f732077656273697465732061696e646120656d20737561206661736520646520636f6e73747275c3a7c3a36f2e2056c3a1726961732076657273c3b56573206e6f76617320737572676972616d20616f206c6f6e676f20646f7320616e6f732c206576656e7475616c6d656e746520706f722061636964656e74652c206520c3a0732076657a65732064652070726f70c3b37369746f2028696e6a6574616e646f2068756d6f722c206520636f6973617320646f2067c3aa6e65726f292e204465206f6e646520656c652076656d3f20416f20636f6e7472c3a172696f20646f207175652073652061637265646974612c204c6f72656d20497073756d206ec3a36f20c3a92073696d706c65736d656e746520756d20746578746f2072616e64c3b46d69636f2e20436f6d206d616973206465203230303020616e6f732c2073756173207261c3ad7a657320706f64656d2073657220656e636f6e74726164617320656d20756d61206f627261206465206c697465726174757261206c6174696e6120636cc3a17373696361206461746164612064652034352041432e2052696368617264204d63436c696e746f636b2c20756d2070726f666573736f72206465206c6174696d20646f2048616d7064656e2d5379646e657920436f6c6c656765206e612056697267696e69612c20706573717569736f7520756d6120646173206d616973206f627363757261732070616c617672617320656d206c6174696d2c20636f6e73656374657475722c206f7269756e646120646520756d6120706173736167656d206465204c6f72656d20497073756d2c20652c2070726f637572616e646f20706f7220656e7472652063697461c3a7c3b565732064612070616c61767261206e61206c69746572617475726120636cc3a173736963612c20646573636f6272697520612073756120696e6475626974c3a176656c206f726967656d2e204c6f72656d20497073756d2076656d20646173207365c3a7c3b5657320312e31302e3332206520312e31302e333320646f202264652046696e6962757320426f6e6f72756d206574204d616c6f72756d2220284f732045787472656d6f7320646f2042656d206520646f204d616c292c2064652043c3ad6365726f2c206573637269746f20656d20343541432e2045737465206c6976726f20c3a920756d207472617461646f2064652074656f72696120646120c3a974696361206d7569746f20706f70756c6172206e6120c3a9706f63612064612052656e617363656ec3a7612e2041207072696d65697261206c696e6861206465204c6f72656d20497073756d2c20224c6f72656d20497073756d20646f6c6f722073697420616d65742e2e2e222076656d20646520756d61206c696e6861206e61207365c3a7c3a36f20312e31302e33322e204f2074726563686f2070616472c3a36f206f726967696e616c206465204c6f72656d20497073756d2c20757361646f206465736465206f2073c3a963756c6f205856492c20657374c3a120726570726f64757a69646f2061626169786f2070617261206f7320696e74657265737361646f732e205365c3a7c3b5657320312e31302e3332206520312e31302e3333206465202264652046696e6962757320426f6e6f72756d206574204d616c6f72756d222064652043696365726f2074616d62c3a96d20666f72616d20726570726f64757a696461732061626169786f20656d2073756120666f726d61206578617461206f726967696e616c2c2061636f6d70616e68616461206461732076657273c3b565732070617261206f20696e676cc3aa73206461207472616475c3a7c3a36f20666569746120706f7220482e205261636b68616d20656d20313931342e204f6e646520706f73736f20636f6e73656775c3ad2d6c6f3f204578697374656d206d7569746173207661726961c3a7c3b5657320646973706f6ec3ad7665697320646520706173736167656e73206465204c6f72656d20497073756d2c206d61732061206d61696f72696120736f6672657520616c67756d207469706f20646520616c74657261c3a7c3a36f2c2073656a6120706f7220696e736572c3a7c3a36f20646520706173736167656e7320636f6d2068756d6f722c206f752070616c617672617320616c656174c3b37269617320717565206ec3a36f207061726563656d206e656d20756d20706f75636f20636f6e76696e63656e7465732e20536520766f63c3aa2070726574656e6465207573617220756d6120706173736167656d206465204c6f72656d20497073756d2c2070726563697361207465722063657274657a6120646520717565206ec3a36f2068c3a120616c676f20656d62617261c3a76f736f206573637269746f206573636f6e6469646f206e6f206d65696f20646f20746578746f2e20546f646f73206f732067657261646f726573206465204c6f72656d20497073756d206e6120696e7465726e65742074656e64656d206120726570657469722070656461c3a76f7320707265646566696e69646f7320636f6e666f726d65206e6563657373c3a172696f2c2066617a656e646f206465737465206f207072696d6569726f2067657261646f72206465204c6f72656d20497073756d20617574c3aa6e7469636f20646120696e7465726e65742e20456c652075736120756d20646963696f6ec3a172696f20636f6d206d616973206465203230302070616c617672617320656d204c6174696d20636f6d62696e61646f20636f6d20756d2070756e6861646f206465206d6f64656c6f732064652065737472757475726120646520667261736573207061726120676572617220756d204c6f72656d20497073756d20636f6d2061706172c3aa6e6369612072617a6fc3a176656c2c206c6976726520646520726570657469c3a7c3b565732c20696e736572c3a7c3b565732064652068756d6f722c2070616c6176726173206ec3a36f206361726163746572c3ad7374696361732c206574632e20223b733a393a227075626c6973686564223b4e3b733a353a227374617465223b693a303b733a363a22616363657373223b733a313a2231223b733a383a226c616e6775616765223b733a313a222a223b733a31383a227075626c6973685f73746172745f64617465223b733a31393a22323031392d30392d30362031313a33373a3136223b733a31363a227075626c6973685f656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a2273746172745f64617465223b733a31393a22323031392d30392d30362031313a33373a3136223b733a383a22656e645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31303a226c6973745f7072696365223b4e3b733a31303a2273616c655f7072696365223b4e3b733a373a22747970655f6964223b693a343b733a31353a2264656661756c744c616e6775616765223b733a353a2270742d4252223b7d);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms0`
--

CREATE TABLE `blog_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms0`
--

INSERT INTO `blog_finder_links_terms0` (`link_id`, `term_id`, `weight`) VALUES
(1, 34, 0.51359),
(1, 35, 0.86331),
(1, 36, 1.05),
(1, 37, 0.79331),
(1, 38, 0.86331),
(1, 39, 0.98),
(1, 40, 1.23669),
(1, 41, 0.93331),
(1, 42, 1.09669),
(1, 43, 0.91),
(1, 44, 1.07331),
(1, 45, 0.93331),
(1, 46, 1.07331),
(1, 47, 0.91),
(1, 48, 1.09669),
(1, 49, 0.81669),
(1, 50, 1.05),
(1, 51, 0.95669),
(1, 52, 1.02669),
(1, 53, 0.81669),
(1, 54, 1.09669),
(1, 55, 0.93331),
(1, 56, 1.00331),
(1, 57, 0.56),
(1, 58, 0.91),
(1, 59, 1.00331),
(1, 60, 0.95669),
(1, 61, 1.02669),
(1, 62, 0.09331),
(1, 63, 0.93331),
(1, 64, 1.19),
(1, 65, 0.37331),
(1, 66, 0.93331),
(1, 67, 1.00331),
(1, 68, 0.51331),
(1, 69, 1.05),
(1, 70, 1.23669),
(1, 71, 0.37331),
(1, 72, 1.02669),
(1, 73, 1.16669),
(1, 74, 0.23331),
(1, 75, 0.93331),
(1, 76, 1.07331),
(1, 77, 0.23331),
(1, 78, 0.88669),
(1, 79, 0.98),
(1, 80, 0.23331),
(1, 81, 1.05),
(1, 82, 1.21331),
(1, 83, 0.46669),
(1, 84, 1.02669),
(1, 85, 1.12),
(1, 86, 0.18669),
(1, 87, 1.05),
(1, 88, 1.23669),
(1, 89, 0.23331),
(1, 90, 0.93331),
(1, 91, 1.00331),
(1, 92, 0.42),
(1, 93, 1.02669),
(1, 94, 1.12),
(1, 95, 0.18669),
(1, 96, 0.88669),
(1, 97, 0.95669),
(1, 98, 0.37338),
(1, 99, 1.12),
(1, 100, 1.21331),
(1, 101, 0.91),
(1, 102, 1.07331),
(1, 103, 0.37324),
(1, 104, 1.96),
(1, 105, 1.05),
(1, 106, 1.05),
(1, 107, 0.88669),
(1, 108, 0.98),
(1, 109, 0.88669),
(1, 110, 1.00331),
(1, 111, 0.84),
(1, 112, 1.12),
(1, 113, 1.26),
(1, 114, 1.09669),
(1, 115, 1.14331),
(1, 116, 0.37338),
(1, 117, 1.00331),
(1, 118, 1.12),
(1, 119, 0.98),
(1, 120, 1.07331),
(1, 124, 0.42),
(1, 125, 0.98),
(1, 126, 1.19),
(1, 1014, 0.23331),
(1, 1015, 0.93331),
(1, 1016, 0.98),
(1, 1017, 0.37324),
(1, 1018, 0.95669),
(1, 1019, 1.09669),
(1, 1020, 0.98),
(1, 1021, 1.07331),
(1, 1022, 1.02669),
(1, 1023, 1.09669),
(1, 1024, 0.86331),
(1, 1025, 1.07331),
(1, 1026, 0.23331),
(1, 1027, 1.00331),
(1, 1028, 1.05),
(1, 1029, 0.56),
(1, 1030, 2.05338),
(1, 1031, 2.14662),
(1, 1032, 0.56),
(1, 1033, 1.86662),
(1, 1034, 1.05),
(1, 1035, 1.09669),
(1, 1036, 0.32669),
(1, 1037, 0.98),
(1, 1038, 1.14331),
(1, 1039, 0.18669),
(1, 1040, 0.88669),
(1, 1041, 1.09669),
(1, 1042, 0.23331),
(1, 1043, 1.05),
(1, 1044, 1.19),
(1, 1045, 0.28),
(1, 1046, 1.05),
(1, 1047, 1.12),
(1, 1048, 1.00331),
(1, 1049, 1.05),
(1, 1050, 0.32669),
(1, 1051, 0.91),
(1, 1052, 0.98),
(1, 1053, 1.12),
(1, 1054, 1.05),
(1, 1055, 1.19),
(1, 1056, 1.07331),
(1, 1057, 1.30669),
(1, 1058, 0.42),
(1, 1059, 0.98),
(1, 1060, 1.12),
(1, 1061, 0.14),
(1, 1062, 0.88669),
(1, 1063, 0.98),
(1, 1064, 0.09331),
(1, 1065, 0.79331),
(1, 1066, 0.93331),
(1, 1067, 0.46669),
(1, 1068, 1.02669),
(1, 1069, 1.09669),
(1, 1070, 0.28),
(1, 1071, 0.98),
(1, 1072, 1.09669),
(1, 1073, 0.84),
(1, 1074, 1.96),
(1, 1075, 1.23669),
(1, 1076, 1.23669),
(1, 1077, 0.56),
(1, 1078, 1.05),
(1, 1079, 1.09669),
(1, 1080, 0.88669),
(1, 1081, 0.95669),
(1, 1082, 0.91),
(1, 1083, 1.05),
(1, 1084, 1.05),
(1, 1085, 1.21331),
(1, 1086, 0.18669),
(1, 1087, 0.95669),
(1, 1088, 1.09669),
(1, 1089, 0.39996),
(1, 1090, 1.59996),
(1, 1091, 0.37331),
(1, 1092, 0.95669),
(1, 1093, 1.09669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms1`
--

CREATE TABLE `blog_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms1`
--

INSERT INTO `blog_finder_links_terms1` (`link_id`, `term_id`, `weight`) VALUES
(1, 30, 0.14),
(1, 31, 0.91),
(1, 32, 0.95669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms2`
--

CREATE TABLE `blog_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms2`
--

INSERT INTO `blog_finder_links_terms2` (`link_id`, `term_id`, `weight`) VALUES
(1, 543, 0.04669),
(1, 544, 0.91),
(1, 545, 0.98),
(1, 546, 0.09331),
(1, 547, 0.86331),
(1, 548, 1.12),
(1, 549, 0.65331),
(1, 550, 1.21331),
(1, 551, 1.28331),
(1, 552, 0.69993),
(1, 553, 0.86331),
(1, 554, 1.02669),
(1, 555, 0.88669),
(1, 556, 1.09669),
(1, 557, 1.02669),
(1, 558, 1.12),
(1, 636, 0.28),
(1, 637, 1.07331),
(1, 638, 1.28331),
(1, 639, 0.69993),
(1, 640, 1.05),
(1, 641, 1.14331),
(1, 642, 1.09669),
(1, 643, 1.28331),
(1, 644, 0.88669),
(1, 645, 1.23669),
(1, 646, 0.28),
(1, 647, 1.05),
(1, 648, 1.21331),
(1, 649, 0.65338),
(1, 650, 0.93331),
(1, 651, 1.02669),
(1, 652, 1.02669),
(1, 653, 1.26),
(1, 654, 0.28),
(1, 655, 0.91),
(1, 656, 1.14331),
(1, 657, 0.28),
(1, 658, 0.91),
(1, 659, 1.14331),
(1, 660, 0.37331),
(1, 661, 1.05),
(1, 662, 1.28331),
(1, 663, 0.46662),
(1, 664, 0.88669),
(1, 665, 1.02669),
(1, 666, 0.88669),
(1, 667, 1.02669),
(1, 668, 0.93338),
(1, 669, 1.14331),
(1, 670, 1.37669),
(1, 671, 1.09669),
(1, 672, 1.30669),
(1, 673, 0.23331),
(1, 674, 0.88669),
(1, 675, 1.14331),
(1, 676, 0.46662),
(1, 677, 0.88669),
(1, 678, 1.07331),
(1, 679, 0.86331),
(1, 680, 0.93331),
(1, 681, 0.23331),
(1, 682, 0.91),
(1, 683, 1.02669),
(1, 684, 4.96617),
(1, 685, 20.3638),
(1, 686, 2.8666),
(1, 687, 1.19),
(1, 688, 1.05),
(1, 689, 1.07331),
(1, 690, 1.09669),
(1, 691, 4.01324),
(1, 692, 1.09669),
(1, 693, 1.05),
(1, 694, 1.02669),
(1, 695, 1.05),
(1, 696, 1.14331),
(1, 697, 1.21331),
(1, 698, 1.09669),
(1, 699, 1.05);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms3`
--

CREATE TABLE `blog_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms3`
--

INSERT INTO `blog_finder_links_terms3` (`link_id`, `term_id`, `weight`) VALUES
(1, 1, 0.28),
(1, 2, 1.00331),
(1, 3, 1.16669),
(1, 634, 0.8),
(1, 635, 2.8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms4`
--

CREATE TABLE `blog_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms4`
--

INSERT INTO `blog_finder_links_terms4` (`link_id`, `term_id`, `weight`) VALUES
(1, 139, 0.7),
(1, 140, 1.14331),
(1, 141, 0.32669),
(1, 142, 0.93331),
(1, 143, 1.02669),
(1, 144, 0.56),
(1, 145, 1.02669),
(1, 146, 1.09669),
(1, 147, 1.00331),
(1, 148, 1.14331),
(1, 149, 0.23331),
(1, 150, 1.00331),
(1, 151, 1.12),
(1, 152, 0.37331),
(1, 153, 0.95669),
(1, 154, 1.14331),
(1, 155, 0.74662),
(1, 156, 1.05),
(1, 157, 1.12),
(1, 158, 1.12),
(1, 159, 1.16669),
(1, 160, 0.28),
(1, 161, 0.91),
(1, 162, 1.07331),
(1, 163, 0.32669),
(1, 164, 0.93331),
(1, 165, 1.14331),
(1, 166, 0.98),
(1, 167, 1.00331),
(1, 168, 1.21331),
(1, 169, 0.91),
(1, 170, 0.98),
(1, 171, 1.77338),
(1, 172, 1.91338),
(1, 173, 0.81669),
(1, 174, 1.02669),
(1, 175, 0.86331),
(1, 176, 0.95669),
(1, 177, 0.84),
(1, 178, 1.02669),
(1, 179, 0.42),
(1, 180, 1.00331),
(1, 181, 1.07331),
(1, 182, 0.56007),
(1, 183, 0.93331),
(1, 184, 1.16669),
(1, 185, 0.95669),
(1, 186, 1.02669),
(1, 187, 1.09669),
(1, 188, 1.26),
(1, 189, 0.37331),
(1, 190, 1.14331),
(1, 191, 1.33),
(1, 192, 0.42),
(1, 193, 0.98),
(1, 194, 1.12),
(1, 195, 0.51331),
(1, 196, 1.14331),
(1, 197, 1.21331),
(1, 198, 0.51331),
(1, 199, 1.14331),
(1, 200, 1.30669),
(1, 201, 0.46669),
(1, 202, 1.09669),
(1, 203, 1.28331),
(1, 204, 0.37331),
(1, 205, 1.12),
(1, 206, 1.19),
(1, 207, 1.11993),
(1, 208, 2.00662),
(1, 209, 1.21331),
(1, 210, 1.19),
(1, 211, 0.95669),
(1, 212, 1.09669),
(1, 213, 0.84),
(1, 214, 0.98),
(1, 215, 1.19),
(1, 216, 0.98),
(1, 217, 1.07331),
(1, 218, 0.56),
(1, 219, 1.05),
(1, 220, 1.16669),
(1, 978, 0.32669),
(1, 979, 0.93331),
(1, 980, 1.05),
(1, 981, 0.28),
(1, 982, 0.98),
(1, 983, 1.07331),
(1, 984, 0.42),
(1, 985, 1.00331),
(1, 986, 1.12),
(1, 987, 0.28),
(1, 988, 0.98),
(1, 989, 1.07331),
(1, 990, 0.37331),
(1, 991, 1.02669),
(1, 992, 1.09669),
(1, 993, 0.56),
(1, 994, 1.14331),
(1, 995, 1.30669),
(1, 996, 0.46669),
(1, 997, 0.98),
(1, 998, 1.19),
(1, 999, 0.46669),
(1, 1000, 1.16669),
(1, 1001, 1.23669),
(1, 1002, 0.32669),
(1, 1003, 1.05),
(1, 1004, 1.35331),
(1, 1005, 0.56),
(1, 1006, 1.14331),
(1, 1007, 1.21331),
(1, 1008, 0.51331),
(1, 1009, 1.12),
(1, 1010, 1.23669),
(1, 1011, 0.32669),
(1, 1012, 1.12),
(1, 1013, 1.19);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms5`
--

CREATE TABLE `blog_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms6`
--

CREATE TABLE `blog_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms6`
--

INSERT INTO `blog_finder_links_terms6` (`link_id`, `term_id`, `weight`) VALUES
(1, 373, 0.11339),
(1, 387, 2.09661),
(1, 388, 2.43661),
(1, 395, 0.81669),
(1, 396, 0.91),
(1, 397, 2.05338),
(1, 398, 1.09669),
(1, 399, 1.12),
(1, 400, 1.58662),
(1, 401, 0.91),
(1, 402, 0.98),
(1, 453, 0.23331),
(1, 454, 0.88669),
(1, 455, 1.14331),
(1, 484, 0.23331),
(1, 485, 0.95669),
(1, 486, 1.14331),
(1, 700, 0.32669),
(1, 701, 1.02669),
(1, 702, 1.16669),
(1, 703, 0.74676),
(1, 704, 1.72662),
(1, 705, 0.95669),
(1, 706, 0.98),
(1, 707, 1.00331),
(1, 708, 1.21331),
(1, 709, 1.09669),
(1, 710, 1.26),
(1, 711, 0.14),
(1, 712, 0.84),
(1, 713, 1.00331),
(1, 714, 0.65338),
(1, 715, 0.93331),
(1, 716, 1.09669),
(1, 717, 0.93331),
(1, 718, 1.14331),
(1, 719, 0.14),
(1, 720, 0.81669),
(1, 721, 1.00331),
(1, 722, 0.46669),
(1, 723, 1.00331),
(1, 724, 1.23669),
(1, 725, 0.18669),
(1, 726, 0.86331),
(1, 727, 1.00331),
(1, 728, 0.65338),
(1, 729, 1.86662),
(1, 730, 1.16669),
(1, 731, 1.07331),
(1, 732, 0.28),
(1, 733, 1.00331),
(1, 734, 1.21331),
(1, 735, 0.28),
(1, 736, 1.07331),
(1, 737, 1.35331),
(1, 738, 0.23331),
(1, 739, 1.00331),
(1, 740, 1.07331),
(1, 741, 0.28),
(1, 742, 1.07331),
(1, 743, 1.14331);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms7`
--

CREATE TABLE `blog_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms7`
--

INSERT INTO `blog_finder_links_terms7` (`link_id`, `term_id`, `weight`) VALUES
(1, 744, 0.65317),
(1, 745, 0.91),
(1, 746, 0.98),
(1, 747, 0.88669),
(1, 748, 0.95669),
(1, 749, 1.91338),
(1, 750, 1.09669),
(1, 751, 1.12),
(1, 752, 1.00331),
(1, 753, 1.21331),
(1, 754, 0.88669),
(1, 755, 1.07331),
(1, 756, 0.95669),
(1, 757, 1.19),
(1, 758, 0.7),
(1, 759, 1.14331),
(1, 760, 1.23669),
(1, 761, 0.81669),
(1, 762, 1.12),
(1, 763, 0.84),
(1, 764, 0.95669),
(1, 765, 0.95669),
(1, 766, 1.05),
(1, 767, 0.84),
(1, 768, 0.88669),
(1, 769, 0.46669),
(1, 770, 1.12),
(1, 771, 1.26),
(1, 772, 0.14),
(1, 773, 0.84),
(1, 774, 0.98),
(1, 775, 0.09331),
(1, 776, 0.86331),
(1, 777, 0.93331),
(1, 778, 0.28),
(1, 779, 0.91),
(1, 780, 1.07331),
(1, 781, 0.14),
(1, 782, 0.81669),
(1, 783, 0.98),
(1, 784, 0.23331),
(1, 785, 1.02669),
(1, 786, 1.09669),
(1, 956, 1.12),
(1, 957, 0.88669),
(1, 958, 1.09669),
(1, 959, 1.02669),
(1, 960, 1.28331),
(1, 961, 1.00331),
(1, 962, 1.05),
(1, 963, 0.91),
(1, 964, 1.14331),
(1, 965, 1.58),
(1, 966, 4.31679),
(1, 967, 5.05679),
(1, 968, 1.72662),
(1, 969, 0.95669),
(1, 970, 1.00331),
(1, 971, 1.72662),
(1, 972, 0.93331),
(1, 973, 1.05),
(1, 974, 0.84),
(1, 975, 1.05),
(1, 976, 0.84),
(1, 977, 1.00331),
(1, 1152, 1.11972),
(1, 1153, 1.00331),
(1, 1154, 1.09669),
(1, 1155, 0.86331),
(1, 1156, 1.09669),
(1, 1157, 0.98),
(1, 1158, 1.28331),
(1, 1159, 0.91),
(1, 1160, 0.98),
(1, 1161, 0.88669),
(1, 1162, 0.95669),
(1, 1163, 0.88669),
(1, 1164, 1.02669),
(1, 1165, 0.88669),
(1, 1166, 1.19),
(1, 1167, 0.98),
(1, 1168, 1.05),
(1, 1169, 0.93331),
(1, 1170, 1.00331),
(1, 1171, 1.77338),
(1, 1172, 1.07331),
(1, 1173, 1.12),
(1, 1174, 0.93331),
(1, 1175, 1.00331),
(1, 1176, 1.54),
(1, 1177, 1.00331),
(1, 1178, 1.19),
(1, 1179, 0.95669),
(1, 1180, 1.02669),
(1, 1181, 0.86331),
(1, 1182, 0.98),
(1, 1183, 1.07331),
(1, 1184, 1.23669),
(1, 1185, 0.91),
(1, 1186, 0.98),
(1, 1187, 0.88669),
(1, 1188, 0.95669),
(1, 1189, 0.93331),
(1, 1190, 1.09669),
(1, 1191, 1.96),
(1, 1192, 2.1),
(1, 1193, 0.93331),
(1, 1194, 1.07331),
(1, 1195, 1.00331),
(1, 1196, 1.07331),
(1, 1197, 0.14),
(1, 1198, 0.84),
(1, 1199, 1.09669),
(1, 1200, 0.23331),
(1, 1201, 0.95669),
(1, 1202, 1.00331),
(1, 1203, 0.18669),
(1, 1204, 0.93331),
(1, 1205, 1.07331),
(1, 1206, 0.28),
(1, 1207, 0.88669),
(1, 1208, 0.95669),
(1, 1209, 0.37338),
(1, 1210, 0.93331),
(1, 1211, 1.07331),
(1, 1212, 0.88669),
(1, 1213, 1.09669),
(1, 1214, 0.32004),
(1, 1215, 0.42),
(1, 1216, 1.05),
(1, 1217, 1.09669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms8`
--

CREATE TABLE `blog_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms8`
--

INSERT INTO `blog_finder_links_terms8` (`link_id`, `term_id`, `weight`) VALUES
(1, 221, 0.55986),
(1, 222, 0.88669),
(1, 223, 1.02669),
(1, 224, 0.98),
(1, 225, 1.26),
(1, 226, 0.95669),
(1, 227, 1.05),
(1, 228, 0.93331),
(1, 229, 1.00331),
(1, 230, 1.00331),
(1, 231, 1.05),
(1, 232, 0.95669),
(1, 233, 1.09669),
(1, 234, 0.42),
(1, 235, 0.88669),
(1, 236, 1.09669),
(1, 237, 0.93331),
(1, 238, 1.12),
(1, 239, 0.95669),
(1, 240, 1.07331),
(1, 241, 0.28),
(1, 242, 0.91),
(1, 243, 0.98),
(1, 244, 4.57219),
(1, 245, 0.84),
(1, 246, 1.05),
(1, 247, 0.86331),
(1, 248, 0.98),
(1, 249, 0.81669),
(1, 250, 0.88669),
(1, 251, 0.81669),
(1, 252, 0.98),
(1, 253, 0.98),
(1, 254, 1.09669),
(1, 255, 1.82),
(1, 256, 1.09669),
(1, 257, 1.07331),
(1, 258, 1.00331),
(1, 259, 1.16669),
(1, 260, 0.95669),
(1, 261, 1.07331),
(1, 262, 0.81669),
(1, 263, 1.00331),
(1, 264, 1.00331),
(1, 265, 1.26),
(1, 266, 0.98),
(1, 267, 1.05),
(1, 268, 1.86662),
(1, 269, 2.24),
(1, 270, 0.91),
(1, 271, 1.02669),
(1, 272, 0.88669),
(1, 273, 1.09669),
(1, 274, 0.98),
(1, 275, 1.02669),
(1, 276, 0.88669),
(1, 277, 0.95669),
(1, 278, 0.91),
(1, 279, 0.98),
(1, 280, 1.00331),
(1, 281, 1.16669),
(1, 282, 7.09352),
(1, 283, 8.21352),
(1, 284, 1.86662),
(1, 285, 2.00662),
(1, 286, 0.86331),
(1, 287, 0.95669),
(1, 288, 0.93331),
(1, 289, 1.00331),
(1, 290, 1.96),
(1, 291, 1.07331),
(1, 292, 1.05),
(1, 293, 0.98),
(1, 294, 1.21331),
(1, 295, 1.00331),
(1, 296, 1.05),
(1, 297, 0.84),
(1, 298, 0.93331),
(1, 299, 1.00331),
(1, 300, 1.23669),
(1, 301, 0.91),
(1, 302, 0.98),
(1, 303, 1.77338),
(1, 304, 0.95669),
(1, 305, 1.07331),
(1, 306, 1.77338),
(1, 307, 0.93331),
(1, 308, 1.02669),
(1, 309, 0.88669),
(1, 310, 0.98),
(1, 311, 0.81669),
(1, 312, 0.95669),
(1, 313, 2.52),
(1, 314, 0.98),
(1, 315, 1.00331),
(1, 316, 1.05),
(1, 317, 0.86331),
(1, 318, 1.00331),
(1, 319, 0.28),
(1, 320, 0.91),
(1, 321, 0.98),
(1, 322, 0.42),
(1, 323, 1.12),
(1, 324, 1.35331),
(1, 325, 0.42),
(1, 326, 0.95669),
(1, 327, 1.05),
(1, 328, 0.56),
(1, 329, 1.12),
(1, 330, 1.21331),
(1, 331, 0.46662),
(1, 332, 1.72662),
(1, 333, 2.05338),
(1, 334, 0.23331),
(1, 335, 0.86331),
(1, 336, 1.07331),
(1, 337, 0.51331),
(1, 338, 1.00331),
(1, 339, 1.21331),
(1, 340, 0.46669),
(1, 341, 1.02669),
(1, 342, 1.14331),
(1, 343, 0.51331),
(1, 344, 1.02669),
(1, 345, 1.26),
(1, 346, 0.42),
(1, 347, 1.00331),
(1, 348, 1.05),
(1, 349, 0.56),
(1, 350, 1.14331),
(1, 351, 1.21331),
(1, 352, 0.65317),
(1, 353, 0.84),
(1, 354, 0.88669),
(1, 355, 0.81669),
(1, 356, 1.00331),
(1, 357, 0.91),
(1, 358, 0.98),
(1, 359, 1.09669),
(1, 360, 1.28331),
(1, 361, 0.84),
(1, 362, 0.91),
(1, 363, 0.84),
(1, 364, 0.91),
(1, 365, 0.88669),
(1, 366, 1.02669),
(1, 367, 0.23331),
(1, 368, 0.91),
(1, 369, 1.02669),
(1, 370, 0.14),
(1, 371, 0.88669),
(1, 372, 1.21331),
(1, 502, 0.18669),
(1, 503, 0.86331),
(1, 504, 1.12),
(1, 505, 0.18669),
(1, 506, 1.02669),
(1, 507, 1.09669),
(1, 508, 0.65338),
(1, 509, 0.95669),
(1, 510, 1.05),
(1, 511, 1.00331),
(1, 512, 1.05),
(1, 513, 0.23331),
(1, 514, 0.88669),
(1, 515, 1.02669),
(1, 516, 0.23331),
(1, 517, 0.91),
(1, 518, 0.95669),
(1, 519, 0.65338),
(1, 520, 2.1),
(1, 521, 2.24),
(1, 522, 0.23331),
(1, 523, 1.12),
(1, 524, 1.28331),
(1, 525, 0.23331),
(1, 526, 0.95669),
(1, 527, 1.16669),
(1, 528, 0.28),
(1, 529, 0.95669),
(1, 530, 1.09669),
(1, 559, 0.42),
(1, 560, 1.21331),
(1, 561, 1.35331),
(1, 562, 0.42),
(1, 563, 0.95669),
(1, 564, 1.05),
(1, 565, 0.46669),
(1, 566, 1.00331),
(1, 567, 1.28331),
(1, 568, 0.6666),
(1, 569, 0.51331),
(1, 570, 1.12),
(1, 571, 1.26),
(1, 572, 0.42),
(1, 573, 1.19),
(1, 574, 1.23669),
(1, 575, 0.28),
(1, 576, 0.91),
(1, 577, 1.12),
(1, 578, 0.42),
(1, 579, 1.05),
(1, 580, 1.09669),
(1, 581, 0.37331),
(1, 582, 0.95669),
(1, 583, 1.19),
(1, 584, 0.42),
(1, 585, 0.98),
(1, 586, 1.12),
(1, 587, 0.42),
(1, 588, 0.95669),
(1, 589, 1.19),
(1, 590, 0.56),
(1, 591, 1.14331),
(1, 592, 1.33),
(1, 593, 1.11993),
(1, 594, 1.02669),
(1, 595, 1.14331),
(1, 596, 0.98),
(1, 597, 1.07331),
(1, 598, 1.05),
(1, 599, 1.09669),
(1, 600, 4.96617),
(1, 601, 2.4666),
(1, 602, 1.05),
(1, 603, 1.12),
(1, 604, 0.91),
(1, 605, 1.14331),
(1, 606, 0.93331),
(1, 607, 1.23669),
(1, 608, 0.95669),
(1, 609, 1.05),
(1, 610, 3.45324),
(1, 611, 0.98),
(1, 612, 1.12),
(1, 613, 0.95669),
(1, 614, 1.16669),
(1, 615, 0.95669),
(1, 616, 1.09669),
(1, 617, 0.91),
(1, 618, 0.95669),
(1, 619, 0.88669),
(1, 620, 1.09669),
(1, 621, 0.91),
(1, 622, 0.95669),
(1, 623, 1.00331),
(1, 624, 1.09669),
(1, 625, 1.07331),
(1, 626, 1.16669),
(1, 627, 0.95669),
(1, 628, 1.09669),
(1, 629, 0.91),
(1, 630, 1.00331),
(1, 631, 0.28),
(1, 632, 1.00331),
(1, 633, 1.16669),
(1, 836, 0.56),
(1, 837, 0.88669),
(1, 838, 0.98),
(1, 839, 1.05),
(1, 840, 1.12),
(1, 841, 0.42),
(1, 842, 1.07331),
(1, 843, 1.16669),
(1, 844, 0.28),
(1, 845, 1.00331),
(1, 846, 1.19),
(1, 847, 0.32669),
(1, 848, 0.93331),
(1, 849, 1.14331),
(1, 850, 0.32669),
(1, 851, 0.93331),
(1, 852, 1.19),
(1, 853, 1.49324),
(1, 854, 1.14331),
(1, 855, 1.23669),
(1, 856, 1.91338),
(1, 857, 2.19338),
(1, 858, 0.98),
(1, 859, 1.35331),
(1, 860, 0.93345),
(1, 861, 0.84),
(1, 862, 1.09669),
(1, 863, 0.93331),
(1, 864, 1.00331),
(1, 865, 0.93331),
(1, 866, 1.00331),
(1, 867, 0.84),
(1, 868, 1.00331),
(1, 869, 0.86331),
(1, 870, 1.16669),
(1, 871, 0.32669),
(1, 872, 0.95669),
(1, 873, 1.02669),
(1, 874, 0.74662),
(1, 875, 1.91338),
(1, 876, 2.19338),
(1, 877, 1.26),
(1, 878, 1.00331),
(1, 879, 1.14331),
(1, 880, 1.96),
(1, 881, 2.24),
(1, 882, 0.28),
(1, 883, 0.88669),
(1, 884, 0.98),
(1, 885, 0.32669),
(1, 886, 1.16669),
(1, 887, 1.37669),
(1, 888, 0.23331),
(1, 889, 0.91),
(1, 890, 1.09669),
(1, 891, 0.56),
(1, 892, 1.33),
(1, 893, 1.4),
(1, 894, 0.42),
(1, 895, 1.00331),
(1, 896, 1.09669),
(1, 897, 0.23331),
(1, 898, 0.91),
(1, 899, 1.19),
(1, 900, 0.32669),
(1, 901, 0.93331),
(1, 902, 1.07331),
(1, 903, 0.51331),
(1, 904, 1.02669),
(1, 905, 1.19),
(1, 906, 0.7),
(1, 907, 0.93331),
(1, 908, 1.09669),
(1, 909, 0.98),
(1, 910, 1.02669),
(1, 911, 0.91),
(1, 912, 1.12),
(1, 913, 0.81669),
(1, 914, 1.00331),
(1, 915, 0.98),
(1, 916, 1.05),
(1, 917, 0.28),
(1, 918, 0.93331),
(1, 919, 0.98),
(1, 920, 0.23331),
(1, 921, 1.09669),
(1, 922, 1.28331),
(1, 923, 0.23331),
(1, 924, 1.12),
(1, 925, 1.19),
(1, 926, 0.32669),
(1, 927, 0.95669),
(1, 928, 1.14331),
(1, 929, 0.56),
(1, 930, 1.19),
(1, 931, 1.4),
(1, 932, 0.37331),
(1, 933, 1.00331),
(1, 934, 1.09669),
(1, 935, 0.37331),
(1, 936, 1.02669),
(1, 937, 1.09669),
(1, 938, 0.37331),
(1, 939, 1.07331),
(1, 940, 1.14331),
(1, 941, 0.46669),
(1, 942, 1.02669),
(1, 943, 1.16669),
(1, 944, 0.42),
(1, 945, 0.98),
(1, 946, 1.12),
(1, 947, 0.42),
(1, 948, 1.14331),
(1, 949, 1.28331),
(1, 950, 0.46669),
(1, 951, 0.98),
(1, 952, 1.19),
(1, 953, 0.32669),
(1, 954, 0.93331),
(1, 955, 1.12);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_terms9`
--

CREATE TABLE `blog_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_terms9`
--

INSERT INTO `blog_finder_links_terms9` (`link_id`, `term_id`, `weight`) VALUES
(1, 127, 0.32669),
(1, 128, 0.93331),
(1, 129, 1.07331),
(1, 130, 0.14),
(1, 131, 0.81669),
(1, 132, 0.88669),
(1, 133, 0.65338),
(1, 134, 1.86662),
(1, 135, 2.24),
(1, 136, 0.23331),
(1, 137, 0.91),
(1, 138, 1.07331),
(1, 1218, 0.37331),
(1, 1219, 0.95669),
(1, 1220, 1.07331),
(1, 1221, 0.42),
(1, 1222, 1.19),
(1, 1223, 1.26),
(1, 1224, 0.28),
(1, 1225, 1.02669),
(1, 1226, 1.16669),
(1, 1227, 0.28),
(1, 1228, 1.05),
(1, 1229, 1.19),
(1, 1230, 0.56),
(1, 1231, 0.84),
(1, 1232, 1.07331),
(1, 1233, 0.86331),
(1, 1234, 1.02669),
(1, 1235, 0.84),
(1, 1236, 0.93331),
(1, 1237, 0.91),
(1, 1238, 1.14331),
(1, 1239, 0.65338),
(1, 1240, 1.00331),
(1, 1241, 1.21331),
(1, 1242, 0.98),
(1, 1243, 1.02669),
(1, 1244, 0.23331),
(1, 1245, 0.88669),
(1, 1246, 1.12),
(1, 1247, 0.37331),
(1, 1248, 1.12),
(1, 1249, 1.21331),
(1, 1250, 0.18669),
(1, 1251, 1.00331),
(1, 1252, 1.12),
(1, 1256, 0.28),
(1, 1257, 0.88669),
(1, 1258, 1.16669),
(1, 1259, 0.93331),
(1, 1260, 1.00331);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termsa`
--

CREATE TABLE `blog_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termsa`
--

INSERT INTO `blog_finder_links_termsa` (`link_id`, `term_id`, `weight`) VALUES
(1, 24, 0.14),
(1, 25, 0.81669),
(1, 26, 1.00331),
(1, 27, 0.18669),
(1, 28, 0.91),
(1, 29, 1.05);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termsb`
--

CREATE TABLE `blog_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termsb`
--

INSERT INTO `blog_finder_links_termsb` (`link_id`, `term_id`, `weight`) VALUES
(1, 531, 0.28),
(1, 532, 0.91),
(1, 533, 1.02669),
(1, 534, 0.32669),
(1, 535, 0.93331),
(1, 536, 1.07331),
(1, 537, 0.42),
(1, 538, 0.98),
(1, 539, 1.12),
(1, 540, 0.23331),
(1, 541, 0.88669),
(1, 542, 1.02669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termsc`
--

CREATE TABLE `blog_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termsc`
--

INSERT INTO `blog_finder_links_termsc` (`link_id`, `term_id`, `weight`) VALUES
(1, 4, 1.47),
(1, 5, 1.82),
(1, 6, 2.19338),
(1, 7, 0.91),
(1, 8, 1.07331),
(1, 9, 0.98),
(1, 10, 0.93331),
(1, 11, 1.00331),
(1, 12, 0.93331),
(1, 13, 1.00331),
(1, 14, 0.28),
(1, 15, 0.91),
(1, 16, 1.05),
(1, 17, 0.2),
(1, 18, 0.21),
(1, 19, 0.98),
(1, 20, 1.05),
(1, 21, 0.28),
(1, 22, 0.91),
(1, 23, 1.02669),
(1, 33, 0.17),
(1, 121, 0.09331),
(1, 122, 0.88669),
(1, 123, 0.95669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termsd`
--

CREATE TABLE `blog_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termsd`
--

INSERT INTO `blog_finder_links_termsd` (`link_id`, `term_id`, `weight`) VALUES
(1, 787, 0.57362),
(1, 788, 0.93331),
(1, 789, 1.00331),
(1, 790, 0.88669),
(1, 791, 0.95669),
(1, 792, 0.93331),
(1, 793, 1.12),
(1, 794, 4.31679),
(1, 795, 4.56321),
(1, 796, 1.77338),
(1, 797, 1.96),
(1, 798, 0.88669),
(1, 799, 1.05),
(1, 800, 0.88669),
(1, 801, 0.93331),
(1, 802, 0.18669),
(1, 803, 0.86331),
(1, 804, 1.12),
(1, 805, 0.37331),
(1, 806, 1.09669),
(1, 807, 1.16669),
(1, 808, 0.37338),
(1, 809, 0.88669),
(1, 810, 0.98),
(1, 811, 0.93331),
(1, 812, 1.21331),
(1, 813, 0.28),
(1, 814, 0.98),
(1, 815, 1.12),
(1, 816, 0.74662),
(1, 817, 1.16669),
(1, 818, 1.26),
(1, 819, 0.95669),
(1, 820, 1.09669),
(1, 821, 0.32669),
(1, 822, 0.93331),
(1, 823, 1.02669),
(1, 824, 0.37324),
(1, 825, 1.00331),
(1, 826, 1.12),
(1, 827, 0.95669),
(1, 828, 1.02669),
(1, 829, 0.98),
(1, 830, 1.05),
(1, 831, 1.05),
(1, 832, 1.21331),
(1, 833, 0.09331),
(1, 834, 0.95669),
(1, 835, 1.21331);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termse`
--

CREATE TABLE `blog_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termse`
--

INSERT INTO `blog_finder_links_termse` (`link_id`, `term_id`, `weight`) VALUES
(1, 373, 0.92713),
(1, 374, 1.82),
(1, 375, 0.98),
(1, 376, 0.98),
(1, 377, 0.79331),
(1, 378, 0.93331),
(1, 379, 0.88669),
(1, 380, 0.95669),
(1, 381, 0.79331),
(1, 382, 1.02669),
(1, 383, 0.79331),
(1, 384, 0.88669),
(1, 385, 0.93331),
(1, 386, 1.00331),
(1, 387, 2.4666),
(1, 388, 2.8666),
(1, 389, 0.84),
(1, 390, 1.14331),
(1, 391, 0.79331),
(1, 392, 1.05),
(1, 393, 0.98),
(1, 394, 1.07331),
(1, 403, 0.81669),
(1, 404, 0.98),
(1, 405, 0.81669),
(1, 406, 0.95669),
(1, 407, 0.93338),
(1, 408, 2.38),
(1, 409, 1.30669),
(1, 410, 1.4),
(1, 411, 0.37331),
(1, 412, 0.95669),
(1, 413, 1.14331),
(1, 414, 0.56),
(1, 415, 0.86331),
(1, 416, 0.95669),
(1, 417, 0.91),
(1, 418, 1.00331),
(1, 419, 0.86331),
(1, 420, 0.93331),
(1, 421, 0.86331),
(1, 422, 0.93331),
(1, 423, 0.93338),
(1, 424, 1.05),
(1, 425, 1.19),
(1, 426, 1.23669),
(1, 427, 1.4),
(1, 428, 0.65317),
(1, 429, 0.86331),
(1, 430, 0.98),
(1, 431, 0.86331),
(1, 432, 0.98),
(1, 433, 1.77338),
(1, 434, 1.12),
(1, 435, 1.16669),
(1, 436, 1.68),
(1, 437, 0.95669),
(1, 438, 0.98),
(1, 439, 0.84),
(1, 440, 0.95669),
(1, 441, 0.46669),
(1, 442, 1.12),
(1, 443, 1.35331),
(1, 444, 0.46669),
(1, 445, 1.05),
(1, 446, 1.19),
(1, 447, 0.51331),
(1, 448, 1.02669),
(1, 449, 1.12),
(1, 450, 0.23331),
(1, 451, 1.02669),
(1, 452, 1.09669),
(1, 456, 0.42),
(1, 457, 0.98),
(1, 458, 1.09669),
(1, 459, 0.65338),
(1, 460, 0.93331),
(1, 461, 1.05),
(1, 462, 1.09669),
(1, 463, 1.16669),
(1, 464, 0.65331),
(1, 465, 1.28331),
(1, 466, 1.35331),
(1, 467, 0.18669),
(1, 468, 1.07331),
(1, 469, 1.23669),
(1, 470, 0.18669),
(1, 471, 0.93331),
(1, 472, 0.98),
(1, 473, 0.32669),
(1, 474, 1.12),
(1, 475, 1.21331),
(1, 476, 0.42),
(1, 477, 0.98),
(1, 478, 1.14331),
(1, 479, 0.18662),
(1, 480, 1.86662),
(1, 481, 1.00331),
(1, 482, 1.00331),
(1, 483, 0.14),
(1, 487, 0.60669),
(1, 488, 1.09669),
(1, 489, 1.30669),
(1, 490, 0.46669),
(1, 491, 1.02669),
(1, 492, 1.30669),
(1, 493, 0.23331),
(1, 494, 1.02669),
(1, 495, 1.30669),
(1, 496, 0.32669),
(1, 497, 1.02669),
(1, 498, 1.26),
(1, 499, 0.37331),
(1, 500, 0.95669),
(1, 501, 1.05),
(1, 1094, 0.56),
(1, 1095, 0.91),
(1, 1096, 1.05),
(1, 1097, 0.98),
(1, 1098, 1.28331),
(1, 1099, 0.14),
(1, 1100, 0.86331),
(1, 1101, 1.16669),
(1, 1102, 0.28),
(1, 1103, 0.88669),
(1, 1104, 1.07331),
(1, 1105, 0.23331),
(1, 1106, 0.91),
(1, 1107, 1.14331),
(1, 1108, 0.28),
(1, 1109, 0.91),
(1, 1110, 1.05),
(1, 1111, 0.14),
(1, 1112, 0.95669),
(1, 1113, 1.02669),
(1, 1114, 1.16655),
(1, 1115, 0.88669),
(1, 1116, 1.12),
(1, 1117, 2.00662),
(1, 1118, 1.07331),
(1, 1119, 1.16669),
(1, 1120, 1.05),
(1, 1121, 1.14331),
(1, 1122, 0.95669),
(1, 1123, 1.02669),
(1, 1124, 0.56),
(1, 1125, 1.14331),
(1, 1126, 1.19),
(1, 1127, 0.18669),
(1, 1128, 0.86331),
(1, 1129, 1.09669),
(1, 1130, 0.51331),
(1, 1131, 1.00331),
(1, 1132, 1.07331),
(1, 1133, 0.46662),
(1, 1134, 0.86331),
(1, 1135, 0.93331),
(1, 1136, 0.95669),
(1, 1137, 1.09669),
(1, 1138, 0.46662),
(1, 1139, 0.88669),
(1, 1140, 1.12),
(1, 1141, 0.91),
(1, 1142, 0.98),
(1, 1143, 0.37331),
(1, 1144, 1.02669),
(1, 1145, 1.12),
(1, 1146, 0.32669),
(1, 1147, 0.93331),
(1, 1148, 1.09669),
(1, 1149, 0.28),
(1, 1150, 1.00331),
(1, 1151, 1.21331);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_links_termsf`
--

CREATE TABLE `blog_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_links_termsf`
--

INSERT INTO `blog_finder_links_termsf` (`link_id`, `term_id`, `weight`) VALUES
(1, 1253, 0.37331),
(1, 1254, 1.02669),
(1, 1255, 1.09669);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_taxonomy`
--

CREATE TABLE `blog_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_taxonomy`
--

INSERT INTO `blog_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0),
(2, 1, 'Type', 1, 1, 0),
(3, 2, 'Article', 1, 1, 0),
(4, 1, 'Author', 1, 1, 0),
(5, 4, 'Super User', 1, 1, 0),
(6, 1, 'Category', 1, 1, 0),
(7, 6, 'Blog', 1, 1, 0),
(8, 1, 'Language', 1, 1, 0),
(9, 8, '*', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_taxonomy_map`
--

CREATE TABLE `blog_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_taxonomy_map`
--

INSERT INTO `blog_finder_taxonomy_map` (`link_id`, `node_id`) VALUES
(1, 3),
(1, 5),
(1, 7),
(1, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_terms`
--

CREATE TABLE `blog_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `weight` float UNSIGNED NOT NULL DEFAULT 0,
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT 0,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_terms`
--

INSERT INTO `blog_finder_terms` (`term_id`, `term`, `stem`, `common`, `phrase`, `weight`, `soundex`, `links`, `language`) VALUES
(1, '\'lorem', 'lorem', 0, 0, 0.4, 'L650', 1, '*'),
(2, '\'lorem ipsum\'', 'lorem ipsum', 0, 1, 1.4333, 'L65125', 1, '*'),
(3, '\'lorem ipsum\' mostra', 'lorem ipsum mostra', 0, 1, 1.6667, 'L65125236', 1, '*'),
(4, '1.10.32', '1.10.32', 0, 0, 0.7, '', 1, '*'),
(5, '1.10.32 e', '1.10.32 e', 0, 1, 1.3, 'E000', 1, '*'),
(6, '1.10.32 e 1.10.33', '1.10.32 e 1.10.33', 0, 1, 1.5667, 'E000', 1, '*'),
(7, '1.10.32 o', '1.10.32 o', 0, 1, 1.3, 'O000', 1, '*'),
(8, '1.10.32 o trecho', '1.10.32 o trecho', 0, 1, 1.5333, 'O362', 1, '*'),
(9, '1.10.33', '1.10.33', 0, 0, 0.7, '', 1, '*'),
(10, '1.10.33 de', '1.10.33 de', 0, 1, 1.3333, 'D000', 1, '*'),
(11, '1.10.33 de de', '1.10.33 de de', 0, 1, 1.4333, 'D000', 1, '*'),
(12, '1.10.33 do', '1.10.33 do', 0, 1, 1.3333, 'D000', 1, '*'),
(13, '1.10.33 do de', '1.10.33 do de', 0, 1, 1.4333, 'D000', 1, '*'),
(14, '1914', '1914', 0, 0, 0.4, '', 1, '*'),
(15, '1914 onde', '1914 onde', 0, 1, 1.3, 'O530', 1, '*'),
(16, '1914 onde posso', '1914 onde posso', 0, 1, 1.5, 'O5312', 1, '*'),
(17, '2', '2', 0, 0, 0.1, '', 1, '*'),
(18, '200', '200', 0, 0, 0.3, '', 1, '*'),
(19, '200 palavras', '200 palavras', 0, 1, 1.4, 'P4162', 1, '*'),
(20, '200 palavras em', '200 palavras em', 0, 1, 1.5, 'P41625', 1, '*'),
(21, '2000', '2000', 0, 0, 0.4, '', 1, '*'),
(22, '2000 anos', '2000 anos', 0, 1, 1.3, 'A520', 1, '*'),
(23, '2000 anos suas', '2000 anos suas', 0, 1, 1.4667, 'A520', 1, '*'),
(24, '45', '45', 0, 0, 0.2, '', 1, '*'),
(25, '45 ac', '45 ac', 0, 1, 1.1667, 'A200', 1, '*'),
(26, '45 ac richard', '45 ac richard', 0, 1, 1.4333, 'A26263', 1, '*'),
(27, '45ac', '45ac', 0, 0, 0.2667, 'A200', 1, '*'),
(28, '45ac este', '45ac este', 0, 1, 1.3, 'A230', 1, '*'),
(29, '45ac este livro', '45ac este livro', 0, 1, 1.5, 'A23416', 1, '*'),
(30, '60', '60', 0, 0, 0.2, '', 1, '*'),
(31, '60 quando', '60 quando', 0, 1, 1.3, 'Q530', 1, '*'),
(32, '60 quando a', '60 quando a', 0, 1, 1.3667, 'Q530', 1, '*'),
(33, '8', '8', 0, 0, 0.1, '', 1, '*'),
(34, 'a', 'a', 0, 0, 0.0667, 'A000', 1, '*'),
(35, 'a cinco', 'a cinco', 0, 1, 1.2333, 'A252', 1, '*'),
(36, 'a cinco séculos', 'a cinco séculos', 0, 1, 1.5, 'A25242', 1, '*'),
(37, 'a de', 'a de', 0, 1, 1.1333, 'A300', 1, '*'),
(38, 'a de um', 'a de um', 0, 1, 1.2333, 'A350', 1, '*'),
(39, 'a editoração', 'a editoração', 0, 1, 1.4, 'A360', 1, '*'),
(40, 'a editoração eletrônica', 'a editoração eletrônica', 0, 1, 1.7667, 'A3643652', 1, '*'),
(41, 'a letraset', 'a letraset', 0, 1, 1.3333, 'A43623', 1, '*'),
(42, 'a letraset lançou', 'a letraset lançou', 0, 1, 1.5667, 'A4362345', 1, '*'),
(43, 'a maioria', 'a maioria', 0, 1, 1.3, 'A560', 1, '*'),
(44, 'a maioria sofreu', 'a maioria sofreu', 0, 1, 1.5333, 'A56216', 1, '*'),
(45, 'a primeira', 'a primeira', 0, 1, 1.3333, 'A1656', 1, '*'),
(46, 'a primeira linha', 'a primeira linha', 0, 1, 1.5333, 'A165645', 1, '*'),
(47, 'a repetir', 'a repetir', 0, 1, 1.3, 'A6136', 1, '*'),
(48, 'a repetir pedaços', 'a repetir pedaços', 0, 1, 1.5667, 'A6136132', 1, '*'),
(49, 'a ser', 'a ser', 0, 1, 1.1667, 'A260', 1, '*'),
(50, 'a ser integrado', 'a ser integrado', 0, 1, 1.5, 'A2653263', 1, '*'),
(51, 'a softwares', 'a softwares', 0, 1, 1.3667, 'A21362', 1, '*'),
(52, 'a softwares de', 'a softwares de', 0, 1, 1.4667, 'A213623', 1, '*'),
(53, 'a sua', 'a sua', 0, 1, 1.1667, 'A200', 1, '*'),
(54, 'a sua indubitável', 'a sua indubitável', 0, 1, 1.5667, 'A2531314', 1, '*'),
(55, 'a vantagem', 'a vantagem', 0, 1, 1.3333, 'A15325', 1, '*'),
(56, 'a vantagem de', 'a vantagem de', 0, 1, 1.4333, 'A153253', 1, '*'),
(57, 'abaixo', 'abaixo', 0, 0, 0.4, 'A120', 1, '*'),
(58, 'abaixo em', 'abaixo em', 0, 1, 1.3, 'A125', 1, '*'),
(59, 'abaixo em sua', 'abaixo em sua', 0, 1, 1.4333, 'A1252', 1, '*'),
(60, 'abaixo para', 'abaixo para', 0, 1, 1.3667, 'A1216', 1, '*'),
(61, 'abaixo para os', 'abaixo para os', 0, 1, 1.4667, 'A12162', 1, '*'),
(62, 'ac', 'ac', 0, 0, 0.1333, 'A200', 1, '*'),
(63, 'ac richard', 'ac richard', 0, 1, 1.3333, 'A26263', 1, '*'),
(64, 'ac richard mcclintock', 'ac richard mcclintock', 0, 1, 1.7, 'A26263524532', 1, '*'),
(65, 'acidente', 'acidente', 0, 0, 0.5333, 'A2353', 1, '*'),
(66, 'acidente e', 'acidente e', 0, 1, 1.3333, 'A2353', 1, '*'),
(67, 'acidente e às', 'acidente e às', 0, 1, 1.4333, 'A23532', 1, '*'),
(68, 'acompanhada', 'acompanhada', 0, 0, 0.7333, 'A25153', 1, '*'),
(69, 'acompanhada das', 'acompanhada das', 0, 1, 1.5, 'A251532', 1, '*'),
(70, 'acompanhada das versões', 'acompanhada das versões', 0, 1, 1.7667, 'A251532162', 1, '*'),
(71, 'acredita', 'acredita', 0, 0, 0.5333, 'A263', 1, '*'),
(72, 'acredita lorem', 'acredita lorem', 0, 1, 1.4667, 'A263465', 1, '*'),
(73, 'acredita lorem ipsum', 'acredita lorem ipsum', 0, 1, 1.6667, 'A263465125', 1, '*'),
(74, 'agora', 'agora', 0, 0, 0.3333, 'A260', 1, '*'),
(75, 'agora usam', 'agora usam', 0, 1, 1.3333, 'A2625', 1, '*'),
(76, 'agora usam lorem', 'agora usam lorem', 0, 1, 1.5333, 'A2625465', 1, '*'),
(77, 'ainda', 'ainda', 0, 0, 0.3333, 'A530', 1, '*'),
(78, 'ainda em', 'ainda em', 0, 1, 1.2667, 'A535', 1, '*'),
(79, 'ainda em sua', 'ainda em sua', 0, 1, 1.4, 'A5352', 1, '*'),
(80, 'aldus', 'aldus', 0, 0, 0.3333, 'A432', 1, '*'),
(81, 'aldus pagemaker', 'aldus pagemaker', 0, 1, 1.5, 'A43212526', 1, '*'),
(82, 'aldus pagemaker porque', 'aldus pagemaker porque', 0, 1, 1.7333, 'A43212526162', 1, '*'),
(83, 'aleatórias', 'aleatórias', 0, 0, 0.6667, 'A4362', 1, '*'),
(84, 'aleatórias que', 'aleatórias que', 0, 1, 1.4667, 'A4362', 1, '*'),
(85, 'aleatórias que não', 'aleatórias que não', 0, 1, 1.6, 'A43625', 1, '*'),
(86, 'algo', 'algo', 0, 0, 0.2667, 'A420', 1, '*'),
(87, 'algo embaraçoso', 'algo embaraçoso', 0, 1, 1.5, 'A425162', 1, '*'),
(88, 'algo embaraçoso escrito', 'algo embaraçoso escrito', 0, 1, 1.7667, 'A42516263', 1, '*'),
(89, 'algum', 'algum', 0, 0, 0.3333, 'A425', 1, '*'),
(90, 'algum tipo', 'algum tipo', 0, 1, 1.3333, 'A42531', 1, '*'),
(91, 'algum tipo de', 'algum tipo de', 0, 1, 1.4333, 'A425313', 1, '*'),
(92, 'alteração', 'alteração', 0, 0, 0.6, 'A436', 1, '*'),
(93, 'alteração seja', 'alteração seja', 0, 1, 1.4667, 'A4362', 1, '*'),
(94, 'alteração seja por', 'alteração seja por', 0, 1, 1.6, 'A436216', 1, '*'),
(95, 'amet', 'amet', 0, 0, 0.2667, 'A530', 1, '*'),
(96, 'amet vem', 'amet vem', 0, 1, 1.2667, 'A5315', 1, '*'),
(97, 'amet vem de', 'amet vem de', 0, 1, 1.3667, 'A53153', 1, '*'),
(98, 'anos', 'anos', 0, 0, 0.2667, 'A520', 1, '*'),
(99, 'anos eventualmente', 'anos eventualmente', 0, 1, 1.6, 'A52153453', 1, '*'),
(100, 'anos eventualmente por', 'anos eventualmente por', 0, 1, 1.7333, 'A5215345316', 1, '*'),
(101, 'anos suas', 'anos suas', 0, 1, 1.3, 'A520', 1, '*'),
(102, 'anos suas raízes', 'anos suas raízes', 0, 1, 1.5333, 'A5262', 1, '*'),
(103, 'ao', 'ao', 0, 0, 0.1333, 'A000', 1, '*'),
(104, 'ao contrário', 'ao contrário', 0, 1, 1.4, 'A2536', 1, '*'),
(105, 'ao contrário de', 'ao contrário de', 0, 1, 1.5, 'A25363', 1, '*'),
(106, 'ao contrário do', 'ao contrário do', 0, 1, 1.5, 'A25363', 1, '*'),
(107, 'ao longo', 'ao longo', 0, 1, 1.2667, 'A452', 1, '*'),
(108, 'ao longo dos', 'ao longo dos', 0, 1, 1.4, 'A45232', 1, '*'),
(109, 'ao salto', 'ao salto', 0, 1, 1.2667, 'A243', 1, '*'),
(110, 'ao salto para', 'ao salto para', 0, 1, 1.4333, 'A24316', 1, '*'),
(111, 'aparência', 'aparência', 0, 0, 0.6, 'A1652', 1, '*'),
(112, 'aparência razoável', 'aparência razoável', 0, 1, 1.6, 'A16526214', 1, '*'),
(113, 'aparência razoável livre', 'aparência razoável livre', 0, 1, 1.8, 'A1652621416', 1, '*'),
(114, 'aparência similar', 'aparência similar', 0, 1, 1.5667, 'A1652546', 1, '*'),
(115, 'aparência similar a', 'aparência similar a', 0, 1, 1.6333, 'A1652546', 1, '*'),
(116, 'aqui', 'aqui', 0, 0, 0.2667, 'A200', 1, '*'),
(117, 'aqui conteúdo', 'aqui conteúdo', 0, 1, 1.4333, 'A253', 1, '*'),
(118, 'aqui conteúdo aqui', 'aqui conteúdo aqui', 0, 1, 1.6, 'A2532', 1, '*'),
(119, 'aqui fazendo', 'aqui fazendo', 0, 1, 1.4, 'A21253', 1, '*'),
(120, 'aqui fazendo com', 'aqui fazendo com', 0, 1, 1.5333, 'A2125325', 1, '*'),
(121, 'às', 'às', 0, 0, 0.1333, 'à200', 1, '*'),
(122, 'às vezes', 'às vezes', 0, 1, 1.2667, 'à212', 1, '*'),
(123, 'às vezes de', 'às vezes de', 0, 1, 1.3667, 'à2123', 1, '*'),
(124, 'autêntico', 'autêntico', 0, 0, 0.6, 'A3532', 1, '*'),
(125, 'autêntico da', 'autêntico da', 0, 1, 1.4, 'A35323', 1, '*'),
(126, 'autêntico da internet', 'autêntico da internet', 0, 1, 1.7, 'A3532353653', 1, '*'),
(127, 'bandeja', 'bandeja', 0, 0, 0.4667, 'B532', 1, '*'),
(128, 'bandeja de', 'bandeja de', 0, 1, 1.3333, 'B5323', 1, '*'),
(129, 'bandeja de tipos', 'bandeja de tipos', 0, 1, 1.5333, 'B532312', 1, '*'),
(130, 'bem', 'bem', 0, 0, 0.2, 'B500', 1, '*'),
(131, 'bem e', 'bem e', 0, 1, 1.1667, 'B500', 1, '*'),
(132, 'bem e do', 'bem e do', 0, 1, 1.2667, 'B530', 1, '*'),
(133, 'bonorum', 'bonorum', 0, 0, 0.4667, 'B565', 1, '*'),
(134, 'bonorum et', 'bonorum et', 0, 1, 1.3333, 'B5653', 1, '*'),
(135, 'bonorum et malorum', 'bonorum et malorum', 0, 1, 1.6, 'B56535465', 1, '*'),
(136, 'busca', 'busca', 0, 0, 0.3333, 'B200', 1, '*'),
(137, 'busca por', 'busca por', 0, 1, 1.3, 'B216', 1, '*'),
(138, 'busca por \'lorem', 'busca por lorem', 0, 1, 1.5333, 'B216465', 1, '*'),
(139, 'características', 'características', 0, 0, 1, 'C6236232', 1, '*'),
(140, 'características etc', 'características etc', 0, 1, 1.6333, 'C623623232', 1, '*'),
(141, 'certeza', 'certeza', 0, 0, 0.4667, 'C632', 1, '*'),
(142, 'certeza de', 'certeza de', 0, 1, 1.3333, 'C6323', 1, '*'),
(143, 'certeza de que', 'certeza de que', 0, 1, 1.4667, 'C63232', 1, '*'),
(144, 'cicero', 'cicero', 0, 0, 0.4, 'C600', 1, '*'),
(145, 'cícero escrito', 'cícero escrito', 0, 1, 1.4667, 'C6263', 1, '*'),
(146, 'cícero escrito em', 'cícero escrito em', 0, 1, 1.5667, 'C62635', 1, '*'),
(147, 'cicero também', 'cicero também', 0, 1, 1.4333, 'C63515', 1, '*'),
(148, 'cicero também foram', 'cicero também foram', 0, 1, 1.6333, 'C63515165', 1, '*'),
(149, 'cinco', 'cinco', 0, 0, 0.3333, 'C520', 1, '*'),
(150, 'cinco séculos', 'cinco séculos', 0, 1, 1.4333, 'C5242', 1, '*'),
(151, 'cinco séculos como', 'cinco séculos como', 0, 1, 1.6, 'C52425', 1, '*'),
(152, 'citações', 'citações', 0, 0, 0.5333, 'C320', 1, '*'),
(153, 'citações da', 'citações da', 0, 1, 1.3667, 'C323', 1, '*'),
(154, 'citações da palavra', 'citações da palavra', 0, 1, 1.6333, 'C3231416', 1, '*'),
(155, 'clássica', 'clássica', 0, 0, 0.5333, 'C420', 1, '*'),
(156, 'clássica datada', 'clássica datada', 0, 1, 1.5, 'C423', 1, '*'),
(157, 'clássica datada de', 'clássica datada de', 0, 1, 1.6, 'C423', 1, '*'),
(158, 'clássica descobriu', 'clássica descobriu', 0, 1, 1.6, 'C423216', 1, '*'),
(159, 'clássica descobriu a', 'clássica descobriu a', 0, 1, 1.6667, 'C423216', 1, '*'),
(160, 'coisas', 'coisas', 0, 0, 0.4, 'C000', 1, '*'),
(161, 'coisas do', 'coisas do', 0, 1, 1.3, 'C300', 1, '*'),
(162, 'coisas do gênero', 'coisas do gênero', 0, 1, 1.5333, 'C3256', 1, '*'),
(163, 'college', 'college', 0, 0, 0.4667, 'C420', 1, '*'),
(164, 'college na', 'college na', 0, 1, 1.3333, 'C425', 1, '*'),
(165, 'college na virginia', 'college na virginia', 0, 1, 1.6333, 'C4251625', 1, '*'),
(166, 'com', 'com', 0, 0, 0.2, 'C500', 1, '*'),
(167, 'com aparência', 'com aparência', 0, 1, 1.4333, 'C51652', 1, '*'),
(168, 'com aparência razoável', 'com aparência razoável', 0, 1, 1.7333, 'C516526214', 1, '*'),
(169, 'com humor', 'com humor', 0, 1, 1.3, 'C560', 1, '*'),
(170, 'com humor ou', 'com humor ou', 0, 1, 1.4, 'C560', 1, '*'),
(171, 'com mais', 'com mais', 0, 1, 1.2667, 'C520', 1, '*'),
(172, 'com mais de', 'com mais de', 0, 1, 1.3667, 'C523', 1, '*'),
(173, 'com o', 'com o', 0, 1, 1.1667, 'C500', 1, '*'),
(174, 'com o conteúdo', 'com o conteúdo', 0, 1, 1.4667, 'C5253', 1, '*'),
(175, 'com que', 'com que', 0, 1, 1.2333, 'C520', 1, '*'),
(176, 'com que ele', 'com que ele', 0, 1, 1.3667, 'C524', 1, '*'),
(177, 'com um', 'com um', 0, 1, 1.2, 'C500', 1, '*'),
(178, 'com um punhado', 'com um punhado', 0, 1, 1.4667, 'C5153', 1, '*'),
(179, 'combinado', 'combinado', 0, 0, 0.6, 'C5153', 1, '*'),
(180, 'combinado com', 'combinado com', 0, 1, 1.4333, 'C515325', 1, '*'),
(181, 'combinado com um', 'combinado com um', 0, 1, 1.5333, 'C515325', 1, '*'),
(182, 'como', 'como', 0, 0, 0.2667, 'C500', 1, '*'),
(183, 'como aldus', 'como aldus', 0, 1, 1.3333, 'C5432', 1, '*'),
(184, 'como aldus pagemaker', 'como aldus pagemaker', 0, 1, 1.6667, 'C543212526', 1, '*'),
(185, 'como também', 'como também', 0, 1, 1.3667, 'C53515', 1, '*'),
(186, 'como também ao', 'como também ao', 0, 1, 1.4667, 'C53515', 1, '*'),
(187, 'como texto-modelo', 'como texto-modelo', 0, 1, 1.5667, 'C5323534', 1, '*'),
(188, 'como texto-modelo padrão', 'como texto-modelo padrão', 0, 1, 1.8, 'C5323534136', 1, '*'),
(189, 'conforme', 'conforme', 0, 0, 0.5333, 'C5165', 1, '*'),
(190, 'conforme necessário', 'conforme necessário', 0, 1, 1.6333, 'C516526', 1, '*'),
(191, 'conforme necessário fazendo', 'conforme necessário fazendo', 0, 1, 1.9, 'C5165261253', 1, '*'),
(192, 'conhecido', 'conhecido', 0, 0, 0.6, 'C523', 1, '*'),
(193, 'conhecido de', 'conhecido de', 0, 1, 1.4, 'C523', 1, '*'),
(194, 'conhecido de todos', 'conhecido de todos', 0, 1, 1.6, 'C5232', 1, '*'),
(195, 'consectetur', 'consectetur', 0, 0, 0.7333, 'C5236', 1, '*'),
(196, 'consectetur oriunda', 'consectetur oriunda', 0, 1, 1.6333, 'C523653', 1, '*'),
(197, 'consectetur oriunda de', 'consectetur oriunda de', 0, 1, 1.7333, 'C523653', 1, '*'),
(198, 'conseguí-lo', 'conseguí-lo', 0, 0, 0.7333, 'C524', 1, '*'),
(199, 'conseguí-lo existem', 'conseguí-lo existem', 0, 1, 1.6333, 'C524235', 1, '*'),
(200, 'conseguí-lo existem muitas', 'conseguí-lo existem muitas', 0, 1, 1.8667, 'C52423532', 1, '*'),
(201, 'construção', 'construção', 0, 0, 0.6667, 'C5236', 1, '*'),
(202, 'construção várias', 'construção várias', 0, 1, 1.5667, 'C5236162', 1, '*'),
(203, 'construção várias versões', 'construção várias versões', 0, 1, 1.8333, 'C5236162162', 1, '*'),
(204, 'contendo', 'contendo', 0, 0, 0.5333, 'C5353', 1, '*'),
(205, 'contendo passagens', 'contendo passagens', 0, 1, 1.6, 'C53531252', 1, '*'),
(206, 'contendo passagens de', 'contendo passagens de', 0, 1, 1.7, 'C535312523', 1, '*'),
(207, 'conteúdo', 'conteúdo', 0, 0, 0.5333, 'C530', 1, '*'),
(208, 'conteúdo aqui', 'conteúdo aqui', 0, 1, 1.4333, 'C532', 1, '*'),
(209, 'conteúdo aqui conteúdo', 'conteúdo aqui conteúdo', 0, 1, 1.7333, 'C53253', 1, '*'),
(210, 'conteúdo aqui fazendo', 'conteúdo aqui fazendo', 0, 1, 1.7, 'C5321253', 1, '*'),
(211, 'conteúdo de', 'conteúdo de', 0, 1, 1.3667, 'C530', 1, '*'),
(212, 'conteúdo de texto', 'conteúdo de texto', 0, 1, 1.5667, 'C5323', 1, '*'),
(213, 'contrário', 'contrário', 0, 0, 0.6, 'C536', 1, '*'),
(214, 'contrário de', 'contrário de', 0, 1, 1.4, 'C5363', 1, '*'),
(215, 'contrário de conteúdo', 'contrário de conteúdo', 0, 1, 1.7, 'C5363253', 1, '*'),
(216, 'contrário do', 'contrário do', 0, 1, 1.4, 'C5363', 1, '*'),
(217, 'contrário do que', 'contrário do que', 0, 1, 1.5333, 'C53632', 1, '*'),
(218, 'convincentes', 'convincentes', 0, 0, 0.8, 'C5152532', 1, '*'),
(219, 'convincentes se', 'convincentes se', 0, 1, 1.5, 'C5152532', 1, '*'),
(220, 'convincentes se você', 'convincentes se você', 0, 1, 1.6667, 'C515253212', 1, '*'),
(221, 'da', 'da', 0, 0, 0.1333, 'D000', 1, '*'),
(222, 'da ética', 'da ética', 0, 1, 1.2667, 'D200', 1, '*'),
(223, 'da ética muito', 'da ética muito', 0, 1, 1.4667, 'D253', 1, '*'),
(224, 'da indústria', 'da indústria', 0, 1, 1.4, 'D53236', 1, '*'),
(225, 'da indústria tipográfica', 'da indústria tipográfica', 0, 1, 1.8, 'D53236312612', 1, '*'),
(226, 'da internet', 'da internet', 0, 1, 1.3667, 'D53653', 1, '*'),
(227, 'da internet ele', 'da internet ele', 0, 1, 1.5, 'D536534', 1, '*'),
(228, 'da palavra', 'da palavra', 0, 1, 1.3333, 'D1416', 1, '*'),
(229, 'da palavra na', 'da palavra na', 0, 1, 1.4333, 'D14165', 1, '*'),
(230, 'da renascença', 'da renascença', 0, 1, 1.4333, 'D6525', 1, '*'),
(231, 'da renascença a', 'da renascença a', 0, 1, 1.5, 'D6525', 1, '*'),
(232, 'da tradução', 'da tradução', 0, 1, 1.3667, 'D630', 1, '*'),
(233, 'da tradução feita', 'da tradução feita', 0, 1, 1.5667, 'D6313', 1, '*'),
(234, 'das', 'das', 0, 0, 0.2, 'D200', 1, '*'),
(235, 'das mais', 'das mais', 0, 1, 1.2667, 'D252', 1, '*'),
(236, 'das mais obscuras', 'das mais obscuras', 0, 1, 1.5667, 'D2521262', 1, '*'),
(237, 'das seções', 'das seções', 0, 1, 1.3333, 'D200', 1, '*'),
(238, 'das seções 1.10.32', 'das seções 1.10.32', 0, 1, 1.6, 'D200', 1, '*'),
(239, 'das versões', 'das versões', 0, 1, 1.3667, 'D2162', 1, '*'),
(240, 'das versões para', 'das versões para', 0, 1, 1.5333, 'D216216', 1, '*'),
(241, 'datada', 'datada', 0, 0, 0.4, 'D000', 1, '*'),
(242, 'datada de', 'datada de', 0, 1, 1.3, 'D000', 1, '*'),
(243, 'datada de 45', 'datada de 45', 0, 1, 1.4, 'D000', 1, '*'),
(244, 'de', 'de', 0, 0, 0.1333, 'D000', 1, '*'),
(245, 'de 200', 'de 200', 0, 1, 1.2, 'D000', 1, '*'),
(246, 'de 200 palavras', 'de 200 palavras', 0, 1, 1.5, 'D14162', 1, '*'),
(247, 'de 2000', 'de 2000', 0, 1, 1.2333, 'D000', 1, '*'),
(248, 'de 2000 anos', 'de 2000 anos', 0, 1, 1.4, 'D520', 1, '*'),
(249, 'de 45', 'de 45', 0, 1, 1.1667, 'D000', 1, '*'),
(250, 'de 45 ac', 'de 45 ac', 0, 1, 1.2667, 'D200', 1, '*'),
(251, 'de 60', 'de 60', 0, 1, 1.1667, 'D000', 1, '*'),
(252, 'de 60 quando', 'de 60 quando', 0, 1, 1.4, 'D253', 1, '*'),
(253, 'de alteração', 'de alteração', 0, 1, 1.4, 'D436', 1, '*'),
(254, 'de alteração seja', 'de alteração seja', 0, 1, 1.5667, 'D4362', 1, '*'),
(255, 'de cicero', 'de cicero', 0, 1, 1.3, 'D260', 1, '*'),
(256, 'de cícero escrito', 'de cícero escrito', 0, 1, 1.5667, 'D26263', 1, '*'),
(257, 'de cicero também', 'de cicero também', 0, 1, 1.5333, 'D263515', 1, '*'),
(258, 'de construção', 'de construção', 0, 1, 1.4333, 'D25236', 1, '*'),
(259, 'de construção várias', 'de construção várias', 0, 1, 1.6667, 'D25236162', 1, '*'),
(260, 'de conteúdo', 'de conteúdo', 0, 1, 1.3667, 'D253', 1, '*'),
(261, 'de conteúdo aqui', 'de conteúdo aqui', 0, 1, 1.5333, 'D2532', 1, '*'),
(262, 'de de', 'de de', 0, 1, 1.1667, 'D000', 1, '*'),
(263, 'de de finibus', 'de de finibus', 0, 1, 1.4333, 'D1512', 1, '*'),
(264, 'de editoração', 'de editoração', 0, 1, 1.4333, 'D600', 1, '*'),
(265, 'de editoração eletrônica', 'de editoração eletrônica', 0, 1, 1.8, 'D643652', 1, '*'),
(266, 'de estrutura', 'de estrutura', 0, 1, 1.4, 'D23636', 1, '*'),
(267, 'de estrutura de', 'de estrutura de', 0, 1, 1.5, 'D236363', 1, '*'),
(268, 'de finibus', 'de finibus', 0, 1, 1.3333, 'D1512', 1, '*'),
(269, 'de finibus bonorum', 'de finibus bonorum', 0, 1, 1.6, 'D15121565', 1, '*'),
(270, 'de frases', 'de frases', 0, 1, 1.3, 'D162', 1, '*'),
(271, 'de frases para', 'de frases para', 0, 1, 1.4667, 'D16216', 1, '*'),
(272, 'de humor', 'de humor', 0, 1, 1.2667, 'D560', 1, '*'),
(273, 'de humor palavras', 'de humor palavras', 0, 1, 1.5667, 'D5614162', 1, '*'),
(274, 'de impressos', 'de impressos', 0, 1, 1.4, 'D5162', 1, '*'),
(275, 'de impressos e', 'de impressos e', 0, 1, 1.4667, 'D5162', 1, '*'),
(276, 'de latim', 'de latim', 0, 1, 1.2667, 'D435', 1, '*'),
(277, 'de latim do', 'de latim do', 0, 1, 1.3667, 'D4353', 1, '*'),
(278, 'de letras', 'de letras', 0, 1, 1.3, 'D4362', 1, '*'),
(279, 'de letras ao', 'de letras ao', 0, 1, 1.4, 'D4362', 1, '*'),
(280, 'de literatura', 'de literatura', 0, 1, 1.4333, 'D43636', 1, '*'),
(281, 'de literatura latina', 'de literatura latina', 0, 1, 1.6667, 'D43636435', 1, '*'),
(282, 'de lorem', 'de lorem', 0, 1, 1.2667, 'D465', 1, '*'),
(283, 'de lorem ipsum', 'de lorem ipsum', 0, 1, 1.4667, 'D465125', 1, '*'),
(284, 'de modelos', 'de modelos', 0, 1, 1.3333, 'D5342', 1, '*'),
(285, 'de modelos de', 'de modelos de', 0, 1, 1.4333, 'D53423', 1, '*'),
(286, 'de onde', 'de onde', 0, 1, 1.2333, 'D530', 1, '*'),
(287, 'de onde ele', 'de onde ele', 0, 1, 1.3667, 'D534', 1, '*'),
(288, 'de páginas', 'de páginas', 0, 1, 1.3333, 'D1252', 1, '*'),
(289, 'de páginas na', 'de páginas na', 0, 1, 1.4333, 'D12525', 1, '*'),
(290, 'de passagens', 'de passagens', 0, 1, 1.4, 'D1252', 1, '*'),
(291, 'de passagens com', 'de passagens com', 0, 1, 1.5333, 'D12525', 1, '*'),
(292, 'de passagens de', 'de passagens de', 0, 1, 1.5, 'D12523', 1, '*'),
(293, 'de propósito', 'de propósito', 0, 1, 1.4, 'D16123', 1, '*'),
(294, 'de propósito injetando', 'de propósito injetando', 0, 1, 1.7333, 'D1612352353', 1, '*'),
(295, 'de publicação', 'de publicação', 0, 1, 1.4333, 'D142', 1, '*'),
(296, 'de publicação e', 'de publicação e', 0, 1, 1.5, 'D142', 1, '*'),
(297, 'de que', 'de que', 0, 1, 1.2, 'D200', 1, '*'),
(298, 'de que não', 'de que não', 0, 1, 1.3333, 'D250', 1, '*'),
(299, 'de repetições', 'de repetições', 0, 1, 1.4333, 'D6132', 1, '*'),
(300, 'de repetições inserções', 'de repetições inserções', 0, 1, 1.7667, 'D61325262', 1, '*'),
(301, 'de teoria', 'de teoria', 0, 1, 1.3, 'D600', 1, '*'),
(302, 'de teoria da', 'de teoria da', 0, 1, 1.4, 'D630', 1, '*'),
(303, 'de texto', 'de texto', 0, 1, 1.2667, 'D230', 1, '*'),
(304, 'de texto da', 'de texto da', 0, 1, 1.3667, 'D230', 1, '*'),
(305, 'de texto legível', 'de texto legível', 0, 1, 1.5333, 'D234214', 1, '*'),
(306, 'de tipos', 'de tipos', 0, 1, 1.2667, 'D120', 1, '*'),
(307, 'de tipos e', 'de tipos e', 0, 1, 1.3333, 'D120', 1, '*'),
(308, 'de tipos lorem', 'de tipos lorem', 0, 1, 1.4667, 'D12465', 1, '*'),
(309, 'de todos', 'de todos', 0, 1, 1.2667, 'D200', 1, '*'),
(310, 'de todos que', 'de todos que', 0, 1, 1.4, 'D200', 1, '*'),
(311, 'de um', 'de um', 0, 1, 1.1667, 'D500', 1, '*'),
(312, 'de um texto', 'de um texto', 0, 1, 1.3667, 'D5323', 1, '*'),
(313, 'de uma', 'de uma', 0, 1, 1.2, 'D500', 1, '*'),
(314, 'de uma linha', 'de uma linha', 0, 1, 1.4, 'D545', 1, '*'),
(315, 'de uma página', 'de uma página', 0, 1, 1.4333, 'D5125', 1, '*'),
(316, 'de uma passagem', 'de uma passagem', 0, 1, 1.5, 'D5125', 1, '*'),
(317, 'de usar', 'de usar', 0, 1, 1.2333, 'D260', 1, '*'),
(318, 'de usar lorem', 'de usar lorem', 0, 1, 1.4333, 'D26465', 1, '*'),
(319, 'década', 'década', 0, 0, 0.4, 'D230', 1, '*'),
(320, 'década de', 'década de', 0, 1, 1.3, 'D230', 1, '*'),
(321, 'década de 60', 'década de 60', 0, 1, 1.4, 'D230', 1, '*'),
(322, 'decalques', 'decalques', 0, 0, 0.6, 'D242', 1, '*'),
(323, 'decalques contendo', 'decalques contendo', 0, 1, 1.6, 'D2425353', 1, '*'),
(324, 'decalques contendo passagens', 'decalques contendo passagens', 0, 1, 1.9333, 'D24253531252', 1, '*'),
(325, 'descobriu', 'descobriu', 0, 0, 0.6, 'D216', 1, '*'),
(326, 'descobriu a', 'descobriu a', 0, 1, 1.3667, 'D216', 1, '*'),
(327, 'descobriu a sua', 'descobriu a sua', 0, 1, 1.5, 'D2162', 1, '*'),
(328, 'desconhecido', 'desconhecido', 0, 0, 0.8, 'D2523', 1, '*'),
(329, 'desconhecido pegou', 'desconhecido pegou', 0, 1, 1.6, 'D252312', 1, '*'),
(330, 'desconhecido pegou uma', 'desconhecido pegou uma', 0, 1, 1.7333, 'D2523125', 1, '*'),
(331, 'desde', 'desde', 0, 0, 0.3333, 'D230', 1, '*'),
(332, 'desde o', 'desde o', 0, 1, 1.2333, 'D230', 1, '*'),
(333, 'desde o século', 'desde o século', 0, 1, 1.4667, 'D2324', 1, '*'),
(334, 'deste', 'deste', 0, 0, 0.3333, 'D230', 1, '*'),
(335, 'deste o', 'deste o', 0, 1, 1.2333, 'D230', 1, '*'),
(336, 'deste o primeiro', 'deste o primeiro', 0, 1, 1.5333, 'D231656', 1, '*'),
(337, 'diagramação', 'diagramação', 0, 0, 0.7333, 'D265', 1, '*'),
(338, 'diagramação a', 'diagramação a', 0, 1, 1.4333, 'D265', 1, '*'),
(339, 'diagramação a vantagem', 'diagramação a vantagem', 0, 1, 1.7333, 'D26515325', 1, '*'),
(340, 'dicionário', 'dicionário', 0, 0, 0.6667, 'D256', 1, '*'),
(341, 'dicionário com', 'dicionário com', 0, 1, 1.4667, 'D25625', 1, '*'),
(342, 'dicionário com mais', 'dicionário com mais', 0, 1, 1.6333, 'D256252', 1, '*'),
(343, 'disponíveis', 'disponíveis', 0, 0, 0.7333, 'D21512', 1, '*'),
(344, 'disponíveis de', 'disponíveis de', 0, 1, 1.4667, 'D215123', 1, '*'),
(345, 'disponíveis de passagens', 'disponíveis de passagens', 0, 1, 1.8, 'D2151231252', 1, '*'),
(346, 'distrairá', 'distrairá', 0, 0, 0.6, 'D236', 1, '*'),
(347, 'distrairá com', 'distrairá com', 0, 1, 1.4333, 'D23625', 1, '*'),
(348, 'distrairá com o', 'distrairá com o', 0, 1, 1.5, 'D23625', 1, '*'),
(349, 'distribuição', 'distribuição', 0, 0, 0.8, 'D2361', 1, '*'),
(350, 'distribuição normal', 'distribuição normal', 0, 1, 1.6333, 'D23615654', 1, '*'),
(351, 'distribuição normal de', 'distribuição normal de', 0, 1, 1.7333, 'D236156543', 1, '*'),
(352, 'do', 'do', 0, 0, 0.1333, 'D000', 1, '*'),
(353, 'do bem', 'do bem', 0, 1, 1.2, 'D150', 1, '*'),
(354, 'do bem e', 'do bem e', 0, 1, 1.2667, 'D150', 1, '*'),
(355, 'do de', 'do de', 0, 1, 1.1667, 'D000', 1, '*'),
(356, 'do de finibus', 'do de finibus', 0, 1, 1.4333, 'D1512', 1, '*'),
(357, 'do gênero', 'do gênero', 0, 1, 1.3, 'D256', 1, '*'),
(358, 'do gênero de', 'do gênero de', 0, 1, 1.4, 'D2563', 1, '*'),
(359, 'do hampden-sydney', 'do hampden-sydney', 0, 1, 1.5667, 'D5135235', 1, '*'),
(360, 'do hampden-sydney college', 'do hampden-sydney college', 0, 1, 1.8333, 'D5135235242', 1, '*'),
(361, 'do mal', 'do mal', 0, 1, 1.2, 'D540', 1, '*'),
(362, 'do mal de', 'do mal de', 0, 1, 1.3, 'D543', 1, '*'),
(363, 'do que', 'do que', 0, 1, 1.2, 'D200', 1, '*'),
(364, 'do que se', 'do que se', 0, 1, 1.3, 'D200', 1, '*'),
(365, 'do texto', 'do texto', 0, 1, 1.2667, 'D230', 1, '*'),
(366, 'do texto todos', 'do texto todos', 0, 1, 1.4667, 'D232', 1, '*'),
(367, 'dolor', 'dolor', 0, 0, 0.3333, 'D460', 1, '*'),
(368, 'dolor sit', 'dolor sit', 0, 1, 1.3, 'D4623', 1, '*'),
(369, 'dolor sit amet', 'dolor sit amet', 0, 1, 1.4667, 'D462353', 1, '*'),
(370, 'dos', 'dos', 0, 0, 0.2, 'D200', 1, '*'),
(371, 'dos anos', 'dos anos', 0, 1, 1.2667, 'D252', 1, '*'),
(372, 'dos anos eventualmente', 'dos anos eventualmente', 0, 1, 1.7333, 'D252153453', 1, '*'),
(373, 'e', 'e', 0, 0, 0.0667, 'E000', 1, '*'),
(374, 'e 1.10.33', 'e 1.10.33', 0, 1, 1.3, 'E000', 1, '*'),
(375, 'e 1.10.33 de', 'e 1.10.33 de', 0, 1, 1.4, 'E300', 1, '*'),
(376, 'e 1.10.33 do', 'e 1.10.33 do', 0, 1, 1.4, 'E300', 1, '*'),
(377, 'e às', 'e às', 0, 1, 1.1333, 'E200', 1, '*'),
(378, 'e às vezes', 'e às vezes', 0, 1, 1.3333, 'E212', 1, '*'),
(379, 'e coisas', 'e coisas', 0, 1, 1.2667, 'E200', 1, '*'),
(380, 'e coisas do', 'e coisas do', 0, 1, 1.3667, 'E230', 1, '*'),
(381, 'e de', 'e de', 0, 1, 1.1333, 'E300', 1, '*'),
(382, 'e de impressos', 'e de impressos', 0, 1, 1.4667, 'E35162', 1, '*'),
(383, 'e do', 'e do', 0, 1, 1.1333, 'E300', 1, '*'),
(384, 'e do mal', 'e do mal', 0, 1, 1.2667, 'E354', 1, '*'),
(385, 'e editores', 'e editores', 0, 1, 1.3333, 'E362', 1, '*'),
(386, 'e editores de', 'e editores de', 0, 1, 1.4333, 'E3623', 1, '*'),
(387, 'e lorem', 'e lorem', 0, 1, 1.2333, 'E465', 1, '*'),
(388, 'e lorem ipsum', 'e lorem ipsum', 0, 1, 1.4333, 'E465125', 1, '*'),
(389, 'e mais', 'e mais', 0, 1, 1.2, 'E520', 1, '*'),
(390, 'e mais recentemente', 'e mais recentemente', 0, 1, 1.6333, 'E52625353', 1, '*'),
(391, 'e os', 'e os', 0, 1, 1.1333, 'E200', 1, '*'),
(392, 'e os embaralhou', 'e os embaralhou', 0, 1, 1.5, 'E25164', 1, '*'),
(393, 'e procurando', 'e procurando', 0, 1, 1.4, 'E162653', 1, '*'),
(394, 'e procurando por', 'e procurando por', 0, 1, 1.5333, 'E16265316', 1, '*'),
(395, 'é que', 'é que', 0, 1, 1.1667, 'é200', 1, '*'),
(396, 'é que ele', 'é que ele', 0, 1, 1.3, 'é240', 1, '*'),
(397, 'é simplesmente', 'é simplesmente', 0, 1, 1.4667, 'é2514253', 1, '*'),
(398, 'é simplesmente um', 'é simplesmente um', 0, 1, 1.5667, 'é25142535', 1, '*'),
(399, 'é simplesmente uma', 'é simplesmente uma', 0, 1, 1.6, 'é25142535', 1, '*'),
(400, 'é um', 'é um', 0, 1, 1.1333, 'é500', 1, '*'),
(401, 'é um fato', 'é um fato', 0, 1, 1.3, 'é513', 1, '*'),
(402, 'é um tratado', 'é um tratado', 0, 1, 1.4, 'é5363', 1, '*'),
(403, 'e uma', 'e uma', 0, 1, 1.1667, 'E500', 1, '*'),
(404, 'e uma rápida', 'e uma rápida', 0, 1, 1.4, 'E5613', 1, '*'),
(405, 'e vem', 'e vem', 0, 1, 1.1667, 'E150', 1, '*'),
(406, 'e vem sendo', 'e vem sendo', 0, 1, 1.3667, 'E15253', 1, '*'),
(407, 'editoração', 'editoração', 0, 0, 0.6667, 'E360', 1, '*'),
(408, 'editoração eletrônica', 'editoração eletrônica', 0, 1, 1.7, 'E3643652', 1, '*'),
(409, 'editoração eletrônica como', 'editoração eletrônica como', 0, 1, 1.8667, 'E36436525', 1, '*'),
(410, 'editoração eletrônica permanecendo', 'editoração eletrônica permanecendo', 0, 1, 2, 'E3643652165253', 1, '*'),
(411, 'editores', 'editores', 0, 0, 0.5333, 'E362', 1, '*'),
(412, 'editores de', 'editores de', 0, 1, 1.3667, 'E3623', 1, '*'),
(413, 'editores de páginas', 'editores de páginas', 0, 1, 1.6333, 'E36231252', 1, '*'),
(414, 'ele', 'ele', 0, 0, 0.2, 'E400', 1, '*'),
(415, 'ele tem', 'ele tem', 0, 1, 1.2333, 'E435', 1, '*'),
(416, 'ele tem uma', 'ele tem uma', 0, 1, 1.3667, 'E435', 1, '*'),
(417, 'ele tenha', 'ele tenha', 0, 1, 1.3, 'E435', 1, '*'),
(418, 'ele tenha uma', 'ele tenha uma', 0, 1, 1.4333, 'E435', 1, '*'),
(419, 'ele usa', 'ele usa', 0, 1, 1.2333, 'E420', 1, '*'),
(420, 'ele usa um', 'ele usa um', 0, 1, 1.3333, 'E425', 1, '*'),
(421, 'ele vem', 'ele vem', 0, 1, 1.2333, 'E415', 1, '*'),
(422, 'ele vem ao', 'ele vem ao', 0, 1, 1.3333, 'E415', 1, '*'),
(423, 'eletrônica', 'eletrônica', 0, 0, 0.6667, 'E43652', 1, '*'),
(424, 'eletrônica como', 'eletrônica como', 0, 1, 1.5, 'E436525', 1, '*'),
(425, 'eletrônica como aldus', 'eletrônica como aldus', 0, 1, 1.7, 'E436525432', 1, '*'),
(426, 'eletrônica permanecendo', 'eletrônica permanecendo', 0, 1, 1.7667, 'E43652165253', 1, '*'),
(427, 'eletrônica permanecendo essencialmente', 'eletrônica permanecendo essencialmente', 0, 1, 2, 'E43652165253252453', 1, '*'),
(428, 'em', 'em', 0, 0, 0.1333, 'E500', 1, '*'),
(429, 'em 1914', 'em 1914', 0, 1, 1.2333, 'E500', 1, '*'),
(430, 'em 1914 onde', 'em 1914 onde', 0, 1, 1.4, 'E530', 1, '*'),
(431, 'em 45ac', 'em 45ac', 0, 1, 1.2333, 'E520', 1, '*'),
(432, 'em 45ac este', 'em 45ac este', 0, 1, 1.4, 'E523', 1, '*'),
(433, 'em latim', 'em latim', 0, 1, 1.2667, 'E5435', 1, '*'),
(434, 'em latim combinado', 'em latim combinado', 0, 1, 1.6, 'E543525153', 1, '*'),
(435, 'em latim consectetur', 'em latim consectetur', 0, 1, 1.6667, 'E543525236', 1, '*'),
(436, 'em sua', 'em sua', 0, 1, 1.2, 'E520', 1, '*'),
(437, 'em sua fase', 'em sua fase', 0, 1, 1.3667, 'E5212', 1, '*'),
(438, 'em sua forma', 'em sua forma', 0, 1, 1.4, 'E52165', 1, '*'),
(439, 'em uma', 'em uma', 0, 1, 1.2, 'E500', 1, '*'),
(440, 'em uma obra', 'em uma obra', 0, 1, 1.3667, 'E516', 1, '*'),
(441, 'embaraçoso', 'embaraçoso', 0, 0, 0.6667, 'E5162', 1, '*'),
(442, 'embaraçoso escrito', 'embaraçoso escrito', 0, 1, 1.6, 'E516263', 1, '*'),
(443, 'embaraçoso escrito escondido', 'embaraçoso escrito escondido', 0, 1, 1.9333, 'E516263253', 1, '*'),
(444, 'embaralhou', 'embaralhou', 0, 0, 0.6667, 'E5164', 1, '*'),
(445, 'embaralhou para', 'embaralhou para', 0, 1, 1.5, 'E516416', 1, '*'),
(446, 'embaralhou para fazer', 'embaralhou para fazer', 0, 1, 1.7, 'E516416126', 1, '*'),
(447, 'encontradas', 'encontradas', 0, 0, 0.7333, 'E5253632', 1, '*'),
(448, 'encontradas em', 'encontradas em', 0, 1, 1.4667, 'E52536325', 1, '*'),
(449, 'encontradas em uma', 'encontradas em uma', 0, 1, 1.6, 'E52536325', 1, '*'),
(450, 'entre', 'entre', 0, 0, 0.3333, 'E536', 1, '*'),
(451, 'entre citações', 'entre citações', 0, 1, 1.4667, 'E536232', 1, '*'),
(452, 'entre citações da', 'entre citações da', 0, 1, 1.5667, 'E5362323', 1, '*'),
(453, 'época', 'época', 0, 0, 0.3333, 'é120', 1, '*'),
(454, 'época da', 'época da', 0, 1, 1.2667, 'é123', 1, '*'),
(455, 'época da renascença', 'época da renascença', 0, 1, 1.6333, 'é1236525', 1, '*'),
(456, 'escondido', 'escondido', 0, 0, 0.6, 'E253', 1, '*'),
(457, 'escondido no', 'escondido no', 0, 1, 1.4, 'E2535', 1, '*'),
(458, 'escondido no meio', 'escondido no meio', 0, 1, 1.5667, 'E2535', 1, '*'),
(459, 'escrito', 'escrito', 0, 0, 0.4667, 'E263', 1, '*'),
(460, 'escrito em', 'escrito em', 0, 1, 1.3333, 'E2635', 1, '*'),
(461, 'escrito em 45ac', 'escrito em 45ac', 0, 1, 1.5, 'E26352', 1, '*'),
(462, 'escrito escondido', 'escrito escondido', 0, 1, 1.5667, 'E263253', 1, '*'),
(463, 'escrito escondido no', 'escrito escondido no', 0, 1, 1.6667, 'E2632535', 1, '*'),
(464, 'essencialmente', 'essencialmente', 0, 0, 0.9333, 'E252453', 1, '*'),
(465, 'essencialmente inalterado', 'essencialmente inalterado', 0, 1, 1.8333, 'E25245354363', 1, '*'),
(466, 'essencialmente inalterado se', 'essencialmente inalterado se', 0, 1, 1.9333, 'E252453543632', 1, '*'),
(467, 'está', 'está', 0, 0, 0.2667, 'E230', 1, '*'),
(468, 'está reproduzido', 'está reproduzido', 0, 1, 1.5333, 'E23616323', 1, '*'),
(469, 'está reproduzido abaixo', 'está reproduzido abaixo', 0, 1, 1.7667, 'E2361632312', 1, '*'),
(470, 'este', 'este', 0, 0, 0.2667, 'E230', 1, '*'),
(471, 'este livro', 'este livro', 0, 1, 1.3333, 'E23416', 1, '*'),
(472, 'este livro é', 'este livro é', 0, 1, 1.4, 'E23416', 1, '*'),
(473, 'estiver', 'estiver', 0, 0, 0.4667, 'E2316', 1, '*'),
(474, 'estiver examinando', 'estiver examinando', 0, 1, 1.6, 'E2316253', 1, '*'),
(475, 'estiver examinando sua', 'estiver examinando sua', 0, 1, 1.7333, 'E23162532', 1, '*'),
(476, 'estrutura', 'estrutura', 0, 0, 0.6, 'E23636', 1, '*'),
(477, 'estrutura de', 'estrutura de', 0, 1, 1.4, 'E236363', 1, '*'),
(478, 'estrutura de frases', 'estrutura de frases', 0, 1, 1.6333, 'E236363162', 1, '*'),
(479, 'et', 'et', 0, 0, 0.1333, 'E300', 1, '*'),
(480, 'et malorum', 'et malorum', 0, 1, 1.3333, 'E35465', 1, '*'),
(481, 'et malorum de', 'et malorum de', 0, 1, 1.4333, 'E354653', 1, '*'),
(482, 'et malorum os', 'et malorum os', 0, 1, 1.4333, 'E354652', 1, '*'),
(483, 'etc', 'etc', 0, 0, 0.2, 'E320', 1, '*'),
(484, 'ética', 'ética', 0, 0, 0.3333, 'é320', 1, '*'),
(485, 'ética muito', 'ética muito', 0, 1, 1.3667, 'é3253', 1, '*'),
(486, 'ética muito popular', 'ética muito popular', 0, 1, 1.6333, 'é3253146', 1, '*'),
(487, 'eventualmente', 'eventualmente', 0, 0, 0.8667, 'E153453', 1, '*'),
(488, 'eventualmente por', 'eventualmente por', 0, 1, 1.5667, 'E15345316', 1, '*'),
(489, 'eventualmente por acidente', 'eventualmente por acidente', 0, 1, 1.8667, 'E153453162353', 1, '*'),
(490, 'examinando', 'examinando', 0, 0, 0.6667, 'E253', 1, '*'),
(491, 'examinando sua', 'examinando sua', 0, 1, 1.4667, 'E2532', 1, '*'),
(492, 'examinando sua diagramação', 'examinando sua diagramação', 0, 1, 1.8667, 'E25323265', 1, '*'),
(493, 'exata', 'exata', 0, 0, 0.3333, 'E230', 1, '*'),
(494, 'exata original', 'exata original', 0, 1, 1.4667, 'E236254', 1, '*'),
(495, 'exata original acompanhada', 'exata original acompanhada', 0, 1, 1.8667, 'E23625425153', 1, '*'),
(496, 'existem', 'existem', 0, 0, 0.4667, 'E235', 1, '*'),
(497, 'existem muitas', 'existem muitas', 0, 1, 1.4667, 'E23532', 1, '*'),
(498, 'existem muitas variações', 'existem muitas variações', 0, 1, 1.8, 'E23532162', 1, '*'),
(499, 'extremos', 'extremos', 0, 0, 0.5333, 'E23652', 1, '*'),
(500, 'extremos do', 'extremos do', 0, 1, 1.3667, 'E236523', 1, '*'),
(501, 'extremos do bem', 'extremos do bem', 0, 1, 1.5, 'E23652315', 1, '*'),
(502, 'fase', 'fase', 0, 0, 0.2667, 'F200', 1, '*'),
(503, 'fase de', 'fase de', 0, 1, 1.2333, 'F230', 1, '*'),
(504, 'fase de construção', 'fase de construção', 0, 1, 1.6, 'F2325236', 1, '*'),
(505, 'fato', 'fato', 0, 0, 0.2667, 'F300', 1, '*'),
(506, 'fato conhecido', 'fato conhecido', 0, 1, 1.4667, 'F32523', 1, '*'),
(507, 'fato conhecido de', 'fato conhecido de', 0, 1, 1.5667, 'F32523', 1, '*'),
(508, 'fazendo', 'fazendo', 0, 0, 0.4667, 'F253', 1, '*'),
(509, 'fazendo com', 'fazendo com', 0, 1, 1.3667, 'F25325', 1, '*'),
(510, 'fazendo com que', 'fazendo com que', 0, 1, 1.5, 'F253252', 1, '*'),
(511, 'fazendo deste', 'fazendo deste', 0, 1, 1.4333, 'F25323', 1, '*'),
(512, 'fazendo deste o', 'fazendo deste o', 0, 1, 1.5, 'F25323', 1, '*'),
(513, 'fazer', 'fazer', 0, 0, 0.3333, 'F260', 1, '*'),
(514, 'fazer um', 'fazer um', 0, 1, 1.2667, 'F265', 1, '*'),
(515, 'fazer um livro', 'fazer um livro', 0, 1, 1.4667, 'F265416', 1, '*'),
(516, 'feita', 'feita', 0, 0, 0.3333, 'F300', 1, '*'),
(517, 'feita por', 'feita por', 0, 1, 1.3, 'F316', 1, '*'),
(518, 'feita por h', 'feita por h', 0, 1, 1.3667, 'F316', 1, '*'),
(519, 'finibus', 'finibus', 0, 0, 0.4667, 'F512', 1, '*'),
(520, 'finibus bonorum', 'finibus bonorum', 0, 1, 1.5, 'F5121565', 1, '*'),
(521, 'finibus bonorum et', 'finibus bonorum et', 0, 1, 1.6, 'F51215653', 1, '*'),
(522, 'foram', 'foram', 0, 0, 0.3333, 'F650', 1, '*'),
(523, 'foram reproduzidas', 'foram reproduzidas', 0, 1, 1.6, 'F656163232', 1, '*'),
(524, 'foram reproduzidas abaixo', 'foram reproduzidas abaixo', 0, 1, 1.8333, 'F65616323212', 1, '*'),
(525, 'forma', 'forma', 0, 0, 0.3333, 'F650', 1, '*'),
(526, 'forma exata', 'forma exata', 0, 1, 1.3667, 'F6523', 1, '*'),
(527, 'forma exata original', 'forma exata original', 0, 1, 1.6667, 'F65236254', 1, '*'),
(528, 'frases', 'frases', 0, 0, 0.4, 'F620', 1, '*'),
(529, 'frases para', 'frases para', 0, 1, 1.3667, 'F6216', 1, '*'),
(530, 'frases para gerar', 'frases para gerar', 0, 1, 1.5667, 'F621626', 1, '*'),
(531, 'gênero', 'gênero', 0, 0, 0.4, 'G560', 1, '*'),
(532, 'gênero de', 'gênero de', 0, 1, 1.3, 'G563', 1, '*'),
(533, 'gênero de onde', 'gênero de onde', 0, 1, 1.4667, 'G56353', 1, '*'),
(534, 'gerador', 'gerador', 0, 0, 0.4667, 'G636', 1, '*'),
(535, 'gerador de', 'gerador de', 0, 1, 1.3333, 'G6363', 1, '*'),
(536, 'gerador de lorem', 'gerador de lorem', 0, 1, 1.5333, 'G6363465', 1, '*'),
(537, 'geradores', 'geradores', 0, 0, 0.6, 'G6362', 1, '*'),
(538, 'geradores de', 'geradores de', 0, 1, 1.4, 'G63623', 1, '*'),
(539, 'geradores de lorem', 'geradores de lorem', 0, 1, 1.6, 'G63623465', 1, '*'),
(540, 'gerar', 'gerar', 0, 0, 0.3333, 'G600', 1, '*'),
(541, 'gerar um', 'gerar um', 0, 1, 1.2667, 'G650', 1, '*'),
(542, 'gerar um lorem', 'gerar um lorem', 0, 1, 1.4667, 'G65465', 1, '*'),
(543, 'h', 'h', 0, 0, 0.0667, 'H000', 1, '*'),
(544, 'h rackham', 'h rackham', 0, 1, 1.3, 'H625', 1, '*'),
(545, 'h rackham em', 'h rackham em', 0, 1, 1.4, 'H625', 1, '*'),
(546, 'há', 'há', 0, 0, 0.1333, 'H000', 1, '*'),
(547, 'há algo', 'há algo', 0, 1, 1.2333, 'H420', 1, '*'),
(548, 'há algo embaraçoso', 'há algo embaraçoso', 0, 1, 1.6, 'H425162', 1, '*'),
(549, 'hampden-sydney', 'hampden-sydney', 0, 0, 0.9333, 'H5135235', 1, '*'),
(550, 'hampden-sydney college', 'hampden-sydney college', 0, 1, 1.7333, 'H5135235242', 1, '*'),
(551, 'hampden-sydney college na', 'hampden-sydney college na', 0, 1, 1.8333, 'H51352352425', 1, '*'),
(552, 'humor', 'humor', 0, 0, 0.3333, 'H560', 1, '*'),
(553, 'humor e', 'humor e', 0, 1, 1.2333, 'H560', 1, '*'),
(554, 'humor e coisas', 'humor e coisas', 0, 1, 1.4667, 'H562', 1, '*'),
(555, 'humor ou', 'humor ou', 0, 1, 1.2667, 'H560', 1, '*'),
(556, 'humor ou palavras', 'humor ou palavras', 0, 1, 1.5667, 'H5614162', 1, '*'),
(557, 'humor palavras', 'humor palavras', 0, 1, 1.4667, 'H5614162', 1, '*'),
(558, 'humor palavras não', 'humor palavras não', 0, 1, 1.6, 'H56141625', 1, '*'),
(559, 'impressor', 'impressor', 0, 0, 0.6, 'I51626', 1, '*'),
(560, 'impressor desconhecido', 'impressor desconhecido', 0, 1, 1.7333, 'I5162632523', 1, '*'),
(561, 'impressor desconhecido pegou', 'impressor desconhecido pegou', 0, 1, 1.9333, 'I516263252312', 1, '*'),
(562, 'impressos', 'impressos', 0, 0, 0.6, 'I5162', 1, '*'),
(563, 'impressos e', 'impressos e', 0, 1, 1.3667, 'I5162', 1, '*'),
(564, 'impressos e vem', 'impressos e vem', 0, 1, 1.5, 'I516215', 1, '*'),
(565, 'inalterado', 'inalterado', 0, 0, 0.6667, 'I54363', 1, '*'),
(566, 'inalterado se', 'inalterado se', 0, 1, 1.4333, 'I543632', 1, '*'),
(567, 'inalterado se popularizou', 'inalterado se popularizou', 0, 1, 1.8333, 'I5436321462', 1, '*'),
(568, 'index', 'index', 0, 0, 0.3333, 'I532', 1, '*'),
(569, 'indubitável', 'indubitável', 0, 0, 0.7333, 'I531314', 1, '*'),
(570, 'indubitável origem', 'indubitável origem', 0, 1, 1.6, 'I531314625', 1, '*'),
(571, 'indubitável origem lorem', 'indubitável origem lorem', 0, 1, 1.8, 'I531314625465', 1, '*'),
(572, 'indústria', 'indústria', 0, 0, 0.6, 'I53236', 1, '*'),
(573, 'indústria tipográfica', 'indústria tipográfica', 0, 1, 1.7, 'I53236312612', 1, '*'),
(574, 'indústria tipográfica e', 'indústria tipográfica e', 0, 1, 1.7667, 'I53236312612', 1, '*'),
(575, 'inglês', 'inglês', 0, 0, 0.4, 'I5242', 1, '*'),
(576, 'inglês da', 'inglês da', 0, 1, 1.3, 'I52423', 1, '*'),
(577, 'inglês da tradução', 'inglês da tradução', 0, 1, 1.6, 'I5242363', 1, '*'),
(578, 'injetando', 'injetando', 0, 0, 0.6, 'I52353', 1, '*'),
(579, 'injetando humor', 'injetando humor', 0, 1, 1.5, 'I5235356', 1, '*'),
(580, 'injetando humor e', 'injetando humor e', 0, 1, 1.5667, 'I5235356', 1, '*'),
(581, 'inserção', 'inserção', 0, 0, 0.5333, 'I526', 1, '*'),
(582, 'inserção de', 'inserção de', 0, 1, 1.3667, 'I5263', 1, '*'),
(583, 'inserção de passagens', 'inserção de passagens', 0, 1, 1.7, 'I52631252', 1, '*'),
(584, 'inserções', 'inserções', 0, 0, 0.6, 'I5262', 1, '*'),
(585, 'inserções de', 'inserções de', 0, 1, 1.4, 'I52623', 1, '*'),
(586, 'inserções de humor', 'inserções de humor', 0, 1, 1.6, 'I5262356', 1, '*'),
(587, 'integrado', 'integrado', 0, 0, 0.6, 'I53263', 1, '*'),
(588, 'integrado a', 'integrado a', 0, 1, 1.3667, 'I53263', 1, '*'),
(589, 'integrado a softwares', 'integrado a softwares', 0, 1, 1.7, 'I5326321362', 1, '*'),
(590, 'interessados', 'interessados', 0, 0, 0.8, 'I536232', 1, '*'),
(591, 'interessados seções', 'interessados seções', 0, 1, 1.6333, 'I536232', 1, '*'),
(592, 'interessados seções 1.10.32', 'interessados seções 1.10.32', 0, 1, 1.9, 'I536232', 1, '*'),
(593, 'internet', 'internet', 0, 0, 0.5333, 'I53653', 1, '*'),
(594, 'internet agora', 'internet agora', 0, 1, 1.4667, 'I5365326', 1, '*'),
(595, 'internet agora usam', 'internet agora usam', 0, 1, 1.6333, 'I536532625', 1, '*'),
(596, 'internet ele', 'internet ele', 0, 1, 1.4, 'I536534', 1, '*'),
(597, 'internet ele usa', 'internet ele usa', 0, 1, 1.5333, 'I5365342', 1, '*'),
(598, 'internet tendem', 'internet tendem', 0, 1, 1.5, 'I53653535', 1, '*'),
(599, 'internet tendem a', 'internet tendem a', 0, 1, 1.5667, 'I53653535', 1, '*'),
(600, 'ipsum', 'ipsum', 0, 0, 0.3333, 'I125', 1, '*'),
(601, 'ipsum 2', 'ipsum 2', 0, 1, 1.2333, 'I125', 1, '*'),
(602, 'ipsum autêntico', 'ipsum autêntico', 0, 1, 1.5, 'I1253532', 1, '*'),
(603, 'ipsum autêntico da', 'ipsum autêntico da', 0, 1, 1.6, 'I12535323', 1, '*'),
(604, 'ipsum com', 'ipsum com', 0, 1, 1.3, 'I12525', 1, '*'),
(605, 'ipsum com aparência', 'ipsum com aparência', 0, 1, 1.6333, 'I125251652', 1, '*'),
(606, 'ipsum como', 'ipsum como', 0, 1, 1.3333, 'I12525', 1, '*'),
(607, 'ipsum como texto-modelo', 'ipsum como texto-modelo', 0, 1, 1.7667, 'I12525323534', 1, '*'),
(608, 'ipsum dolor', 'ipsum dolor', 0, 1, 1.3667, 'I125346', 1, '*'),
(609, 'ipsum dolor sit', 'ipsum dolor sit', 0, 1, 1.5, 'I12534623', 1, '*'),
(610, 'ipsum e', 'ipsum e', 0, 1, 1.2333, 'I125', 1, '*'),
(611, 'ipsum e mais', 'ipsum e mais', 0, 1, 1.4, 'I1252', 1, '*'),
(612, 'ipsum e procurando', 'ipsum e procurando', 0, 1, 1.6, 'I125162653', 1, '*'),
(613, 'ipsum é que', 'ipsum é que', 0, 1, 1.3667, 'I1252', 1, '*'),
(614, 'ipsum é simplesmente', 'ipsum é simplesmente', 0, 1, 1.6667, 'I1252514253', 1, '*'),
(615, 'ipsum lorem', 'ipsum lorem', 0, 1, 1.3667, 'I125465', 1, '*'),
(616, 'ipsum lorem ipsum', 'ipsum lorem ipsum', 0, 1, 1.5667, 'I125465125', 1, '*'),
(617, 'ipsum mas', 'ipsum mas', 0, 1, 1.3, 'I1252', 1, '*'),
(618, 'ipsum mas a', 'ipsum mas a', 0, 1, 1.3667, 'I1252', 1, '*'),
(619, 'ipsum na', 'ipsum na', 0, 1, 1.2667, 'I125', 1, '*'),
(620, 'ipsum na internet', 'ipsum na internet', 0, 1, 1.5667, 'I1253653', 1, '*'),
(621, 'ipsum não', 'ipsum não', 0, 1, 1.3, 'I125', 1, '*'),
(622, 'ipsum não é', 'ipsum não é', 0, 1, 1.3667, 'I125', 1, '*'),
(623, 'ipsum precisa', 'ipsum precisa', 0, 1, 1.4333, 'I125162', 1, '*'),
(624, 'ipsum precisa ter', 'ipsum precisa ter', 0, 1, 1.5667, 'I12516236', 1, '*'),
(625, 'ipsum sobreviveu', 'ipsum sobreviveu', 0, 1, 1.5333, 'I1252161', 1, '*'),
(626, 'ipsum sobreviveu não', 'ipsum sobreviveu não', 0, 1, 1.6667, 'I12521615', 1, '*'),
(627, 'ipsum usado', 'ipsum usado', 0, 1, 1.3667, 'I12523', 1, '*'),
(628, 'ipsum usado desde', 'ipsum usado desde', 0, 1, 1.5667, 'I1252323', 1, '*'),
(629, 'ipsum vem', 'ipsum vem', 0, 1, 1.3, 'I12515', 1, '*'),
(630, 'ipsum vem das', 'ipsum vem das', 0, 1, 1.4333, 'I1251532', 1, '*'),
(631, 'ipsum\'', 'ipsum', 0, 0, 0.4, 'I125', 1, '*'),
(632, 'ipsum\' mostra', 'ipsum mostra', 0, 1, 1.4333, 'I125236', 1, '*'),
(633, 'ipsum\' mostra vários', 'ipsum mostra vários', 0, 1, 1.6667, 'I125236162', 1, '*'),
(634, 'joomla', 'joomla', 0, 0, 0.4, 'J540', 1, '*'),
(635, 'joomla index', 'joomla index', 0, 1, 1.4, 'J54532', 1, '*'),
(636, 'lançou', 'lançou', 0, 0, 0.4, 'L500', 1, '*'),
(637, 'lançou decalques', 'lançou decalques', 0, 1, 1.5333, 'L53242', 1, '*'),
(638, 'lançou decalques contendo', 'lançou decalques contendo', 0, 1, 1.8333, 'L532425353', 1, '*'),
(639, 'latim', 'latim', 0, 0, 0.3333, 'L350', 1, '*'),
(640, 'latim combinado', 'latim combinado', 0, 1, 1.5, 'L3525153', 1, '*'),
(641, 'latim combinado com', 'latim combinado com', 0, 1, 1.6333, 'L352515325', 1, '*'),
(642, 'latim consectetur', 'latim consectetur', 0, 1, 1.5667, 'L3525236', 1, '*'),
(643, 'latim consectetur oriunda', 'latim consectetur oriunda', 0, 1, 1.8333, 'L352523653', 1, '*'),
(644, 'latim do', 'latim do', 0, 1, 1.2667, 'L353', 1, '*'),
(645, 'latim do hampden-sydney', 'latim do hampden-sydney', 0, 1, 1.7667, 'L3535135235', 1, '*'),
(646, 'latina', 'latina', 0, 0, 0.4, 'L350', 1, '*'),
(647, 'latina clássica', 'latina clássica', 0, 1, 1.5, 'L35242', 1, '*'),
(648, 'latina clássica datada', 'latina clássica datada', 0, 1, 1.7333, 'L352423', 1, '*'),
(649, 'legível', 'legível', 0, 0, 0.4667, 'L214', 1, '*'),
(650, 'legível de', 'legível de', 0, 1, 1.3333, 'L2143', 1, '*'),
(651, 'legível de uma', 'legível de uma', 0, 1, 1.4667, 'L21435', 1, '*'),
(652, 'legível muitos', 'legível muitos', 0, 1, 1.4667, 'L214532', 1, '*'),
(653, 'legível muitos softwares', 'legível muitos softwares', 0, 1, 1.8, 'L2145321362', 1, '*'),
(654, 'leitor', 'leitor', 0, 0, 0.4, 'L360', 1, '*'),
(655, 'leitor se', 'leitor se', 0, 1, 1.3, 'L362', 1, '*'),
(656, 'leitor se distrairá', 'leitor se distrairá', 0, 1, 1.6333, 'L3623236', 1, '*'),
(657, 'letras', 'letras', 0, 0, 0.4, 'L362', 1, '*'),
(658, 'letras ao', 'letras ao', 0, 1, 1.3, 'L362', 1, '*'),
(659, 'letras ao contrário', 'letras ao contrário', 0, 1, 1.6333, 'L362536', 1, '*'),
(660, 'letraset', 'letraset', 0, 0, 0.5333, 'L3623', 1, '*'),
(661, 'letraset lançou', 'letraset lançou', 0, 1, 1.5, 'L362345', 1, '*'),
(662, 'letraset lançou decalques', 'letraset lançou decalques', 0, 1, 1.8333, 'L3623453242', 1, '*'),
(663, 'linha', 'linha', 0, 0, 0.3333, 'L500', 1, '*'),
(664, 'linha de', 'linha de', 0, 1, 1.2667, 'L530', 1, '*'),
(665, 'linha de lorem', 'linha de lorem', 0, 1, 1.4667, 'L53465', 1, '*'),
(666, 'linha na', 'linha na', 0, 1, 1.2667, 'L500', 1, '*'),
(667, 'linha na seção', 'linha na seção', 0, 1, 1.4667, 'L520', 1, '*'),
(668, 'literatura', 'literatura', 0, 0, 0.6667, 'L3636', 1, '*'),
(669, 'literatura clássica', 'literatura clássica', 0, 1, 1.6333, 'L3636242', 1, '*'),
(670, 'literatura clássica descobriu', 'literatura clássica descobriu', 0, 1, 1.9667, 'L36362423216', 1, '*'),
(671, 'literatura latina', 'literatura latina', 0, 1, 1.5667, 'L3636435', 1, '*'),
(672, 'literatura latina clássica', 'literatura latina clássica', 0, 1, 1.8667, 'L3636435242', 1, '*'),
(673, 'livre', 'livre', 0, 0, 0.3333, 'L160', 1, '*'),
(674, 'livre de', 'livre de', 0, 1, 1.2667, 'L163', 1, '*'),
(675, 'livre de repetições', 'livre de repetições', 0, 1, 1.6333, 'L1636132', 1, '*'),
(676, 'livro', 'livro', 0, 0, 0.3333, 'L160', 1, '*'),
(677, 'livro de', 'livro de', 0, 1, 1.2667, 'L163', 1, '*'),
(678, 'livro de modelos', 'livro de modelos', 0, 1, 1.5333, 'L1635342', 1, '*'),
(679, 'livro é', 'livro é', 0, 1, 1.2333, 'L160', 1, '*'),
(680, 'livro é um', 'livro é um', 0, 1, 1.3333, 'L165', 1, '*'),
(681, 'longo', 'longo', 0, 0, 0.3333, 'L520', 1, '*'),
(682, 'longo dos', 'longo dos', 0, 1, 1.3, 'L5232', 1, '*'),
(683, 'longo dos anos', 'longo dos anos', 0, 1, 1.4667, 'L523252', 1, '*'),
(684, 'lorem', 'lorem', 0, 0, 0.3333, 'L650', 1, '*'),
(685, 'lorem ipsum', 'lorem ipsum', 0, 1, 1.3667, 'L65125', 1, '*'),
(686, 'lorem ipsum 2', 'lorem ipsum 2', 0, 1, 1.4333, 'L65125', 1, '*'),
(687, 'lorem ipsum autêntico', 'lorem ipsum autêntico', 0, 1, 1.7, 'L651253532', 1, '*'),
(688, 'lorem ipsum com', 'lorem ipsum com', 0, 1, 1.5, 'L6512525', 1, '*'),
(689, 'lorem ipsum como', 'lorem ipsum como', 0, 1, 1.5333, 'L6512525', 1, '*'),
(690, 'lorem ipsum dolor', 'lorem ipsum dolor', 0, 1, 1.5667, 'L65125346', 1, '*'),
(691, 'lorem ipsum e', 'lorem ipsum e', 0, 1, 1.4333, 'L65125', 1, '*'),
(692, 'lorem ipsum lorem', 'lorem ipsum lorem', 0, 1, 1.5667, 'L65125465', 1, '*'),
(693, 'lorem ipsum mas', 'lorem ipsum mas', 0, 1, 1.5, 'L651252', 1, '*'),
(694, 'lorem ipsum na', 'lorem ipsum na', 0, 1, 1.4667, 'L65125', 1, '*'),
(695, 'lorem ipsum não', 'lorem ipsum não', 0, 1, 1.5, 'L65125', 1, '*'),
(696, 'lorem ipsum precisa', 'lorem ipsum precisa', 0, 1, 1.6333, 'L65125162', 1, '*'),
(697, 'lorem ipsum sobreviveu', 'lorem ipsum sobreviveu', 0, 1, 1.7333, 'L651252161', 1, '*'),
(698, 'lorem ipsum usado', 'lorem ipsum usado', 0, 1, 1.5667, 'L6512523', 1, '*'),
(699, 'lorem ipsum vem', 'lorem ipsum vem', 0, 1, 1.5, 'L6512515', 1, '*'),
(700, 'maioria', 'maioria', 0, 0, 0.4667, 'M600', 1, '*'),
(701, 'maioria sofreu', 'maioria sofreu', 0, 1, 1.4667, 'M6216', 1, '*'),
(702, 'maioria sofreu algum', 'maioria sofreu algum', 0, 1, 1.6667, 'M6216425', 1, '*'),
(703, 'mais', 'mais', 0, 0, 0.2667, 'M200', 1, '*'),
(704, 'mais de', 'mais de', 0, 1, 1.2333, 'M230', 1, '*'),
(705, 'mais de 200', 'mais de 200', 0, 1, 1.3667, 'M230', 1, '*'),
(706, 'mais de 2000', 'mais de 2000', 0, 1, 1.4, 'M230', 1, '*'),
(707, 'mais obscuras', 'mais obscuras', 0, 1, 1.4333, 'M21262', 1, '*'),
(708, 'mais obscuras palavras', 'mais obscuras palavras', 0, 1, 1.7333, 'M2126214162', 1, '*'),
(709, 'mais recentemente', 'mais recentemente', 0, 1, 1.5667, 'M2625353', 1, '*'),
(710, 'mais recentemente quando', 'mais recentemente quando', 0, 1, 1.8, 'M2625353253', 1, '*'),
(711, 'mal', 'mal', 0, 0, 0.2, 'M400', 1, '*'),
(712, 'mal de', 'mal de', 0, 1, 1.2, 'M430', 1, '*'),
(713, 'mal de cícero', 'mal de cícero', 0, 1, 1.4333, 'M4326', 1, '*'),
(714, 'malorum', 'malorum', 0, 0, 0.4667, 'M465', 1, '*'),
(715, 'malorum de', 'malorum de', 0, 1, 1.3333, 'M4653', 1, '*'),
(716, 'malorum de cicero', 'malorum de cicero', 0, 1, 1.5667, 'M465326', 1, '*'),
(717, 'malorum os', 'malorum os', 0, 1, 1.3333, 'M4652', 1, '*'),
(718, 'malorum os extremos', 'malorum os extremos', 0, 1, 1.6333, 'M46523652', 1, '*'),
(719, 'mas', 'mas', 0, 0, 0.2, 'M200', 1, '*'),
(720, 'mas a', 'mas a', 0, 1, 1.1667, 'M200', 1, '*'),
(721, 'mas a maioria', 'mas a maioria', 0, 1, 1.4333, 'M256', 1, '*'),
(722, 'mcclintock', 'mcclintock', 0, 0, 0.6667, 'M24532', 1, '*'),
(723, 'mcclintock um', 'mcclintock um', 0, 1, 1.4333, 'M245325', 1, '*'),
(724, 'mcclintock um professor', 'mcclintock um professor', 0, 1, 1.7667, 'M24532516126', 1, '*'),
(725, 'meio', 'meio', 0, 0, 0.2667, 'M000', 1, '*');
INSERT INTO `blog_finder_terms` (`term_id`, `term`, `stem`, `common`, `phrase`, `weight`, `soundex`, `links`, `language`) VALUES
(726, 'meio do', 'meio do', 0, 1, 1.2333, 'M300', 1, '*'),
(727, 'meio do texto', 'meio do texto', 0, 1, 1.4333, 'M323', 1, '*'),
(728, 'modelos', 'modelos', 0, 0, 0.4667, 'M342', 1, '*'),
(729, 'modelos de', 'modelos de', 0, 1, 1.3333, 'M3423', 1, '*'),
(730, 'modelos de estrutura', 'modelos de estrutura', 0, 1, 1.6667, 'M342323636', 1, '*'),
(731, 'modelos de tipos', 'modelos de tipos', 0, 1, 1.5333, 'M342312', 1, '*'),
(732, 'mostra', 'mostra', 0, 0, 0.4, 'M236', 1, '*'),
(733, 'mostra vários', 'mostra vários', 0, 1, 1.4333, 'M236162', 1, '*'),
(734, 'mostra vários websites', 'mostra vários websites', 0, 1, 1.7333, 'M2361621232', 1, '*'),
(735, 'muitas', 'muitas', 0, 0, 0.4, 'M320', 1, '*'),
(736, 'muitas variações', 'muitas variações', 0, 1, 1.5333, 'M32162', 1, '*'),
(737, 'muitas variações disponíveis', 'muitas variações disponíveis', 0, 1, 1.9333, 'M32162321512', 1, '*'),
(738, 'muito', 'muito', 0, 0, 0.3333, 'M300', 1, '*'),
(739, 'muito popular', 'muito popular', 0, 1, 1.4333, 'M3146', 1, '*'),
(740, 'muito popular na', 'muito popular na', 0, 1, 1.5333, 'M31465', 1, '*'),
(741, 'muitos', 'muitos', 0, 0, 0.4, 'M320', 1, '*'),
(742, 'muitos softwares', 'muitos softwares', 0, 1, 1.5333, 'M321362', 1, '*'),
(743, 'muitos softwares de', 'muitos softwares de', 0, 1, 1.6333, 'M3213623', 1, '*'),
(744, 'na', 'na', 0, 0, 0.1333, 'N000', 1, '*'),
(745, 'na década', 'na década', 0, 1, 1.3, 'N323', 1, '*'),
(746, 'na década de', 'na década de', 0, 1, 1.4, 'N323', 1, '*'),
(747, 'na época', 'na época', 0, 1, 1.2667, 'N120', 1, '*'),
(748, 'na época da', 'na época da', 0, 1, 1.3667, 'N123', 1, '*'),
(749, 'na internet', 'na internet', 0, 1, 1.3667, 'N3653', 1, '*'),
(750, 'na internet agora', 'na internet agora', 0, 1, 1.5667, 'N365326', 1, '*'),
(751, 'na internet tendem', 'na internet tendem', 0, 1, 1.6, 'N3653535', 1, '*'),
(752, 'na literatura', 'na literatura', 0, 1, 1.4333, 'N43636', 1, '*'),
(753, 'na literatura clássica', 'na literatura clássica', 0, 1, 1.7333, 'N43636242', 1, '*'),
(754, 'na seção', 'na seção', 0, 1, 1.2667, 'N200', 1, '*'),
(755, 'na seção 1.10.32', 'na seção 1.10.32', 0, 1, 1.5333, 'N200', 1, '*'),
(756, 'na virginia', 'na virginia', 0, 1, 1.3667, 'N1625', 1, '*'),
(757, 'na virginia pesquisou', 'na virginia pesquisou', 0, 1, 1.7, 'N162512', 1, '*'),
(758, 'não', 'não', 0, 0, 0.2, 'N000', 1, '*'),
(759, 'não características', 'não características', 0, 1, 1.6333, 'N26236232', 1, '*'),
(760, 'não características etc', 'não características etc', 0, 1, 1.7667, 'N2623623232', 1, '*'),
(761, 'não é', 'não é', 0, 1, 1.1667, 'N000', 1, '*'),
(762, 'não é simplesmente', 'não é simplesmente', 0, 1, 1.6, 'N2514253', 1, '*'),
(763, 'não há', 'não há', 0, 1, 1.2, 'N000', 1, '*'),
(764, 'não há algo', 'não há algo', 0, 1, 1.3667, 'N420', 1, '*'),
(765, 'não parecem', 'não parecem', 0, 1, 1.3667, 'N1625', 1, '*'),
(766, 'não parecem nem', 'não parecem nem', 0, 1, 1.5, 'N1625', 1, '*'),
(767, 'não só', 'não só', 0, 1, 1.2, 'N200', 1, '*'),
(768, 'não só a', 'não só a', 0, 1, 1.2667, 'N200', 1, '*'),
(769, 'necessário', 'necessário', 0, 0, 0.6667, 'N260', 1, '*'),
(770, 'necessário fazendo', 'necessário fazendo', 0, 1, 1.6, 'N261253', 1, '*'),
(771, 'necessário fazendo deste', 'necessário fazendo deste', 0, 1, 1.8, 'N26125323', 1, '*'),
(772, 'nem', 'nem', 0, 0, 0.2, 'N000', 1, '*'),
(773, 'nem um', 'nem um', 0, 1, 1.2, 'N000', 1, '*'),
(774, 'nem um pouco', 'nem um pouco', 0, 1, 1.4, 'N120', 1, '*'),
(775, 'no', 'no', 0, 0, 0.1333, 'N000', 1, '*'),
(776, 'no meio', 'no meio', 0, 1, 1.2333, 'N000', 1, '*'),
(777, 'no meio do', 'no meio do', 0, 1, 1.3333, 'N300', 1, '*'),
(778, 'normal', 'normal', 0, 0, 0.4, 'N654', 1, '*'),
(779, 'normal de', 'normal de', 0, 1, 1.3, 'N6543', 1, '*'),
(780, 'normal de letras', 'normal de letras', 0, 1, 1.5333, 'N65434362', 1, '*'),
(781, 'nós', 'nós', 0, 0, 0.2, 'N200', 1, '*'),
(782, 'nós o', 'nós o', 0, 1, 1.1667, 'N200', 1, '*'),
(783, 'nós o usamos', 'nós o usamos', 0, 1, 1.4, 'N252', 1, '*'),
(784, 'novas', 'novas', 0, 0, 0.3333, 'N120', 1, '*'),
(785, 'novas surgiram', 'novas surgiram', 0, 1, 1.4667, 'N126265', 1, '*'),
(786, 'novas surgiram ao', 'novas surgiram ao', 0, 1, 1.5667, 'N126265', 1, '*'),
(787, 'o', 'o', 0, 0, 0.0667, 'O000', 1, '*'),
(788, 'o conteúdo', 'o conteúdo', 0, 1, 1.3333, 'O253', 1, '*'),
(789, 'o conteúdo de', 'o conteúdo de', 0, 1, 1.4333, 'O253', 1, '*'),
(790, 'o inglês', 'o inglês', 0, 1, 1.2667, 'O5242', 1, '*'),
(791, 'o inglês da', 'o inglês da', 0, 1, 1.3667, 'O52423', 1, '*'),
(792, 'o primeiro', 'o primeiro', 0, 1, 1.3333, 'O1656', 1, '*'),
(793, 'o primeiro gerador', 'o primeiro gerador', 0, 1, 1.6, 'O16562636', 1, '*'),
(794, 'o que', 'o que', 0, 1, 1.1667, 'O200', 1, '*'),
(795, 'o que e', 'o que e', 0, 1, 1.2333, 'O200', 1, '*'),
(796, 'o século', 'o século', 0, 1, 1.2667, 'O240', 1, '*'),
(797, 'o século xvi', 'o século xvi', 0, 1, 1.4, 'O2421', 1, '*'),
(798, 'o trecho', 'o trecho', 0, 1, 1.2667, 'O362', 1, '*'),
(799, 'o trecho padrão', 'o trecho padrão', 0, 1, 1.5, 'O362136', 1, '*'),
(800, 'o usamos', 'o usamos', 0, 1, 1.2667, 'O252', 1, '*'),
(801, 'o usamos é', 'o usamos é', 0, 1, 1.3333, 'O252', 1, '*'),
(802, 'obra', 'obra', 0, 0, 0.2667, 'O160', 1, '*'),
(803, 'obra de', 'obra de', 0, 1, 1.2333, 'O163', 1, '*'),
(804, 'obra de literatura', 'obra de literatura', 0, 1, 1.6, 'O16343636', 1, '*'),
(805, 'obscuras', 'obscuras', 0, 0, 0.5333, 'O1262', 1, '*'),
(806, 'obscuras palavras', 'obscuras palavras', 0, 1, 1.5667, 'O126214162', 1, '*'),
(807, 'obscuras palavras em', 'obscuras palavras em', 0, 1, 1.6667, 'O1262141625', 1, '*'),
(808, 'onde', 'onde', 0, 0, 0.2667, 'O530', 1, '*'),
(809, 'onde ele', 'onde ele', 0, 1, 1.2667, 'O534', 1, '*'),
(810, 'onde ele vem', 'onde ele vem', 0, 1, 1.4, 'O53415', 1, '*'),
(811, 'onde posso', 'onde posso', 0, 1, 1.3333, 'O5312', 1, '*'),
(812, 'onde posso conseguí-lo', 'onde posso conseguí-lo', 0, 1, 1.7333, 'O5312524', 1, '*'),
(813, 'origem', 'origem', 0, 0, 0.4, 'O625', 1, '*'),
(814, 'origem lorem', 'origem lorem', 0, 1, 1.4, 'O625465', 1, '*'),
(815, 'origem lorem ipsum', 'origem lorem ipsum', 0, 1, 1.6, 'O625465125', 1, '*'),
(816, 'original', 'original', 0, 0, 0.5333, 'O6254', 1, '*'),
(817, 'original acompanhada', 'original acompanhada', 0, 1, 1.6667, 'O625425153', 1, '*'),
(818, 'original acompanhada das', 'original acompanhada das', 0, 1, 1.8, 'O6254251532', 1, '*'),
(819, 'original de', 'original de', 0, 1, 1.3667, 'O62543', 1, '*'),
(820, 'original de lorem', 'original de lorem', 0, 1, 1.5667, 'O62543465', 1, '*'),
(821, 'oriunda', 'oriunda', 0, 0, 0.4667, 'O653', 1, '*'),
(822, 'oriunda de', 'oriunda de', 0, 1, 1.3333, 'O653', 1, '*'),
(823, 'oriunda de uma', 'oriunda de uma', 0, 1, 1.4667, 'O6535', 1, '*'),
(824, 'os', 'os', 0, 0, 0.1333, 'O200', 1, '*'),
(825, 'os embaralhou', 'os embaralhou', 0, 1, 1.4333, 'O25164', 1, '*'),
(826, 'os embaralhou para', 'os embaralhou para', 0, 1, 1.6, 'O2516416', 1, '*'),
(827, 'os extremos', 'os extremos', 0, 1, 1.3667, 'O23652', 1, '*'),
(828, 'os extremos do', 'os extremos do', 0, 1, 1.4667, 'O236523', 1, '*'),
(829, 'os geradores', 'os geradores', 0, 1, 1.4, 'O26362', 1, '*'),
(830, 'os geradores de', 'os geradores de', 0, 1, 1.5, 'O263623', 1, '*'),
(831, 'os interessados', 'os interessados', 0, 1, 1.5, 'O2536232', 1, '*'),
(832, 'os interessados seções', 'os interessados seções', 0, 1, 1.7333, 'O2536232', 1, '*'),
(833, 'ou', 'ou', 0, 0, 0.1333, 'O000', 1, '*'),
(834, 'ou palavras', 'ou palavras', 0, 1, 1.3667, 'O14162', 1, '*'),
(835, 'ou palavras aleatórias', 'ou palavras aleatórias', 0, 1, 1.7333, 'O141624362', 1, '*'),
(836, 'padrão', 'padrão', 0, 0, 0.4, 'P360', 1, '*'),
(837, 'padrão e', 'padrão e', 0, 1, 1.2667, 'P360', 1, '*'),
(838, 'padrão e uma', 'padrão e uma', 0, 1, 1.4, 'P365', 1, '*'),
(839, 'padrão original', 'padrão original', 0, 1, 1.5, 'P36254', 1, '*'),
(840, 'padrão original de', 'padrão original de', 0, 1, 1.6, 'P362543', 1, '*'),
(841, 'pagemaker', 'pagemaker', 0, 0, 0.6, 'P2526', 1, '*'),
(842, 'pagemaker porque', 'pagemaker porque', 0, 1, 1.5333, 'P2526162', 1, '*'),
(843, 'pagemaker porque nós', 'pagemaker porque nós', 0, 1, 1.6667, 'P252616252', 1, '*'),
(844, 'página', 'página', 0, 0, 0.4, 'P250', 1, '*'),
(845, 'página quando', 'página quando', 0, 1, 1.4333, 'P25253', 1, '*'),
(846, 'página quando estiver', 'página quando estiver', 0, 1, 1.7, 'P252532316', 1, '*'),
(847, 'páginas', 'páginas', 0, 0, 0.4667, 'P252', 1, '*'),
(848, 'páginas na', 'páginas na', 0, 1, 1.3333, 'P2525', 1, '*'),
(849, 'páginas na internet', 'páginas na internet', 0, 1, 1.6333, 'P25253653', 1, '*'),
(850, 'palavra', 'palavra', 0, 0, 0.4667, 'P416', 1, '*'),
(851, 'palavra na', 'palavra na', 0, 1, 1.3333, 'P4165', 1, '*'),
(852, 'palavra na literatura', 'palavra na literatura', 0, 1, 1.7, 'P416543636', 1, '*'),
(853, 'palavras', 'palavras', 0, 0, 0.5333, 'P4162', 1, '*'),
(854, 'palavras aleatórias', 'palavras aleatórias', 0, 1, 1.6333, 'P41624362', 1, '*'),
(855, 'palavras aleatórias que', 'palavras aleatórias que', 0, 1, 1.7667, 'P41624362', 1, '*'),
(856, 'palavras em', 'palavras em', 0, 1, 1.3667, 'P41625', 1, '*'),
(857, 'palavras em latim', 'palavras em latim', 0, 1, 1.5667, 'P41625435', 1, '*'),
(858, 'palavras não', 'palavras não', 0, 1, 1.4, 'P41625', 1, '*'),
(859, 'palavras não características', 'palavras não características', 0, 1, 1.9333, 'P4162526236232', 1, '*'),
(860, 'para', 'para', 0, 0, 0.2667, 'P600', 1, '*'),
(861, 'para a', 'para a', 0, 1, 1.2, 'P600', 1, '*'),
(862, 'para a editoração', 'para a editoração', 0, 1, 1.5667, 'P636', 1, '*'),
(863, 'para fazer', 'para fazer', 0, 1, 1.3333, 'P6126', 1, '*'),
(864, 'para fazer um', 'para fazer um', 0, 1, 1.4333, 'P61265', 1, '*'),
(865, 'para gerar', 'para gerar', 0, 1, 1.3333, 'P626', 1, '*'),
(866, 'para gerar um', 'para gerar um', 0, 1, 1.4333, 'P6265', 1, '*'),
(867, 'para o', 'para o', 0, 1, 1.2, 'P600', 1, '*'),
(868, 'para o inglês', 'para o inglês', 0, 1, 1.4333, 'P65242', 1, '*'),
(869, 'para os', 'para os', 0, 1, 1.2333, 'P620', 1, '*'),
(870, 'para os interessados', 'para os interessados', 0, 1, 1.6667, 'P62536232', 1, '*'),
(871, 'parecem', 'parecem', 0, 0, 0.4667, 'P625', 1, '*'),
(872, 'parecem nem', 'parecem nem', 0, 1, 1.3667, 'P625', 1, '*'),
(873, 'parecem nem um', 'parecem nem um', 0, 1, 1.4667, 'P625', 1, '*'),
(874, 'passagem', 'passagem', 0, 0, 0.5333, 'P250', 1, '*'),
(875, 'passagem de', 'passagem de', 0, 1, 1.3667, 'P253', 1, '*'),
(876, 'passagem de lorem', 'passagem de lorem', 0, 1, 1.5667, 'P253465', 1, '*'),
(877, 'passagens', 'passagens', 0, 0, 0.6, 'P252', 1, '*'),
(878, 'passagens com', 'passagens com', 0, 1, 1.4333, 'P2525', 1, '*'),
(879, 'passagens com humor', 'passagens com humor', 0, 1, 1.6333, 'P25256', 1, '*'),
(880, 'passagens de', 'passagens de', 0, 1, 1.4, 'P2523', 1, '*'),
(881, 'passagens de lorem', 'passagens de lorem', 0, 1, 1.6, 'P2523465', 1, '*'),
(882, 'passou', 'passou', 0, 0, 0.4, 'P200', 1, '*'),
(883, 'passou a', 'passou a', 0, 1, 1.2667, 'P200', 1, '*'),
(884, 'passou a ser', 'passou a ser', 0, 1, 1.4, 'P260', 1, '*'),
(885, 'pedaços', 'pedaços', 0, 0, 0.4667, 'P320', 1, '*'),
(886, 'pedaços predefinidos', 'pedaços predefinidos', 0, 1, 1.6667, 'P321631532', 1, '*'),
(887, 'pedaços predefinidos conforme', 'pedaços predefinidos conforme', 0, 1, 1.9667, 'P3216315325165', 1, '*'),
(888, 'pegou', 'pegou', 0, 0, 0.3333, 'P200', 1, '*'),
(889, 'pegou uma', 'pegou uma', 0, 1, 1.3, 'P250', 1, '*'),
(890, 'pegou uma bandeja', 'pegou uma bandeja', 0, 1, 1.5667, 'P251532', 1, '*'),
(891, 'permanecendo', 'permanecendo', 0, 0, 0.8, 'P65253', 1, '*'),
(892, 'permanecendo essencialmente', 'permanecendo essencialmente', 0, 1, 1.9, 'P65253252453', 1, '*'),
(893, 'permanecendo essencialmente inalterado', 'permanecendo essencialmente inalterado', 0, 1, 2, 'P6525325245354363', 1, '*'),
(894, 'pesquisou', 'pesquisou', 0, 0, 0.6, 'P200', 1, '*'),
(895, 'pesquisou uma', 'pesquisou uma', 0, 1, 1.4333, 'P250', 1, '*'),
(896, 'pesquisou uma das', 'pesquisou uma das', 0, 1, 1.5667, 'P2532', 1, '*'),
(897, 'podem', 'podem', 0, 0, 0.3333, 'P350', 1, '*'),
(898, 'podem ser', 'podem ser', 0, 1, 1.3, 'P3526', 1, '*'),
(899, 'podem ser encontradas', 'podem ser encontradas', 0, 1, 1.7, 'P35265253632', 1, '*'),
(900, 'popular', 'popular', 0, 0, 0.4667, 'P460', 1, '*'),
(901, 'popular na', 'popular na', 0, 1, 1.3333, 'P465', 1, '*'),
(902, 'popular na época', 'popular na época', 0, 1, 1.5333, 'P46512', 1, '*'),
(903, 'popularizou', 'popularizou', 0, 0, 0.7333, 'P462', 1, '*'),
(904, 'popularizou na', 'popularizou na', 0, 1, 1.4667, 'P4625', 1, '*'),
(905, 'popularizou na década', 'popularizou na década', 0, 1, 1.7, 'P4625323', 1, '*'),
(906, 'por', 'por', 0, 0, 0.2, 'P600', 1, '*'),
(907, 'por \'lorem', 'por lorem', 0, 1, 1.3333, 'P6465', 1, '*'),
(908, 'por \'lorem ipsum\'', 'por lorem ipsum', 0, 1, 1.5667, 'P6465125', 1, '*'),
(909, 'por acidente', 'por acidente', 0, 1, 1.4, 'P62353', 1, '*'),
(910, 'por acidente e', 'por acidente e', 0, 1, 1.4667, 'P62353', 1, '*'),
(911, 'por entre', 'por entre', 0, 1, 1.3, 'P6536', 1, '*'),
(912, 'por entre citações', 'por entre citações', 0, 1, 1.6, 'P6536232', 1, '*'),
(913, 'por h', 'por h', 0, 1, 1.1667, 'P600', 1, '*'),
(914, 'por h rackham', 'por h rackham', 0, 1, 1.4333, 'P625', 1, '*'),
(915, 'por inserção', 'por inserção', 0, 1, 1.4, 'P6526', 1, '*'),
(916, 'por inserção de', 'por inserção de', 0, 1, 1.5, 'P65263', 1, '*'),
(917, 'porque', 'porque', 0, 0, 0.4, 'P620', 1, '*'),
(918, 'porque nós', 'porque nós', 0, 1, 1.3333, 'P6252', 1, '*'),
(919, 'porque nós o', 'porque nós o', 0, 1, 1.4, 'P6252', 1, '*'),
(920, 'posso', 'posso', 0, 0, 0.3333, 'P200', 1, '*'),
(921, 'posso conseguí-lo', 'posso conseguí-lo', 0, 1, 1.5667, 'P2524', 1, '*'),
(922, 'posso conseguí-lo existem', 'posso conseguí-lo existem', 0, 1, 1.8333, 'P2524235', 1, '*'),
(923, 'pouco', 'pouco', 0, 0, 0.3333, 'P200', 1, '*'),
(924, 'pouco convincentes', 'pouco convincentes', 0, 1, 1.6, 'P25152532', 1, '*'),
(925, 'pouco convincentes se', 'pouco convincentes se', 0, 1, 1.7, 'P25152532', 1, '*'),
(926, 'precisa', 'precisa', 0, 0, 0.4667, 'P620', 1, '*'),
(927, 'precisa ter', 'precisa ter', 0, 1, 1.3667, 'P6236', 1, '*'),
(928, 'precisa ter certeza', 'precisa ter certeza', 0, 1, 1.6333, 'P62362632', 1, '*'),
(929, 'predefinidos', 'predefinidos', 0, 0, 0.8, 'P631532', 1, '*'),
(930, 'predefinidos conforme', 'predefinidos conforme', 0, 1, 1.7, 'P6315325165', 1, '*'),
(931, 'predefinidos conforme necessário', 'predefinidos conforme necessário', 0, 1, 2, 'P631532516526', 1, '*'),
(932, 'pretende', 'pretende', 0, 0, 0.5333, 'P6353', 1, '*'),
(933, 'pretende usar', 'pretende usar', 0, 1, 1.4333, 'P635326', 1, '*'),
(934, 'pretende usar uma', 'pretende usar uma', 0, 1, 1.5667, 'P6353265', 1, '*'),
(935, 'primeira', 'primeira', 0, 0, 0.5333, 'P656', 1, '*'),
(936, 'primeira linha', 'primeira linha', 0, 1, 1.4667, 'P65645', 1, '*'),
(937, 'primeira linha de', 'primeira linha de', 0, 1, 1.5667, 'P656453', 1, '*'),
(938, 'primeiro', 'primeiro', 0, 0, 0.5333, 'P656', 1, '*'),
(939, 'primeiro gerador', 'primeiro gerador', 0, 1, 1.5333, 'P6562636', 1, '*'),
(940, 'primeiro gerador de', 'primeiro gerador de', 0, 1, 1.6333, 'P65626363', 1, '*'),
(941, 'procurando', 'procurando', 0, 0, 0.6667, 'P62653', 1, '*'),
(942, 'procurando por', 'procurando por', 0, 1, 1.4667, 'P6265316', 1, '*'),
(943, 'procurando por entre', 'procurando por entre', 0, 1, 1.6667, 'P6265316536', 1, '*'),
(944, 'professor', 'professor', 0, 0, 0.6, 'P6126', 1, '*'),
(945, 'professor de', 'professor de', 0, 1, 1.4, 'P61263', 1, '*'),
(946, 'professor de latim', 'professor de latim', 0, 1, 1.6, 'P61263435', 1, '*'),
(947, 'propósito', 'propósito', 0, 0, 0.6, 'P6123', 1, '*'),
(948, 'propósito injetando', 'propósito injetando', 0, 1, 1.6333, 'P612352353', 1, '*'),
(949, 'propósito injetando humor', 'propósito injetando humor', 0, 1, 1.8333, 'P61235235356', 1, '*'),
(950, 'publicação', 'publicação', 0, 0, 0.6667, 'P420', 1, '*'),
(951, 'publicação e', 'publicação e', 0, 1, 1.4, 'P420', 1, '*'),
(952, 'publicação e editores', 'publicação e editores', 0, 1, 1.7, 'P42362', 1, '*'),
(953, 'punhado', 'punhado', 0, 0, 0.4667, 'P530', 1, '*'),
(954, 'punhado de', 'punhado de', 0, 1, 1.3333, 'P530', 1, '*'),
(955, 'punhado de modelos', 'punhado de modelos', 0, 1, 1.6, 'P535342', 1, '*'),
(956, 'quando', 'quando', 0, 0, 0.4, 'Q530', 1, '*'),
(957, 'quando a', 'quando a', 0, 1, 1.2667, 'Q530', 1, '*'),
(958, 'quando a letraset', 'quando a letraset', 0, 1, 1.5667, 'Q5343623', 1, '*'),
(959, 'quando estiver', 'quando estiver', 0, 1, 1.4667, 'Q532316', 1, '*'),
(960, 'quando estiver examinando', 'quando estiver examinando', 0, 1, 1.8333, 'Q532316253', 1, '*'),
(961, 'quando passou', 'quando passou', 0, 1, 1.4333, 'Q5312', 1, '*'),
(962, 'quando passou a', 'quando passou a', 0, 1, 1.5, 'Q5312', 1, '*'),
(963, 'quando um', 'quando um', 0, 1, 1.3, 'Q535', 1, '*'),
(964, 'quando um impressor', 'quando um impressor', 0, 1, 1.6333, 'Q5351626', 1, '*'),
(965, 'que', 'que', 0, 0, 0.2, 'Q000', 1, '*'),
(966, 'que e', 'que e', 0, 1, 1.1667, 'Q000', 1, '*'),
(967, 'que e lorem', 'que e lorem', 0, 1, 1.3667, 'Q465', 1, '*'),
(968, 'que ele', 'que ele', 0, 1, 1.2333, 'Q400', 1, '*'),
(969, 'que ele tem', 'que ele tem', 0, 1, 1.3667, 'Q435', 1, '*'),
(970, 'que ele tenha', 'que ele tenha', 0, 1, 1.4333, 'Q435', 1, '*'),
(971, 'que não', 'que não', 0, 1, 1.2333, 'Q500', 1, '*'),
(972, 'que não há', 'que não há', 0, 1, 1.3333, 'Q500', 1, '*'),
(973, 'que não parecem', 'que não parecem', 0, 1, 1.5, 'Q51625', 1, '*'),
(974, 'que se', 'que se', 0, 1, 1.2, 'Q000', 1, '*'),
(975, 'que se acredita', 'que se acredita', 0, 1, 1.5, 'Q630', 1, '*'),
(976, 'que um', 'que um', 0, 1, 1.2, 'Q500', 1, '*'),
(977, 'que um leitor', 'que um leitor', 0, 1, 1.4333, 'Q5436', 1, '*'),
(978, 'rackham', 'rackham', 0, 0, 0.4667, 'R250', 1, '*'),
(979, 'rackham em', 'rackham em', 0, 1, 1.3333, 'R250', 1, '*'),
(980, 'rackham em 1914', 'rackham em 1914', 0, 1, 1.5, 'R250', 1, '*'),
(981, 'raízes', 'raízes', 0, 0, 0.4, 'R200', 1, '*'),
(982, 'raízes podem', 'raízes podem', 0, 1, 1.4, 'R2135', 1, '*'),
(983, 'raízes podem ser', 'raízes podem ser', 0, 1, 1.5333, 'R213526', 1, '*'),
(984, 'randômico', 'randômico', 0, 0, 0.6, 'R5352', 1, '*'),
(985, 'randômico com', 'randômico com', 0, 1, 1.4333, 'R53525', 1, '*'),
(986, 'randômico com mais', 'randômico com mais', 0, 1, 1.6, 'R535252', 1, '*'),
(987, 'rápida', 'rápida', 0, 0, 0.4, 'R130', 1, '*'),
(988, 'rápida busca', 'rápida busca', 0, 1, 1.4, 'R1312', 1, '*'),
(989, 'rápida busca por', 'rápida busca por', 0, 1, 1.5333, 'R131216', 1, '*'),
(990, 'razoável', 'razoável', 0, 0, 0.5333, 'R214', 1, '*'),
(991, 'razoável livre', 'razoável livre', 0, 1, 1.4667, 'R21416', 1, '*'),
(992, 'razoável livre de', 'razoável livre de', 0, 1, 1.5667, 'R214163', 1, '*'),
(993, 'recentemente', 'recentemente', 0, 0, 0.8, 'R25353', 1, '*'),
(994, 'recentemente quando', 'recentemente quando', 0, 1, 1.6333, 'R25353253', 1, '*'),
(995, 'recentemente quando passou', 'recentemente quando passou', 0, 1, 1.8667, 'R2535325312', 1, '*'),
(996, 'renascença', 'renascença', 0, 0, 0.6667, 'R525', 1, '*'),
(997, 'renascença a', 'renascença a', 0, 1, 1.4, 'R525', 1, '*'),
(998, 'renascença a primeira', 'renascença a primeira', 0, 1, 1.7, 'R5251656', 1, '*'),
(999, 'repetições', 'repetições', 0, 0, 0.6667, 'R132', 1, '*'),
(1000, 'repetições inserções', 'repetições inserções', 0, 1, 1.6667, 'R1325262', 1, '*'),
(1001, 'repetições inserções de', 'repetições inserções de', 0, 1, 1.7667, 'R13252623', 1, '*'),
(1002, 'repetir', 'repetir', 0, 0, 0.4667, 'R136', 1, '*'),
(1003, 'repetir pedaços', 'repetir pedaços', 0, 1, 1.5, 'R136132', 1, '*'),
(1004, 'repetir pedaços predefinidos', 'repetir pedaços predefinidos', 0, 1, 1.9333, 'R1361321631532', 1, '*'),
(1005, 'reproduzidas', 'reproduzidas', 0, 0, 0.8, 'R163232', 1, '*'),
(1006, 'reproduzidas abaixo', 'reproduzidas abaixo', 0, 1, 1.6333, 'R16323212', 1, '*'),
(1007, 'reproduzidas abaixo em', 'reproduzidas abaixo em', 0, 1, 1.7333, 'R163232125', 1, '*'),
(1008, 'reproduzido', 'reproduzido', 0, 0, 0.7333, 'R16323', 1, '*'),
(1009, 'reproduzido abaixo', 'reproduzido abaixo', 0, 1, 1.6, 'R1632312', 1, '*'),
(1010, 'reproduzido abaixo para', 'reproduzido abaixo para', 0, 1, 1.7667, 'R163231216', 1, '*'),
(1011, 'richard', 'richard', 0, 0, 0.4667, 'R263', 1, '*'),
(1012, 'richard mcclintock', 'richard mcclintock', 0, 1, 1.6, 'R263524532', 1, '*'),
(1013, 'richard mcclintock um', 'richard mcclintock um', 0, 1, 1.7, 'R2635245325', 1, '*'),
(1014, 'salto', 'salto', 0, 0, 0.3333, 'S430', 1, '*'),
(1015, 'salto para', 'salto para', 0, 1, 1.3333, 'S4316', 1, '*'),
(1016, 'salto para a', 'salto para a', 0, 1, 1.4, 'S4316', 1, '*'),
(1017, 'se', 'se', 0, 0, 0.1333, 'S000', 1, '*'),
(1018, 'se acredita', 'se acredita', 0, 1, 1.3667, 'S630', 1, '*'),
(1019, 'se acredita lorem', 'se acredita lorem', 0, 1, 1.5667, 'S63465', 1, '*'),
(1020, 'se distrairá', 'se distrairá', 0, 1, 1.4, 'S3236', 1, '*'),
(1021, 'se distrairá com', 'se distrairá com', 0, 1, 1.5333, 'S323625', 1, '*'),
(1022, 'se popularizou', 'se popularizou', 0, 1, 1.4667, 'S1462', 1, '*'),
(1023, 'se popularizou na', 'se popularizou na', 0, 1, 1.5667, 'S14625', 1, '*'),
(1024, 'se você', 'se você', 0, 1, 1.2333, 'S120', 1, '*'),
(1025, 'se você pretende', 'se você pretende', 0, 1, 1.5333, 'S1216353', 1, '*'),
(1026, 'seção', 'seção', 0, 0, 0.3333, 'S000', 1, '*'),
(1027, 'seção 1.10.32', 'seção 1.10.32', 0, 1, 1.4333, 'S000', 1, '*'),
(1028, 'seção 1.10.32 o', 'seção 1.10.32 o', 0, 1, 1.5, 'S000', 1, '*'),
(1029, 'seções', 'seções', 0, 0, 0.4, 'S000', 1, '*'),
(1030, 'seções 1.10.32', 'seções 1.10.32', 0, 1, 1.4667, 'S000', 1, '*'),
(1031, 'seções 1.10.32 e', 'seções 1.10.32 e', 0, 1, 1.5333, 'S000', 1, '*'),
(1032, 'século', 'século', 0, 0, 0.4, 'S400', 1, '*'),
(1033, 'século xvi', 'século xvi', 0, 1, 1.3333, 'S421', 1, '*'),
(1034, 'século xvi está', 'século xvi está', 0, 1, 1.5, 'S42123', 1, '*'),
(1035, 'século xvi quando', 'século xvi quando', 0, 1, 1.5667, 'S421253', 1, '*'),
(1036, 'séculos', 'séculos', 0, 0, 0.4667, 'S420', 1, '*'),
(1037, 'séculos como', 'séculos como', 0, 1, 1.4, 'S425', 1, '*'),
(1038, 'séculos como também', 'séculos como também', 0, 1, 1.6333, 'S4253515', 1, '*'),
(1039, 'seja', 'seja', 0, 0, 0.2667, 'S000', 1, '*'),
(1040, 'seja por', 'seja por', 0, 1, 1.2667, 'S160', 1, '*'),
(1041, 'seja por inserção', 'seja por inserção', 0, 1, 1.5667, 'S16526', 1, '*'),
(1042, 'sendo', 'sendo', 0, 0, 0.3333, 'S530', 1, '*'),
(1043, 'sendo utilizado', 'sendo utilizado', 0, 1, 1.5, 'S53423', 1, '*'),
(1044, 'sendo utilizado desde', 'sendo utilizado desde', 0, 1, 1.7, 'S5342323', 1, '*'),
(1045, 'ser', 'ser', 0, 0, 0.2, 'S600', 1, '*'),
(1046, 'ser encontradas', 'ser encontradas', 0, 1, 1.5, 'S65253632', 1, '*'),
(1047, 'ser encontradas em', 'ser encontradas em', 0, 1, 1.6, 'S652536325', 1, '*'),
(1048, 'ser integrado', 'ser integrado', 0, 1, 1.4333, 'S653263', 1, '*'),
(1049, 'ser integrado a', 'ser integrado a', 0, 1, 1.5, 'S653263', 1, '*'),
(1050, 'similar', 'similar', 0, 0, 0.4667, 'S546', 1, '*'),
(1051, 'similar a', 'similar a', 0, 1, 1.3, 'S546', 1, '*'),
(1052, 'similar a de', 'similar a de', 0, 1, 1.4, 'S5463', 1, '*'),
(1053, 'simplesmente', 'simplesmente', 0, 0, 0.8, 'S514253', 1, '*'),
(1054, 'simplesmente um', 'simplesmente um', 0, 1, 1.5, 'S5142535', 1, '*'),
(1055, 'simplesmente um texto', 'simplesmente um texto', 0, 1, 1.7, 'S5142535323', 1, '*'),
(1056, 'simplesmente uma', 'simplesmente uma', 0, 1, 1.5333, 'S5142535', 1, '*'),
(1057, 'simplesmente uma simulação', 'simplesmente uma simulação', 0, 1, 1.8667, 'S5142535254', 1, '*'),
(1058, 'simulação', 'simulação', 0, 0, 0.6, 'S540', 1, '*'),
(1059, 'simulação de', 'simulação de', 0, 1, 1.4, 'S543', 1, '*'),
(1060, 'simulação de texto', 'simulação de texto', 0, 1, 1.6, 'S54323', 1, '*'),
(1061, 'sit', 'sit', 0, 0, 0.2, 'S300', 1, '*'),
(1062, 'sit amet', 'sit amet', 0, 1, 1.2667, 'S353', 1, '*'),
(1063, 'sit amet vem', 'sit amet vem', 0, 1, 1.4, 'S35315', 1, '*'),
(1064, 'só', 'só', 0, 0, 0.1333, 'S000', 1, '*'),
(1065, 'só a', 'só a', 0, 1, 1.1333, 'S000', 1, '*'),
(1066, 'só a cinco', 'só a cinco', 0, 1, 1.3333, 'S520', 1, '*'),
(1067, 'sobreviveu', 'sobreviveu', 0, 0, 0.6667, 'S161', 1, '*'),
(1068, 'sobreviveu não', 'sobreviveu não', 0, 1, 1.4667, 'S1615', 1, '*'),
(1069, 'sobreviveu não só', 'sobreviveu não só', 0, 1, 1.5667, 'S16152', 1, '*'),
(1070, 'sofreu', 'sofreu', 0, 0, 0.4, 'S160', 1, '*'),
(1071, 'sofreu algum', 'sofreu algum', 0, 1, 1.4, 'S16425', 1, '*'),
(1072, 'sofreu algum tipo', 'sofreu algum tipo', 0, 1, 1.5667, 'S1642531', 1, '*'),
(1073, 'softwares', 'softwares', 0, 0, 0.6, 'S1362', 1, '*'),
(1074, 'softwares de', 'softwares de', 0, 1, 1.4, 'S13623', 1, '*'),
(1075, 'softwares de editoração', 'softwares de editoração', 0, 1, 1.7667, 'S136236', 1, '*'),
(1076, 'softwares de publicação', 'softwares de publicação', 0, 1, 1.7667, 'S13623142', 1, '*'),
(1077, 'sua', 'sua', 0, 0, 0.2, 'S000', 1, '*'),
(1078, 'sua diagramação', 'sua diagramação', 0, 1, 1.5, 'S3265', 1, '*'),
(1079, 'sua diagramação a', 'sua diagramação a', 0, 1, 1.5667, 'S3265', 1, '*'),
(1080, 'sua fase', 'sua fase', 0, 1, 1.2667, 'S120', 1, '*'),
(1081, 'sua fase de', 'sua fase de', 0, 1, 1.3667, 'S123', 1, '*'),
(1082, 'sua forma', 'sua forma', 0, 1, 1.3, 'S165', 1, '*'),
(1083, 'sua forma exata', 'sua forma exata', 0, 1, 1.5, 'S16523', 1, '*'),
(1084, 'sua indubitável', 'sua indubitável', 0, 1, 1.5, 'S531314', 1, '*'),
(1085, 'sua indubitável origem', 'sua indubitável origem', 0, 1, 1.7333, 'S531314625', 1, '*'),
(1086, 'suas', 'suas', 0, 0, 0.2667, 'S000', 1, '*'),
(1087, 'suas raízes', 'suas raízes', 0, 1, 1.3667, 'S620', 1, '*'),
(1088, 'suas raízes podem', 'suas raízes podem', 0, 1, 1.5667, 'S62135', 1, '*'),
(1089, 'super', 'super', 0, 0, 0.3333, 'S160', 1, '*'),
(1090, 'super user', 'super user', 0, 1, 1.3333, 'S1626', 1, '*'),
(1091, 'surgiram', 'surgiram', 0, 0, 0.5333, 'S6265', 1, '*'),
(1092, 'surgiram ao', 'surgiram ao', 0, 1, 1.3667, 'S6265', 1, '*'),
(1093, 'surgiram ao longo', 'surgiram ao longo', 0, 1, 1.5667, 'S6265452', 1, '*'),
(1094, 'também', 'também', 0, 0, 0.4, 'T515', 1, '*'),
(1095, 'também ao', 'também ao', 0, 1, 1.3, 'T515', 1, '*'),
(1096, 'também ao salto', 'também ao salto', 0, 1, 1.5, 'T515243', 1, '*'),
(1097, 'também foram', 'também foram', 0, 1, 1.4, 'T515165', 1, '*'),
(1098, 'também foram reproduzidas', 'também foram reproduzidas', 0, 1, 1.8333, 'T5151656163232', 1, '*'),
(1099, 'tem', 'tem', 0, 0, 0.2, 'T500', 1, '*'),
(1100, 'tem uma', 'tem uma', 0, 1, 1.2333, 'T500', 1, '*'),
(1101, 'tem uma distribuição', 'tem uma distribuição', 0, 1, 1.6667, 'T532361', 1, '*'),
(1102, 'tendem', 'tendem', 0, 0, 0.4, 'T535', 1, '*'),
(1103, 'tendem a', 'tendem a', 0, 1, 1.2667, 'T535', 1, '*'),
(1104, 'tendem a repetir', 'tendem a repetir', 0, 1, 1.5333, 'T5356136', 1, '*'),
(1105, 'tenha', 'tenha', 0, 0, 0.3333, 'T500', 1, '*'),
(1106, 'tenha uma', 'tenha uma', 0, 1, 1.3, 'T500', 1, '*'),
(1107, 'tenha uma aparência', 'tenha uma aparência', 0, 1, 1.6333, 'T51652', 1, '*'),
(1108, 'teoria', 'teoria', 0, 0, 0.4, 'T600', 1, '*'),
(1109, 'teoria da', 'teoria da', 0, 1, 1.3, 'T630', 1, '*'),
(1110, 'teoria da ética', 'teoria da ética', 0, 1, 1.5, 'T632', 1, '*'),
(1111, 'ter', 'ter', 0, 0, 0.2, 'T600', 1, '*'),
(1112, 'ter certeza', 'ter certeza', 0, 1, 1.3667, 'T62632', 1, '*'),
(1113, 'ter certeza de', 'ter certeza de', 0, 1, 1.4667, 'T626323', 1, '*'),
(1114, 'texto', 'texto', 0, 0, 0.3333, 'T230', 1, '*'),
(1115, 'texto da', 'texto da', 0, 1, 1.2667, 'T230', 1, '*'),
(1116, 'texto da indústria', 'texto da indústria', 0, 1, 1.6, 'T2353236', 1, '*'),
(1117, 'texto legível', 'texto legível', 0, 1, 1.4333, 'T234214', 1, '*'),
(1118, 'texto legível de', 'texto legível de', 0, 1, 1.5333, 'T2342143', 1, '*'),
(1119, 'texto legível muitos', 'texto legível muitos', 0, 1, 1.6667, 'T234214532', 1, '*'),
(1120, 'texto randômico', 'texto randômico', 0, 1, 1.5, 'T2365352', 1, '*'),
(1121, 'texto randômico com', 'texto randômico com', 0, 1, 1.6333, 'T23653525', 1, '*'),
(1122, 'texto todos', 'texto todos', 0, 1, 1.3667, 'T232', 1, '*'),
(1123, 'texto todos os', 'texto todos os', 0, 1, 1.4667, 'T232', 1, '*'),
(1124, 'texto-modelo', 'texto-modelo', 0, 0, 0.8, 'T23534', 1, '*'),
(1125, 'texto-modelo padrão', 'texto-modelo padrão', 0, 1, 1.6333, 'T23534136', 1, '*'),
(1126, 'texto-modelo padrão e', 'texto-modelo padrão e', 0, 1, 1.7, 'T23534136', 1, '*'),
(1127, 'tipo', 'tipo', 0, 0, 0.2667, 'T100', 1, '*'),
(1128, 'tipo de', 'tipo de', 0, 1, 1.2333, 'T130', 1, '*'),
(1129, 'tipo de alteração', 'tipo de alteração', 0, 1, 1.5667, 'T13436', 1, '*'),
(1130, 'tipográfica', 'tipográfica', 0, 0, 0.7333, 'T12612', 1, '*'),
(1131, 'tipográfica e', 'tipográfica e', 0, 1, 1.4333, 'T12612', 1, '*'),
(1132, 'tipográfica e de', 'tipográfica e de', 0, 1, 1.5333, 'T126123', 1, '*'),
(1133, 'tipos', 'tipos', 0, 0, 0.3333, 'T120', 1, '*'),
(1134, 'tipos e', 'tipos e', 0, 1, 1.2333, 'T120', 1, '*'),
(1135, 'tipos e os', 'tipos e os', 0, 1, 1.3333, 'T120', 1, '*'),
(1136, 'tipos lorem', 'tipos lorem', 0, 1, 1.3667, 'T12465', 1, '*'),
(1137, 'tipos lorem ipsum', 'tipos lorem ipsum', 0, 1, 1.5667, 'T12465125', 1, '*'),
(1138, 'todos', 'todos', 0, 0, 0.3333, 'T200', 1, '*'),
(1139, 'todos os', 'todos os', 0, 1, 1.2667, 'T200', 1, '*'),
(1140, 'todos os geradores', 'todos os geradores', 0, 1, 1.6, 'T26362', 1, '*'),
(1141, 'todos que', 'todos que', 0, 1, 1.3, 'T200', 1, '*'),
(1142, 'todos que um', 'todos que um', 0, 1, 1.4, 'T250', 1, '*'),
(1143, 'tradução', 'tradução', 0, 0, 0.5333, 'T630', 1, '*'),
(1144, 'tradução feita', 'tradução feita', 0, 1, 1.4667, 'T6313', 1, '*'),
(1145, 'tradução feita por', 'tradução feita por', 0, 1, 1.6, 'T631316', 1, '*'),
(1146, 'tratado', 'tratado', 0, 0, 0.4667, 'T630', 1, '*'),
(1147, 'tratado de', 'tratado de', 0, 1, 1.3333, 'T630', 1, '*'),
(1148, 'tratado de teoria', 'tratado de teoria', 0, 1, 1.5667, 'T636', 1, '*'),
(1149, 'trecho', 'trecho', 0, 0, 0.4, 'T620', 1, '*'),
(1150, 'trecho padrão', 'trecho padrão', 0, 1, 1.4333, 'T62136', 1, '*'),
(1151, 'trecho padrão original', 'trecho padrão original', 0, 1, 1.7333, 'T62136254', 1, '*'),
(1152, 'um', 'um', 0, 0, 0.1333, 'U500', 1, '*'),
(1153, 'um dicionário', 'um dicionário', 0, 1, 1.4333, 'U53256', 1, '*'),
(1154, 'um dicionário com', 'um dicionário com', 0, 1, 1.5667, 'U5325625', 1, '*'),
(1155, 'um fato', 'um fato', 0, 1, 1.2333, 'U513', 1, '*'),
(1156, 'um fato conhecido', 'um fato conhecido', 0, 1, 1.5667, 'U5132523', 1, '*'),
(1157, 'um impressor', 'um impressor', 0, 1, 1.4, 'U51626', 1, '*'),
(1158, 'um impressor desconhecido', 'um impressor desconhecido', 0, 1, 1.8333, 'U5162632523', 1, '*'),
(1159, 'um leitor', 'um leitor', 0, 1, 1.3, 'U5436', 1, '*'),
(1160, 'um leitor se', 'um leitor se', 0, 1, 1.4, 'U54362', 1, '*'),
(1161, 'um livro', 'um livro', 0, 1, 1.2667, 'U5416', 1, '*'),
(1162, 'um livro de', 'um livro de', 0, 1, 1.3667, 'U54163', 1, '*'),
(1163, 'um lorem', 'um lorem', 0, 1, 1.2667, 'U5465', 1, '*'),
(1164, 'um lorem ipsum', 'um lorem ipsum', 0, 1, 1.4667, 'U5465125', 1, '*'),
(1165, 'um pouco', 'um pouco', 0, 1, 1.2667, 'U512', 1, '*'),
(1166, 'um pouco convincentes', 'um pouco convincentes', 0, 1, 1.7, 'U5125152532', 1, '*'),
(1167, 'um professor', 'um professor', 0, 1, 1.4, 'U516126', 1, '*'),
(1168, 'um professor de', 'um professor de', 0, 1, 1.5, 'U5161263', 1, '*'),
(1169, 'um punhado', 'um punhado', 0, 1, 1.3333, 'U5153', 1, '*'),
(1170, 'um punhado de', 'um punhado de', 0, 1, 1.4333, 'U5153', 1, '*'),
(1171, 'um texto', 'um texto', 0, 1, 1.2667, 'U5323', 1, '*'),
(1172, 'um texto legível', 'um texto legível', 0, 1, 1.5333, 'U53234214', 1, '*'),
(1173, 'um texto randômico', 'um texto randômico', 0, 1, 1.6, 'U532365352', 1, '*'),
(1174, 'um tratado', 'um tratado', 0, 1, 1.3333, 'U5363', 1, '*'),
(1175, 'um tratado de', 'um tratado de', 0, 1, 1.4333, 'U5363', 1, '*'),
(1176, 'uma', 'uma', 0, 0, 0.2, 'U500', 1, '*'),
(1177, 'uma aparência', 'uma aparência', 0, 1, 1.4333, 'U51652', 1, '*'),
(1178, 'uma aparência similar', 'uma aparência similar', 0, 1, 1.7, 'U51652546', 1, '*'),
(1179, 'uma bandeja', 'uma bandeja', 0, 1, 1.3667, 'U51532', 1, '*'),
(1180, 'uma bandeja de', 'uma bandeja de', 0, 1, 1.4667, 'U515323', 1, '*'),
(1181, 'uma das', 'uma das', 0, 1, 1.2333, 'U532', 1, '*'),
(1182, 'uma das mais', 'uma das mais', 0, 1, 1.4, 'U53252', 1, '*'),
(1183, 'uma distribuição', 'uma distribuição', 0, 1, 1.5333, 'U532361', 1, '*'),
(1184, 'uma distribuição normal', 'uma distribuição normal', 0, 1, 1.7667, 'U5323615654', 1, '*'),
(1185, 'uma linha', 'uma linha', 0, 1, 1.3, 'U545', 1, '*'),
(1186, 'uma linha na', 'uma linha na', 0, 1, 1.4, 'U545', 1, '*'),
(1187, 'uma obra', 'uma obra', 0, 1, 1.2667, 'U516', 1, '*'),
(1188, 'uma obra de', 'uma obra de', 0, 1, 1.3667, 'U5163', 1, '*'),
(1189, 'uma página', 'uma página', 0, 1, 1.3333, 'U5125', 1, '*'),
(1190, 'uma página quando', 'uma página quando', 0, 1, 1.5667, 'U5125253', 1, '*'),
(1191, 'uma passagem', 'uma passagem', 0, 1, 1.4, 'U5125', 1, '*'),
(1192, 'uma passagem de', 'uma passagem de', 0, 1, 1.5, 'U51253', 1, '*'),
(1193, 'uma rápida', 'uma rápida', 0, 1, 1.3333, 'U5613', 1, '*'),
(1194, 'uma rápida busca', 'uma rápida busca', 0, 1, 1.5333, 'U561312', 1, '*'),
(1195, 'uma simulação', 'uma simulação', 0, 1, 1.4333, 'U5254', 1, '*'),
(1196, 'uma simulação de', 'uma simulação de', 0, 1, 1.5333, 'U52543', 1, '*'),
(1197, 'usa', 'usa', 0, 0, 0.2, 'U200', 1, '*'),
(1198, 'usa um', 'usa um', 0, 1, 1.2, 'U250', 1, '*'),
(1199, 'usa um dicionário', 'usa um dicionário', 0, 1, 1.5667, 'U253256', 1, '*'),
(1200, 'usado', 'usado', 0, 0, 0.3333, 'U230', 1, '*'),
(1201, 'usado desde', 'usado desde', 0, 1, 1.3667, 'U2323', 1, '*'),
(1202, 'usado desde o', 'usado desde o', 0, 1, 1.4333, 'U2323', 1, '*'),
(1203, 'usam', 'usam', 0, 0, 0.2667, 'U250', 1, '*'),
(1204, 'usam lorem', 'usam lorem', 0, 1, 1.3333, 'U25465', 1, '*'),
(1205, 'usam lorem ipsum', 'usam lorem ipsum', 0, 1, 1.5333, 'U25465125', 1, '*'),
(1206, 'usamos', 'usamos', 0, 0, 0.4, 'U252', 1, '*'),
(1207, 'usamos é', 'usamos é', 0, 1, 1.2667, 'U252', 1, '*'),
(1208, 'usamos é um', 'usamos é um', 0, 1, 1.3667, 'U2525', 1, '*'),
(1209, 'usar', 'usar', 0, 0, 0.2667, 'U260', 1, '*'),
(1210, 'usar lorem', 'usar lorem', 0, 1, 1.3333, 'U26465', 1, '*'),
(1211, 'usar lorem ipsum', 'usar lorem ipsum', 0, 1, 1.5333, 'U26465125', 1, '*'),
(1212, 'usar uma', 'usar uma', 0, 1, 1.2667, 'U265', 1, '*'),
(1213, 'usar uma passagem', 'usar uma passagem', 0, 1, 1.5667, 'U265125', 1, '*'),
(1214, 'user', 'user', 0, 0, 0.2667, 'U260', 1, '*'),
(1215, 'utilizado', 'utilizado', 0, 0, 0.6, 'U3423', 1, '*'),
(1216, 'utilizado desde', 'utilizado desde', 0, 1, 1.5, 'U342323', 1, '*'),
(1217, 'utilizado desde o', 'utilizado desde o', 0, 1, 1.5667, 'U342323', 1, '*'),
(1218, 'vantagem', 'vantagem', 0, 0, 0.5333, 'V5325', 1, '*'),
(1219, 'vantagem de', 'vantagem de', 0, 1, 1.3667, 'V53253', 1, '*'),
(1220, 'vantagem de usar', 'vantagem de usar', 0, 1, 1.5333, 'V5325326', 1, '*'),
(1221, 'variações', 'variações', 0, 0, 0.6, 'V620', 1, '*'),
(1222, 'variações disponíveis', 'variações disponíveis', 0, 1, 1.7, 'V62321512', 1, '*'),
(1223, 'variações disponíveis de', 'variações disponíveis de', 0, 1, 1.8, 'V623215123', 1, '*'),
(1224, 'várias', 'várias', 0, 0, 0.4, 'V620', 1, '*'),
(1225, 'várias versões', 'várias versões', 0, 1, 1.4667, 'V62162', 1, '*'),
(1226, 'várias versões novas', 'várias versões novas', 0, 1, 1.6667, 'V62162512', 1, '*'),
(1227, 'vários', 'vários', 0, 0, 0.4, 'V620', 1, '*'),
(1228, 'vários websites', 'vários websites', 0, 1, 1.5, 'V621232', 1, '*'),
(1229, 'vários websites ainda', 'vários websites ainda', 0, 1, 1.7, 'V62123253', 1, '*'),
(1230, 'vem', 'vem', 0, 0, 0.2, 'V500', 1, '*'),
(1231, 'vem ao', 'vem ao', 0, 1, 1.2, 'V500', 1, '*'),
(1232, 'vem ao contrário', 'vem ao contrário', 0, 1, 1.5333, 'V52536', 1, '*'),
(1233, 'vem das', 'vem das', 0, 1, 1.2333, 'V532', 1, '*'),
(1234, 'vem das seções', 'vem das seções', 0, 1, 1.4667, 'V532', 1, '*'),
(1235, 'vem de', 'vem de', 0, 1, 1.2, 'V530', 1, '*'),
(1236, 'vem de uma', 'vem de uma', 0, 1, 1.3333, 'V535', 1, '*'),
(1237, 'vem sendo', 'vem sendo', 0, 1, 1.3, 'V5253', 1, '*'),
(1238, 'vem sendo utilizado', 'vem sendo utilizado', 0, 1, 1.6333, 'V5253423', 1, '*'),
(1239, 'versões', 'versões', 0, 0, 0.4667, 'V620', 1, '*'),
(1240, 'versões novas', 'versões novas', 0, 1, 1.4333, 'V62512', 1, '*'),
(1241, 'versões novas surgiram', 'versões novas surgiram', 0, 1, 1.7333, 'V625126265', 1, '*'),
(1242, 'versões para', 'versões para', 0, 1, 1.4, 'V6216', 1, '*'),
(1243, 'versões para o', 'versões para o', 0, 1, 1.4667, 'V6216', 1, '*'),
(1244, 'vezes', 'vezes', 0, 0, 0.3333, 'V200', 1, '*'),
(1245, 'vezes de', 'vezes de', 0, 1, 1.2667, 'V230', 1, '*'),
(1246, 'vezes de propósito', 'vezes de propósito', 0, 1, 1.6, 'V2316123', 1, '*'),
(1247, 'virginia', 'virginia', 0, 0, 0.5333, 'V625', 1, '*'),
(1248, 'virginia pesquisou', 'virginia pesquisou', 0, 1, 1.6, 'V62512', 1, '*'),
(1249, 'virginia pesquisou uma', 'virginia pesquisou uma', 0, 1, 1.7333, 'V625125', 1, '*'),
(1250, 'você', 'você', 0, 0, 0.2667, 'V200', 1, '*'),
(1251, 'você pretende', 'você pretende', 0, 1, 1.4333, 'V216353', 1, '*'),
(1252, 'você pretende usar', 'você pretende usar', 0, 1, 1.6, 'V21635326', 1, '*'),
(1253, 'websites', 'websites', 0, 0, 0.5333, 'W1232', 1, '*'),
(1254, 'websites ainda', 'websites ainda', 0, 1, 1.4667, 'W123253', 1, '*'),
(1255, 'websites ainda em', 'websites ainda em', 0, 1, 1.5667, 'W1232535', 1, '*'),
(1256, 'xvi', 'xvi', 0, 0, 0.2, 'X100', 1, '*'),
(1257, 'xvi está', 'xvi está', 0, 1, 1.2667, 'X123', 1, '*'),
(1258, 'xvi está reproduzido', 'xvi está reproduzido', 0, 1, 1.6667, 'X123616323', 1, '*'),
(1259, 'xvi quando', 'xvi quando', 0, 1, 1.3333, 'X1253', 1, '*'),
(1260, 'xvi quando um', 'xvi quando um', 0, 1, 1.4333, 'X12535', 1, '*');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_terms_common`
--

CREATE TABLE `blog_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_terms_common`
--

INSERT INTO `blog_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_tokens`
--

CREATE TABLE `blog_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `weight` float UNSIGNED NOT NULL DEFAULT 1,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT 2,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_tokens_aggregate`
--

CREATE TABLE `blog_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT 2,
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_finder_types`
--

CREATE TABLE `blog_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `blog_finder_types`
--

INSERT INTO `blog_finder_types` (`id`, `title`, `mime`) VALUES
(1, 'Tag', ''),
(2, 'Category', ''),
(3, 'Contact', ''),
(4, 'Article', ''),
(5, 'News Feed', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_languages`
--

CREATE TABLE `blog_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_languages`
--

INSERT INTO `blog_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (en-GB)', 'English (United Kingdom)', 'en', 'en_gb', '', '', '', '', 1, 1, 2),
(2, 60, 'pt-BR', 'Português do Brasil (pt-BR)', 'Português Brasileiro (pt-BR)', 'pt', 'pt_br', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_menu`
--

CREATE TABLE `blog_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to #__users.id',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_menu`
--

INSERT INTO `blog_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 65, 0, '*', 0),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 20, 0, '*', 1),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 21, 26, 0, '*', 1),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 22, 23, 0, '*', 1),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 24, 25, 0, '*', 1),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 27, 28, 0, '*', 1),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 29, 30, 0, '*', 1),
(18, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 31, 32, 0, '*', 1),
(19, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 33, 34, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 35, 36, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 37, 38, 0, '*', 1),
(22, 'main', 'com_associations', 'Multilingual Associations', '', 'Multilingual Associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 0, 'class:associations', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 9, '{\"featured_categories\":[\"\"],\"layout_type\":\"blog\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"3\",\"num_columns\":\"3\",\"num_links\":\"0\",\"multi_column_order\":\"1\",\"orderby_pri\":\"\",\"orderby_sec\":\"front\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 41, 42, 0, '*', 0),
(102, '0menu-principal-do-blog', 'Blog', 'blog', '', 'blog', 'index.php?option=com_content&view=category&layout=blog&id=8', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"0\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"show_cat_tags\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"4\",\"num_intro_articles\":\"0\",\"num_columns\":\"4\",\"num_links\":\"2\",\"multi_column_order\":\"1\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_featured\":\"\",\"article_layout\":\"_:default\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"0\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"0\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"helixultimatemenulayout\":\"{\\\"width\\\":600,\\\"menualign\\\":\\\"right\\\",\\\"megamenu\\\":false,\\\"showtitle\\\":\\\"\\\",\\\"faicon\\\":\\\"\\\",\\\"customclass\\\":\\\"\\\",\\\"dropdown\\\":\\\"right\\\",\\\"badge\\\":\\\"\\\",\\\"badge_position\\\":\\\"\\\",\\\"badge_bg_color\\\":\\\"\\\",\\\"badge_text_color\\\":\\\"\\\",\\\"layout\\\":[]}\",\"helixultimate_enable_page_title\":\"0\",\"helixultimate_page_title_alt\":\"\",\"helixultimate_page_subtitle\":\"\",\"helixultimate_page_title_heading\":\"h2\",\"helixultimate_page_title_bg_color\":\"\",\"helixultimate_page_title_bg_image\":\"\"}', 43, 44, 1, '*', 0),
(103, '0menu-principal-do-blog', 'Sobre', 'sobre', '', 'sobre', 'index.php?option=com_content&view=article&id=1', 'component', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{\"info_block_position\":0,\"show_category\":0,\"link_category\":0,\"show_author\":0,\"show_create_date\":0,\"show_publish_date\":0,\"show_hits\":0,\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 45, 46, 0, '*', 0),
(104, '0menu-principal-do-blog', 'Acesso de Autor', 'acesso-de-autor', '', 'acesso-de-autor', 'index.php?option=com_users&view=login', 'component', 0, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{\"logindescription_show\":1,\"logoutdescription_show\":1,\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 47, 48, 0, '*', 0),
(105, '1menu-do-autor', 'Criar um Artigo', 'criar-um-artigo', '', 'criar-um-artigo', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 3, ' ', 9, '{\"enable_category\":1,\"catid\":\"8\",\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 49, 50, 0, '*', 0),
(106, '1menu-do-autor', 'Trabalhando em seu Site', 'trabalhando-em-seu-site', '', 'trabalhando-em-seu-site', 'index.php?option=com_content&view=article&id=2', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 51, 56, 0, '*', 0),
(107, '1menu-do-autor', 'Administrador do Site', 'administrador-do-site', '', 'administrador-do-site', 'administrator', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 1, 3, ' ', 0, '{\"menu_text\":1}', 57, 58, 0, '*', 0),
(108, '1menu-do-autor', 'Alterar Senha', 'alterar-senha', '', 'alterar-senha', 'index.php?option=com_users&view=profile&layout=edit', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 2, ' ', 9, '{\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 59, 60, 0, '*', 0),
(109, '1menu-do-autor', 'Sair', 'sair', '', 'sair', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{\"logindescription_show\":1,\"logoutdescription_show\":1,\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 61, 62, 0, '*', 0),
(110, '2menu-inferior', 'Acesso de Autor', 'acesso-de-autor-1', '', 'acesso-de-autor-1', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 9, '{\"login_redirect_url\":\"index.php?Itemid=102\",\"logindescription_show\":1,\"logoutdescription_show\":1,\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 63, 64, 0, '*', 0),
(111, '1menu-do-autor', 'Configurações do Site', 'configuracoes-do-site', '', 'trabalhando-em-seu-site/configuracoes-do-site', 'index.php?option=com_config&view=config&controller=config.display.config', 'component', 1, 106, 2, 23, 0, '0000-00-00 00:00:00', 0, 6, ' ', 0, '{\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 52, 53, 0, '*', 0),
(112, '1menu-do-autor', 'Configurações de Temas', 'configuracoes-de-temas', '', 'trabalhando-em-seu-site/configuracoes-de-temas', 'index.php?option=com_config&view=templates&controller=config.display.templates', 'component', 1, 106, 2, 23, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu_text\":1,\"show_page_heading\":0,\"secure\":0}', 54, 55, 0, '*', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_menu_types`
--

CREATE TABLE `blog_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_menu_types`
--

INSERT INTO `blog_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0),
(2, 69, '0menu-principal-do-blog', 'Menu Principal do Blog', 'O menu principal do site', 0),
(3, 70, '1menu-do-autor', 'Menu do Autor', '23', 0),
(4, 71, '2menu-inferior', 'Menu Inferior', '23', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_messages`
--

CREATE TABLE `blog_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_messages_cfg`
--

CREATE TABLE `blog_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_modules`
--

CREATE TABLE `blog_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_modules`
--

INSERT INTO `blog_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"mainmenu\",\"startLevel\":\"0\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"\",\"moduleclass_sfx\":\"_menu\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_login', 1, 1, '{\"greeting\":\"1\",\"name\":\"0\"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{\"moduleclass_sfx\":\"\",\"showHome\":\"1\",\"homeText\":\"\",\"showComponent\":\"1\",\"separator\":\"\",\"cache\":\"0\",\"cache_time\":\"0\",\"cachemode\":\"itemid\"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(87, 55, 'Sample Data', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sampledata', 6, 1, '{}', 1, '*'),
(88, 58, 'Latest Actions', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latestactions', 6, 1, '{}', 1, '*'),
(89, 59, 'Privacy Dashboard', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_privacy_dashboard', 6, 1, '{}', 1, '*'),
(90, 72, 'Menu Principal do Blog', '', '', 1, 'position-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 0, '{\"menutype\":\"0menu-principal-do-blog\",\"startLevel\":1,\"endLevel\":0,\"showAllChildren\":0,\"class_sfx\":\" nav-pills\",\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(91, 73, 'Menu do Autor', '', '', 1, 'position-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 0, '{\"menutype\":\"1menu-do-autor\",\"startLevel\":1,\"endLevel\":0,\"showAllChildren\":1,\"class_sfx\":\" nav-pills\",\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(92, 74, 'Distribuição', '', '', 6, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_syndicate', 1, 0, '{\"display_text\":1,\"text\":\"My Blog\",\"format\":\"rss\",\"layout\":\"_:default\",\"cache\":0}', 0, '*'),
(93, 75, 'Artigos Arquivados', '', '', 4, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_archive', 1, 1, '{\"count\":10,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"static\"}', 0, '*'),
(94, 76, 'Artigos mais lidos', '', '', 5, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_popular', 1, 1, '{\"catid\":\"8\",\"count\":5,\"show_front\":1,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"static\"}', 0, '*'),
(95, 77, 'Artigos mais antigos', '', '', 2, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_category', 1, 1, '{\"mode\":\"normal\",\"show_on_article_page\":0,\"show_front\":\"show\",\"count\":6,\"category_filtering_type\":1,\"catid\":\"8\",\"show_child_category_articles\":0,\"levels\":1,\"author_filtering_type\":1,\"author_alias_filtering_type\":1,\"date_filtering\":\"off\",\"date_field\":\"a.created\",\"relative_date\":30,\"article_ordering\":\"a.created\",\"article_ordering_direction\":\"DESC\",\"article_grouping\":\"none\",\"article_grouping_direction\":\"krsort\",\"month_year_format\":\"F Y\",\"item_heading\":5,\"link_titles\":1,\"show_date\":0,\"show_date_field\":\"created\",\"show_date_format\":\"d-m-Y H:i\",\"show_category\":0,\"show_hits\":0,\"show_author\":0,\"show_introtext\":0,\"introtext_limit\":100,\"show_readmore\":0,\"show_readmore_title\":1,\"readmore_limit\":15,\"layout\":\"_:default\",\"owncache\":1,\"cache_time\":900,\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(96, 78, 'Menu Inferior', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 0, '{\"menutype\":\"2menu-inferior\",\"startLevel\":1,\"endLevel\":0,\"showAllChildren\":0,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(97, 79, 'Pesquisa', '', '', 1, 'position-0', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{\"width\":20,\"button_pos\":\"right\",\"opensearch\":1,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"itemid\"}', 0, '*'),
(98, 80, 'Imagem', '', '<p><img src=\"images/headers/raindrops.jpg\" alt=\"\" /></p>', 1, 'position-3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":1,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(99, 81, 'Marcadores Populares', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_tags_popular', 1, 1, '{\"maximum\":8,\"timeframe\":\"alltime\",\"order_value\":\"count\",\"order_direction\":1,\"display_count\":0,\"no_results_text\":0,\"minsize\":1,\"maxsize\":2,\"layout\":\"_:default\",\"owncache\":1,\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 0, '*'),
(100, 82, 'Itens Semelhantes', '', '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_tags_similar', 1, 1, '{\"maximum\":5,\"matchtype\":\"any\",\"ordering\":\"count\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"owncache\":1,\"cache_time\":900,\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(101, 83, 'Informações do Site', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_stats_admin', 6, 1, '{\"serverinfo\":1,\"siteinfo\":1,\"counter\":0,\"increase\":0,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":6,\"header_tag\":\"h3\",\"style\":0}', 1, '*'),
(102, 84, 'Notícias de Lançamento', '', '', 1, 'postinstall', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_feed', 1, 1, '{\"rssurl\":\"https:\\/\\/www.joomla.org\\/announcements\\/release-news.feed\",\"rssrtl\":0,\"rsstitle\":1,\"rssdesc\":1,\"rssimage\":1,\"rssitems\":3,\"rssitemdesc\":1,\"word_count\":0,\"layout\":\"_:default\",\"cache\":1,\"cache_time\":900,\"module_tag\":\"div\",\"bootstrap_size\":0,\"header_tag\":\"h3\",\"style\":0}', 1, '*');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_modules_menu`
--

CREATE TABLE `blog_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT 0,
  `menuid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_modules_menu`
--

INSERT INTO `blog_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(101, 0),
(102, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_newsfeeds`
--

CREATE TABLE `blog_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT 0,
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT 3600,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `rtl` tinyint(4) NOT NULL DEFAULT 0,
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_overrider`
--

CREATE TABLE `blog_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_postinstall_messages`
--

CREATE TABLE `blog_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT 700 COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT 1,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_postinstall_messages`
--

INSERT INTO `blog_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(5, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(6, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1),
(7, 700, 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_TITLE', 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_BODY', 'PLG_PLG_RECAPTCHA_VERSION_1_POSTINSTALL_ACTION', 'plg_captcha_recaptcha', 1, 'action', 'site://plugins/captcha/recaptcha/postinstall/actions.php', 'recaptcha_postinstall_action', 'site://plugins/captcha/recaptcha/postinstall/actions.php', 'recaptcha_postinstall_condition', '3.8.6', 1),
(8, 700, 'COM_ACTIONLOGS_POSTINSTALL_TITLE', 'COM_ACTIONLOGS_POSTINSTALL_BODY', '', 'com_actionlogs', 1, 'message', '', '', '', '', '3.9.0', 1),
(9, 700, 'COM_PRIVACY_POSTINSTALL_TITLE', 'COM_PRIVACY_POSTINSTALL_BODY', '', 'com_privacy', 1, 'message', '', '', '', '', '3.9.0', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_privacy_consents`
--

CREATE TABLE `blog_privacy_consents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `state` int(10) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remind` tinyint(4) NOT NULL DEFAULT 0,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_privacy_requests`
--

CREATE TABLE `blog_privacy_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `requested_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `request_type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_token_created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_redirect_links`
--

CREATE TABLE `blog_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT 301
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_schemas`
--

CREATE TABLE `blog_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_schemas`
--

INSERT INTO `blog_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.9.10-2019-07-09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_session`
--

CREATE TABLE `blog_session` (
  `session_id` varbinary(192) NOT NULL,
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(3) UNSIGNED DEFAULT 1,
  `time` int(11) NOT NULL DEFAULT 0,
  `data` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT 0,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_session`
--

INSERT INTO `blog_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
(0x6a7261393235707538673738383932656e63743737336a393862, 0, 1, 1567783752, 'joomla|s:668:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjozOntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjI6e3M6NzoiY291bnRlciI7aToyODtzOjU6InRpbWVyIjtPOjg6InN0ZENsYXNzIjozOntzOjU6InN0YXJ0IjtpOjE1Njc3ODI0ODE7czo0OiJsYXN0IjtpOjE1Njc3ODM3NDY7czozOiJub3ciO2k6MTU2Nzc4Mzc1MTt9fXM6ODoicmVnaXN0cnkiO086MjQ6Ikpvb21sYVxSZWdpc3RyeVxSZWdpc3RyeSI6Mzp7czo3OiIAKgBkYXRhIjtPOjg6InN0ZENsYXNzIjowOnt9czoxNDoiACoAaW5pdGlhbGl6ZWQiO2I6MDtzOjk6InNlcGFyYXRvciI7czoxOiIuIjt9czo0OiJ1c2VyIjtPOjIwOiJKb29tbGFcQ01TXFVzZXJcVXNlciI6MTp7czoyOiJpZCI7aTowO319fXM6MTQ6IgAqAGluaXRpYWxpemVkIjtiOjA7czo5OiJzZXBhcmF0b3IiO3M6MToiLiI7fQ==\";', 0, ''),
(0x7630646675376b336c626f6a6b7571326e6e6130656468303539, 1, 0, 1567783669, 'joomla|s:2312:\"TzoyNDoiSm9vbWxhXFJlZ2lzdHJ5XFJlZ2lzdHJ5IjozOntzOjc6IgAqAGRhdGEiO086ODoic3RkQ2xhc3MiOjE6e3M6OToiX19kZWZhdWx0IjtPOjg6InN0ZENsYXNzIjo0OntzOjc6InNlc3Npb24iO086ODoic3RkQ2xhc3MiOjM6e3M6NzoiY291bnRlciI7aTo1NztzOjU6InRva2VuIjtzOjMyOiJVSzJzTVdBQVhBOEJBbXhyUUVDREppc1N2Z2xOYTB6USI7czo1OiJ0aW1lciI7Tzo4OiJzdGRDbGFzcyI6Mzp7czo1OiJzdGFydCI7aToxNTY3NzgyNTM0O3M6NDoibGFzdCI7aToxNTY3NzgzNjY5O3M6Mzoibm93IjtpOjE1Njc3ODM2Njk7fX1zOjg6InJlZ2lzdHJ5IjtPOjI0OiJKb29tbGFcUmVnaXN0cnlcUmVnaXN0cnkiOjM6e3M6NzoiACoAZGF0YSI7Tzo4OiJzdGRDbGFzcyI6Njp7czoxMzoiY29tX2luc3RhbGxlciI7Tzo4OiJzdGRDbGFzcyI6Mjp7czo3OiJtZXNzYWdlIjtzOjA6IiI7czoxNzoiZXh0ZW5zaW9uX21lc3NhZ2UiO3M6MDoiIjt9czoxMToiY29tX3BsdWdpbnMiO086ODoic3RkQ2xhc3MiOjE6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo2OiJwbHVnaW4iO086ODoic3RkQ2xhc3MiOjI6e3M6MjoiaWQiO2E6MDp7fXM6NDoiZGF0YSI7Tjt9fX1zOjk6ImNvbV9hZG1pbiI7Tzo4OiJzdGRDbGFzcyI6MTp7czo0OiJlZGl0IjtPOjg6InN0ZENsYXNzIjoxOntzOjc6InByb2ZpbGUiO086ODoic3RkQ2xhc3MiOjI6e3M6MjoiaWQiO2E6MDp7fXM6NDoiZGF0YSI7Tjt9fX1zOjEwOiJjb21fZmllbGRzIjtPOjg6InN0ZENsYXNzIjoyOntzOjY6ImZpZWxkcyI7Tzo4OiJzdGRDbGFzcyI6MTp7czo3OiJjb250ZXh0IjtzOjE0OiJjb21fdXNlcnMudXNlciI7fXM6NjoiZ3JvdXBzIjtPOjg6InN0ZENsYXNzIjoyOntzOjc6ImNvbnRleHQiO3M6MTQ6ImNvbV91c2Vycy51c2VyIjtzOjQ6Imxpc3QiO2E6NDp7czo5OiJkaXJlY3Rpb24iO3M6MzoiYXNjIjtzOjU6ImxpbWl0IjtzOjI6IjIwIjtzOjg6Im9yZGVyaW5nIjtzOjEwOiJhLm9yZGVyaW5nIjtzOjU6InN0YXJ0IjtkOjA7fX19czo5OiJjb21fdXNlcnMiO086ODoic3RkQ2xhc3MiOjE6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo1OiJncm91cCI7Tzo4OiJzdGRDbGFzcyI6Mjp7czoyOiJpZCI7YTowOnt9czo0OiJkYXRhIjtOO319fXM6MTM6ImNvbV90ZW1wbGF0ZXMiO086ODoic3RkQ2xhc3MiOjI6e3M6NDoiZWRpdCI7Tzo4OiJzdGRDbGFzcyI6MTp7czo1OiJzdHlsZSI7Tzo4OiJzdGRDbGFzcyI6Mjp7czoyOiJpZCI7YToxOntpOjA7aTo0O31zOjQ6ImRhdGEiO047fX1zOjY6InN0eWxlcyI7Tzo4OiJzdGRDbGFzcyI6NDp7czo2OiJmaWx0ZXIiO2E6Mzp7czo2OiJzZWFyY2giO3M6MDoiIjtzOjg6Im1lbnVpdGVtIjtzOjA6IiI7czo4OiJ0ZW1wbGF0ZSI7czowOiIiO31zOjk6ImNsaWVudF9pZCI7czoxOiIwIjtzOjQ6Imxpc3QiO2E6Mjp7czoxMjoiZnVsbG9yZGVyaW5nIjtzOjA6IiI7czo1OiJsaW1pdCI7czoyOiIyMCI7fXM6MTA6ImxpbWl0c3RhcnQiO2k6MDt9fX1zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO31zOjQ6InVzZXIiO086MjA6Ikpvb21sYVxDTVNcVXNlclxVc2VyIjoxOntzOjI6ImlkIjtzOjM6IjkzMiI7fXM6MTE6ImFwcGxpY2F0aW9uIjtPOjg6InN0ZENsYXNzIjoxOntzOjU6InF1ZXVlIjthOjA6e319fX1zOjE0OiIAKgBpbml0aWFsaXplZCI7YjowO3M6OToic2VwYXJhdG9yIjtzOjE6Ii4iO30=\";', 932, 'Opineasy');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lft` int(11) NOT NULL DEFAULT 0,
  `rgt` int(11) NOT NULL DEFAULT 0,
  `level` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 932, '2019-09-05 15:27:58', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_template_styles`
--

CREATE TABLE `blog_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_template_styles`
--

INSERT INTO `blog_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '1', 'Beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.png\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(9, 'ltsocialcompany', 0, '0', 'ltsocialcompany - Padrão', '{\"logo_type\":\"image\",\"logo_image\":\"images\\/logo-min.png\",\"logo_height\":\"36\",\"mobile_logo\":\"images\\/logo-min.png\",\"logo_text\":\"LT SOCIAL COMPANY\",\"logo_slogan\":\"\",\"header_height\":\"73px\",\"sticky_header\":\"1\",\"favicon\":\"\",\"preloader\":\"1\",\"boxed_layout\":\"1\",\"body_bg_image\":\"\",\"body_bg_repeat\":\"inherit\",\"body_bg_size\":\"inherit\",\"body_bg_attachment\":\"inherit\",\"body_bg_position\":\"0 0\",\"enabled_copyright\":\"1\",\"copyright_position\":\"footer1\",\"copyright_load_pos\":\"default\",\"copyright\":\"\\u00a9 Your Company. Designed By ltheme\",\"show_social_icons\":\"1\",\"social_position\":\"top1\",\"social_load_pos\":\"default\",\"facebook\":\"#\",\"twitter\":\"#\",\"googleplus\":\"#\",\"pinterest\":\"#\",\"linkedin\":\"#\",\"dribbble\":\"#\",\"instagram\":\"#\",\"behance\":\"#\",\"youtube\":\"#\",\"flickr\":\"#\",\"skype\":\"pq.softs\",\"whatsapp\":\"#\",\"vk\":\"#\",\"custom\":\"\",\"contactinfo\":\"1\",\"contact_position\":\"top2\",\"contact_phone\":\"+228 872 4444\",\"contact_mobile\":\"+775 872 4444\",\"contact_email\":\"contact@email.com\",\"contact_time\":\"\",\"comingsoon_title\":\"Coming Soon Title\",\"comingsoon_content\":\"Coming soon content\",\"comingsoon_date\":\"11-11-2018\",\"comingsoon_logo\":\"\",\"comingsoon_bg_image\":\"\",\"comingsoon_social_icons\":\"1\",\"error_logo\":\"\",\"error_bg\":\"\",\"preset\":\"{\\\"footer_link_hover_color\\\":\\\"#FFFFFF\\\",\\\"footer_link_color\\\":\\\"#A2A2A2\\\",\\\"footer_text_color\\\":\\\"#FFFFFF\\\",\\\"footer_bg_color\\\":\\\"#171717\\\",\\\"menu_dropdown_text_active_color\\\":\\\"#0fa89d\\\",\\\"menu_dropdown_text_hover_color\\\":\\\"#0fa89d\\\",\\\"menu_dropdown_text_color\\\":\\\"#252525\\\",\\\"menu_dropdown_bg_color\\\":\\\"#FFFFFF\\\",\\\"menu_text_active_color\\\":\\\"#0fa89d\\\",\\\"menu_text_hover_color\\\":\\\"#0fa89d\\\",\\\"menu_text_color\\\":\\\"#252525\\\",\\\"logo_text_color\\\":\\\"#0fa89d\\\",\\\"topbar_text_color\\\":\\\"#AAAAAA\\\",\\\"topbar_bg_color\\\":\\\"#333333\\\",\\\"header_bg_color\\\":\\\"#FFFFFF\\\",\\\"link_hover_color\\\":\\\"#044CD0\\\",\\\"link_color\\\":\\\"#0fa89d\\\",\\\"bg_color\\\":\\\"#FFFFFF\\\",\\\"text_color\\\":\\\"#252525\\\",\\\"preset\\\":\\\"preset3\\\"}\",\"text_color\":\"#00c896\",\"bg_color\":\"#ffffff\",\"link_color\":\"#00c896\",\"link_hover_color\":\"#00c896\",\"header_bg_color\":\"#ffffff\",\"logo_text_color\":\"#0345bf\",\"topbar_bg_color\":\"#333333\",\"topbar_text_color\":\"#aaaaaa\",\"menu_text_color\":\"#ffffff\",\"menu_text_hover_color\":\"#00c896\",\"menu_text_active_color\":\"#00c896\",\"menu_dropdown_bg_color\":\"#ffffff\",\"menu_dropdown_text_color\":\"#000000\",\"menu_dropdown_text_hover_color\":\"#00c896\",\"menu_dropdown_text_active_color\":\"#00c896\",\"footer_bg_color\":\"#00c896\",\"footer_text_color\":\"#ffffff\",\"footer_link_color\":\"#dc612d\",\"footer_link_hover_color\":\"#ffffff\",\"predefined_header\":\"1\",\"header_style\":\"style-2\",\"layout\":\"[{\\\"type\\\":\\\"row\\\",\\\"layout\\\":12,\\\"settings\\\":{\\\"hide_on_desktop\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_phone\\\":0,\\\"background_image\\\":\\\"\\\",\\\"background_color\\\":\\\"\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"padding\\\":\\\"\\\",\\\"custom_class\\\":\\\"\\\",\\\"fluidrow\\\":1,\\\"name\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"hide_mobile\\\":1,\\\"hide_large_mobile\\\":1,\\\"hide_tablet\\\":0,\\\"hide_small_desktop\\\":0,\\\"hide_desktop\\\":0},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"title\\\",\\\"grid_size\\\":12}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"4+4+4\\\",\\\"settings\\\":{\\\"name\\\":\\\"Main Body\\\",\\\"background_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"padding\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"fluidrow\\\":0,\\\"custom_class\\\":\\\"\\\"},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"left\\\",\\\"grid_size\\\":4}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"hide_on_desktop\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_phone\\\":0,\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"md_col\\\":\\\"\\\",\\\"xl_col\\\":\\\"\\\",\\\"custom_class\\\":\\\"\\\",\\\"name\\\":\\\"position-0\\\",\\\"column_type\\\":1,\\\"grid_size\\\":4,\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"right\\\",\\\"column_type\\\":0,\\\"grid_size\\\":4}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":12,\\\"settings\\\":{},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"name\\\":\\\"none\\\",\\\"column_type\\\":0,\\\"grid_size\\\":12}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"3+3+3+3\\\",\\\"settings\\\":{\\\"hide_on_desktop\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_phone\\\":0,\\\"background_position\\\":\\\"\\\",\\\"background_attachment\\\":\\\"\\\",\\\"background_size\\\":\\\"\\\",\\\"background_repeat\\\":\\\"\\\",\\\"background_image\\\":\\\"images\\/demo\\/Testimonials\\/tes2.jpg\\\",\\\"background_color\\\":\\\"#1b1d1f\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"padding\\\":\\\"\\\",\\\"custom_class\\\":\\\"\\\",\\\"fluidrow\\\":0,\\\"name\\\":\\\"Bottom\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"grid_size\\\":3,\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"col-sm-6\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"bottom1\\\",\\\"column_type\\\":0,\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"grid_size\\\":3,\\\"column_type\\\":0,\\\"name\\\":\\\"bottom2\\\",\\\"hidden_xs\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_md\\\":0,\\\"sm_col\\\":\\\"col-sm-6\\\",\\\"xs_col\\\":\\\"\\\",\\\"custom_class\\\":\\\"\\\",\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"name\\\":\\\"bottom3\\\",\\\"custom_class\\\":\\\"\\\",\\\"xl_col\\\":\\\"\\\",\\\"md_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"hide_on_phone\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_desktop\\\":0,\\\"grid_size\\\":3}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"column_type\\\":0,\\\"name\\\":\\\"bottom4\\\",\\\"custom_class\\\":\\\"\\\",\\\"xl_col\\\":\\\"\\\",\\\"md_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"hide_on_phone\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_desktop\\\":0,\\\"grid_size\\\":3}}]},{\\\"type\\\":\\\"row\\\",\\\"layout\\\":\\\"6+6\\\",\\\"settings\\\":{\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"Footer\\\",\\\"fluidrow\\\":0,\\\"custom_class\\\":\\\"\\\",\\\"padding\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"color\\\":\\\"\\\",\\\"link_color\\\":\\\"\\\",\\\"link_hover_color\\\":\\\"\\\",\\\"background_color\\\":\\\"#1b1d1f\\\",\\\"background_image\\\":\\\"\\\",\\\"background_repeat\\\":\\\"\\\",\\\"background_size\\\":\\\"\\\",\\\"background_attachment\\\":\\\"\\\",\\\"background_position\\\":\\\"\\\",\\\"hide_on_phone\\\":0,\\\"hide_on_large_phone\\\":0,\\\"hide_on_tablet\\\":0,\\\"hide_on_small_desktop\\\":0,\\\"hide_on_desktop\\\":0},\\\"attr\\\":[{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"footer1\\\",\\\"column_type\\\":0,\\\"grid_size\\\":6,\\\"sortableitem\\\":\\\"[object Object]\\\"}},{\\\"type\\\":\\\"sp_col\\\",\\\"settings\\\":{\\\"custom_class\\\":\\\"\\\",\\\"xs_col\\\":\\\"\\\",\\\"sm_col\\\":\\\"\\\",\\\"hidden_md\\\":0,\\\"hidden_sm\\\":0,\\\"hidden_xs\\\":0,\\\"name\\\":\\\"footer2\\\",\\\"column_type\\\":0,\\\"grid_size\\\":6}}]}]\",\"menu\":\"0menu-principal-do-blog\",\"menu_type\":\"mega_offcanvas\",\"dropdown_width\":\"\",\"menu_animation\":\"menu-animation-fade-up\",\"offcanvas_position\":\"right\",\"enable_body_font\":\"1\",\"body_font\":\"{\\\"fontFamily\\\":\\\"Hind\\\",\\\"fontSize\\\":\\\"16\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h1_font\":\"1\",\"h1_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h2_font\":\"1\",\"h2_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h3_font\":\"1\",\"h3_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h4_font\":\"1\",\"h4_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h5_font\":\"1\",\"h5_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_h6_font\":\"1\",\"h6_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"enable_navigation_font\":\"1\",\"navigation_font\":\"{\\\"fontFamily\\\":\\\"Poppins\\\",\\\"fontSize\\\":\\\"12\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"600\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"latin\\\"}\",\"custom_font\":\"{\\\"fontFamily\\\":\\\"Arial\\\",\\\"fontSize\\\":\\\"\\\",\\\"fontSize_sm\\\":\\\"\\\",\\\"fontSize_xs\\\":\\\"\\\",\\\"fontWeight\\\":\\\"\\\",\\\"fontStyle\\\":\\\"\\\",\\\"fontSubset\\\":\\\"\\\"}\",\"custom_font_selectors\":\"\",\"image_small\":\"1\",\"image_small_size\":\"100X100\",\"image_thumbnail\":\"1\",\"image_thumbnail_size\":\"600X340\",\"image_medium\":\"1\",\"image_medium_size\":\"300X300\",\"image_large\":\"1\",\"image_large_size\":\"600X600\",\"image_crop_quality\":\"100\",\"blog_list_image\":\"thumbnail\",\"leading_blog_list_image\":\"large\",\"blog_details_image\":\"default\",\"social_share\":\"1\",\"author_info\":\"1\",\"comment\":\"disabled\",\"comment_disqus_subdomain\":\"\",\"comment_disqus_devmode\":\"1\",\"comment_intensedebate_acc\":\"\",\"comment_facebook_app_id\":\"\",\"comment_facebook_width\":\"100\",\"comment_facebook_number\":\"10\",\"comments_count\":\"1\",\"before_head\":\"\",\"before_body\":\"\",\"custom_css\":\"\",\"custom_js\":\"\",\"exclude_js\":\"\",\"scssoption\":\"1\"}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_ucm_base`
--

CREATE TABLE `blog_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_ucm_content`
--

CREATE TABLE `blog_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_state` tinyint(1) NOT NULL DEFAULT 0,
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT 0,
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `core_ordering` int(11) NOT NULL DEFAULT 0,
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_ucm_history`
--

CREATE TABLE `blog_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_ucm_history`
--

INSERT INTO `blog_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 8, 5, '', '2019-09-05 15:45:43', 932, 457, 'e9a38dc04ddba681619abad7d94da1a25652bb40', '{\"id\":8,\"asset_id\":61,\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":12,\"level\":1,\"path\":null,\"extension\":\"com_content\",\"title\":\"Blog\",\"alias\":\"blog\",\"note\":null,\"description\":\"\",\"published\":1,\"checked_out\":null,\"checked_out_time\":null,\"access\":1,\"params\":\"\",\"metadesc\":null,\"metakey\":null,\"metadata\":null,\"created_user_id\":\"932\",\"created_time\":\"2019-09-05 15:45:43\",\"modified_user_id\":null,\"modified_time\":\"2019-09-05 15:45:43\",\"hits\":null,\"language\":\"*\",\"version\":null}', 0),
(2, 9, 5, '', '2019-09-05 15:45:43', 932, 466, 'c21ad6fc2385da420f05c9b86c53d02aa6ac8ead', '{\"id\":9,\"asset_id\":62,\"parent_id\":\"1\",\"lft\":\"13\",\"rgt\":14,\"level\":1,\"path\":\"blog\",\"extension\":\"com_content\",\"title\":\"Ajuda\",\"alias\":\"ajuda\",\"note\":\"\",\"description\":\"\",\"published\":1,\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"access\":1,\"params\":\"\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"\",\"created_user_id\":\"932\",\"created_time\":\"2019-09-05 15:45:43\",\"modified_user_id\":\"0\",\"modified_time\":\"2019-09-05 15:45:43\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(3, 1, 1, '', '2019-09-05 15:45:43', 932, 730, '1206bcd0b807dd443383a7aeb13d04af7a8cd0e4', '{\"id\":1,\"asset_id\":63,\"title\":\"Sobre\",\"alias\":\"sobre\",\"introtext\":\"<p>Aqui se fala um pouco sobre este blog e a pessoa que o escreve.<\\/p> <p>Quando estiver logado, ser\\u00e1 poss\\u00edvel editar esta p\\u00e1gina, clicando no \\u00edcone de edi\\u00e7\\u00e3o.<\\/p>\",\"fulltext\":\"23\",\"state\":1,\"catid\":\"9\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":null,\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":1,\"ordering\":2,\"metakey\":\"\",\"metadesc\":\"\",\"access\":1,\"hits\":null,\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":null}', 0),
(4, 2, 1, '', '2019-09-05 15:45:43', 932, 2825, '4e3cd6118352ac7670f0a1f7b20796bf32312502', '{\"id\":2,\"asset_id\":64,\"title\":\"Trabalhando em seu Site\",\"alias\":\"trabalhando-em-seu-site\",\"introtext\":\"<p>Algumas dicas b\\u00e1sicas para trabalhar no seu site.<\\/p><ul><li>Joomla! tem um \'front-end\', que \\u00e9 o que se v\\u00ea agora, e um \'Administrador\' ou \'back-end\' que \\u00e9 onde se faz o trabalho mais avan\\u00e7ado de cria\\u00e7\\u00e3o do site, como configurar menus e decidir quais m\\u00f3dulos mostrar. Voc\\u00ea precisa acessar o administrador separadamente usando o mesmo nome de usu\\u00e1rio e senha que voc\\u00ea usou para acessar para esta \\u00e1rea do site.<\\/li> <li>Uma das primeiras coisas que voc\\u00ea provavelmente vai querer fazer \\u00e9 alterar o t\\u00edtulo do site, a descri\\u00e7\\u00e3o e adicionar uma logomarca. Para fazer isso clique em Configura\\u00e7\\u00f5es de Tema (Template Settings) no Menu do Usu\\u00e1rio (User Menu). <\\/li><li>Para alterar a descri\\u00e7\\u00e3o do site, t\\u00edtulo do navegador e outros itens, clique em Configura\\u00e7\\u00f5es do Site (Site Settings).<\\/li><li>Op\\u00e7\\u00f5es de configura\\u00e7\\u00e3o mais avan\\u00e7adas est\\u00e3o dispon\\u00edveis no administrador.<\\/li><li>Para mudar completamente a apar\\u00eancia do seu site, voc\\u00ea pode instalar um novo modelo. No menu Extens\\u00f5es clique Gerenciar &gt; Instalar. Existem muitos modelos gr\\u00e1tis ou pagos dispon\\u00edveis para Joomla.<\\/li><li>Como voc\\u00ea j\\u00e1 viu, pode-se controlar quem pode ver as diferentes \\u00e1reas do site. Ao trabalhar com m\\u00f3dulos, artigos ou weblinks e definir o N\\u00edvel de Acesso como Registered permitir\\u00e1 apenas aos usu\\u00e1rios logados v\\u00ea-los<\\/li><li>Quando criar um novo artigo ou outro tipo de conte\\u00fado voc\\u00ea tamb\\u00e9m pode salv\\u00e1-lo como Publicado ou N\\u00e3o Publicado. Os N\\u00e3o Publicados n\\u00e3o poder\\u00e3o ser vistos pelos visitantes, mas voc\\u00ea poder\\u00e1 v\\u00ea-los.<\\/li><li>No administrador h\\u00e1 bot\\u00f5es de ajuda em todas as p\\u00e1ginas que fornecem informa\\u00e7\\u00f5es detalhadas sobre as fun\\u00e7\\u00f5es da p\\u00e1gina.<\\/li><li>Voc\\u00ea aprender\\u00e1 muito mais sobre como trabalhar com o Joomla no <a href=\'https:\\/\\/docs.joomla.org\\/\'> site de documenta\\u00e7\\u00e3o do Joomla<\\/a> e conseguir\\u00e1 ajuda de outros usu\\u00e1rios no <a href=\'https:\\/\\/forum.joomla.org\\/\'> F\\u00f3rum Joomla<\\/a>.<\\/li><\\/ul>\",\"fulltext\":\"23\",\"state\":1,\"catid\":\"9\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":\"0\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":2,\"ordering\":1,\"metakey\":\"\",\"metadesc\":\"\",\"access\":3,\"hits\":\"0\",\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(5, 3, 1, '', '2019-09-05 15:45:43', 932, 1109, '9a20986e494d553cbe00521e2a42562a96c1b238', '{\"id\":3,\"asset_id\":65,\"title\":\"Bem-vindo ao seu blog\",\"alias\":\"bem-vindo-ao-seu-blog\",\"introtext\":\"<p>Este \\u00e9 um exemplo de publica\\u00e7\\u00e3o de blog.<\\/p><p>Se voc\\u00ea acessar o site (o link Login de Autor est\\u00e1 bem no final desta p\\u00e1gina), poder\\u00e1 editar este artigo e todos os outros existentes. Voc\\u00ea tamb\\u00e9m poder\\u00e1 criar um novo artigo e fazer outras altera\\u00e7\\u00f5es no site.<\\/p><p>\\u00c0 medida que adicionar e modificar artigos, ver\\u00e1 como seu site muda e como pode personaliz\\u00e1-lo de v\\u00e1rias maneiras.<\\/p><p>V\\u00e1 em frente, n\\u00e3o h\\u00e1 como estrag\\u00e1-lo.<\\/p>\",\"fulltext\":\"23\",\"state\":1,\"catid\":\"8\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":\"0\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":3,\"ordering\":2,\"metakey\":\"\",\"metadesc\":\"\",\"access\":1,\"hits\":\"0\",\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(6, 4, 1, '', '2019-09-05 15:45:43', 932, 1445, '86af37fa9168bd939ace49161248be5026ab1ec3', '{\"id\":4,\"asset_id\":66,\"title\":\"Sobre sua p\\u00e1gina principal\",\"alias\":\"sobre-sua-pagina-principal\",\"introtext\":\"<p>Sua p\\u00e1gina inicial est\\u00e1 configurada para exibir quatro artigos mais recentes da categoria blog em uma coluna. Depois aparecem links para os dois artigos anteriores. Voc\\u00ea pode mudar a quantidade editando as configura\\u00e7\\u00f5es de op\\u00e7\\u00f5es de conte\\u00fado na aba blog na administra\\u00e7\\u00e3o do seu site. Existe um link para a administra\\u00e7\\u00e3o do seu site no menu superior.<\\/p><p>Se quiser que sua publica\\u00e7\\u00e3o no blog seja dividida em duas partes, introdu\\u00e7\\u00e3o e depois o contet\\u00fado completo separado, use o bot\\u00e3o Leia Mais para inserir uma quebra.<\\/p>\",\"fulltext\":\"<p>Na p\\u00e1gina completa, voc\\u00ea ver\\u00e1 tanto o conte\\u00fado introdut\\u00f3rio como o resto do artigo. Voc\\u00ea pode mudar as configura\\u00e7\\u00f5es para ocultar a introdu\\u00e7\\u00e3o, se quiser.<\\/p><p><\\/p><p><\\/p><p><\\/p>\",\"state\":1,\"catid\":\"8\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":\"0\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":4,\"ordering\":1,\"metakey\":\"\",\"metadesc\":\"\",\"access\":1,\"hits\":\"0\",\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(7, 5, 1, '', '2019-09-05 15:45:43', 932, 2022, '6d013e9dedb52d348aa030063f84b3029d72a7ff', '{\"id\":5,\"asset_id\":67,\"title\":\"Seus M\\u00f3dulos\",\"alias\":\"seus-modulos\",\"introtext\":\"<p>Seu site j\\u00e1 possui alguns m\\u00f3dulos de uso comum preconfigurados. Eles incluem:<\\/p><ul>\\n <li> Imagem, que mostra uma imagem sob o menu. Este \\u00e9 um m\\u00f3dulo personalizado que se pode editar para mudar a imagem.<\\/li>\\n <li> Artigos - Mais Lidas, que lista artigos com base no n\\u00famero de vezes que foram lidos.<\\/li>\\n <li> Artigos - Arquivados, que enumera artigos artigos por m\\u00eas de publica\\u00e7\\u00e3o.<\\/li>\\n <li>Distribui\\u00e7\\u00e3o de Feeds, que cria uma distribui\\u00e7\\u00e3o de feed para a p\\u00e1gina onde \\u00e9 exibido..<\\/li>\\n <li>Marcadores - Populares, que \\u00e9 exibido se a marca\\u00e7\\u00e3o for usada nos artigos. Basta inserir um marcador no campo Marcadores ao editar.<\\/li><\\/ul><p>Cada um desses m\\u00f3dulos tem muitas op\\u00e7\\u00f5es que voc\\u00ea pode experimentar no Gerenciador de M\\u00f3dulos na administra\\u00e7\\u00e3o do seu site. Mover o mouse sobre um m\\u00f3dulo e clicar no \\u00edcone de edi\\u00e7\\u00e3o o levar\\u00e1 para uma tela de edi\\u00e7\\u00e3o deste m\\u00f3dulo. Lembre sempre de salvar e fechar qualquer m\\u00f3dulo que editar.<\\/p>\\n <p>O Joomla! tamb\\u00e9m inclui muitos outros m\\u00f3dulos que voc\\u00ea pode incorporar ao seu site. \\u00c0 medida em que desenvolve seu site, \\u00e9 poss\\u00edvel incluir mais m\\u00f3dulos que podem ser encontrados no <a href=\'https:\\/\\/extensions.joomla.org\\/\'>Diret\\u00f3rio de Extens\\u00f5es Joomla (Joomla Extensions Directory)<\\/a>.<\\/p>\",\"fulltext\":\"23\",\"state\":1,\"catid\":\"8\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":\"0\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":5,\"ordering\":0,\"metakey\":\"\",\"metadesc\":\"\",\"access\":1,\"hits\":\"0\",\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(8, 6, 1, '', '2019-09-05 15:45:43', 932, 1163, '26402989be57f9e6ada72ca63f78746f4763ca54', '{\"id\":6,\"asset_id\":68,\"title\":\"Seu Tema\",\"alias\":\"seu-tema\",\"introtext\":\"<p>Temas controlam a apar\\u00eancia do seu site.<\\/p><p>Este blog est\\u00e1 instalado com o tema Protostar.<\\/p><p>Voc\\u00ea pode editar as op\\u00e7\\u00f5es clicando no link Trabalhando no Seu Site, Configura\\u00e7\\u00f5es de Tema no menu superior (vis\\u00edvel quando voc\\u00ea entrar).<\\/p><p>Por exemplo, voc\\u00ea pode mudar a cor de fundo do site, cor de destaque, t\\u00edtulo, descri\\u00e7\\u00e3o e fonte de caracteres.<\\/p><p>Mais op\\u00e7\\u00f5es est\\u00e3o dispon\\u00edveis na administra\\u00e7\\u00e3o do site. Voc\\u00ea tamb\\u00e9m pode instalar um novo tema usando o gerenciador de extens\\u00f5es.<\\/p>\",\"fulltext\":\"23\",\"state\":1,\"catid\":\"8\",\"created\":\"2019-09-05 15:45:43\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-05 15:45:43\",\"modified_by\":\"0\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"publish_up\":\"2019-09-05 15:45:43\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"\",\"urls\":\"{}\",\"attribs\":\"{}\",\"version\":6,\"ordering\":0,\"metakey\":\"\",\"metadesc\":\"\",\"access\":1,\"hits\":\"0\",\"metadata\":\"{}\",\"featured\":0,\"language\":\"*\",\"xreference\":\"\",\"note\":\"\"}', 0),
(9, 7, 1, '', '2019-09-06 11:36:05', 932, 4736, '82aef765ee47a769cc414048df33eddf81211ef6', '{\"id\":7,\"asset_id\":85,\"title\":\"O que \\u00e9 Lorem Ipsum?\",\"alias\":\"o-que-e-lorem-ipsum\",\"introtext\":\"<div>\\r\\n<p><strong>Lorem Ipsum<\\/strong>\\u00a0\\u00e9 simplesmente uma simula\\u00e7\\u00e3o de texto da ind\\u00fastria tipogr\\u00e1fica e de impressos, e vem sendo utilizado desde o s\\u00e9culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu n\\u00e3o s\\u00f3 a cinco s\\u00e9culos, como tamb\\u00e9m ao salto para a editora\\u00e7\\u00e3o eletr\\u00f4nica, permanecendo essencialmente inalterado. Se popularizou na d\\u00e9cada de 60, quando a Letraset lan\\u00e7ou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editora\\u00e7\\u00e3o eletr\\u00f4nica como Aldus PageMaker.<\\/p>\\r\\n<\\/div>\\r\\n<div>\\r\\n<h2>Porque n\\u00f3s o usamos?<\\/h2>\\r\\n<p>\\u00c9 um fato conhecido de todos que um leitor se distrair\\u00e1 com o conte\\u00fado de texto leg\\u00edvel de uma p\\u00e1gina quando estiver examinando sua diagrama\\u00e7\\u00e3o. A vantagem de usar Lorem Ipsum \\u00e9 que ele tem uma distribui\\u00e7\\u00e3o normal de letras, ao contr\\u00e1rio de \\\"Conte\\u00fado aqui, conte\\u00fado aqui\\\", fazendo com que ele tenha uma apar\\u00eancia similar a de um texto leg\\u00edvel. Muitos softwares de publica\\u00e7\\u00e3o e editores de p\\u00e1ginas na internet agora usam Lorem Ipsum como texto-modelo padr\\u00e3o, e uma r\\u00e1pida busca por \'lorem ipsum\' mostra v\\u00e1rios websites ainda em sua fase de constru\\u00e7\\u00e3o. V\\u00e1rias vers\\u00f5es novas surgiram ao longo dos anos, eventualmente por acidente, e \\u00e0s vezes de prop\\u00f3sito (injetando humor, e coisas do g\\u00eanero).<\\/p>\\r\\n<\\/div>\\r\\n<p>\\u00a0<\\/p>\\r\\n<div>\\r\\n<h2>De onde ele vem?<\\/h2>\\r\\n<p>Ao contr\\u00e1rio do que se acredita, Lorem Ipsum n\\u00e3o \\u00e9 simplesmente um texto rand\\u00f4mico. Com mais de 2000 anos, suas ra\\u00edzes podem ser encontradas em uma obra de literatura latina cl\\u00e1ssica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre cita\\u00e7\\u00f5es da palavra na literatura cl\\u00e1ssica, descobriu a sua indubit\\u00e1vel origem. Lorem Ipsum vem das se\\u00e7\\u00f5es 1.10.32 e 1.10.33 do \\\"de Finibus Bonorum et Malorum\\\" (Os Extremos do Bem e do Mal), de C\\u00edcero, escrito em 45 AC. Este livro \\u00e9 um tratado de teoria da \\u00e9tica muito popular na \\u00e9poca da Renascen\\u00e7a. A primeira linha de Lorem Ipsum, \\\"Lorem Ipsum dolor sit amet...\\\" vem de uma linha na se\\u00e7\\u00e3o 1.10.32.<\\/p>\\r\\n<p>O trecho padr\\u00e3o original de Lorem Ipsum, usado desde o s\\u00e9culo XVI, est\\u00e1 reproduzido abaixo para os interessados. Se\\u00e7\\u00f5es 1.10.32 e 1.10.33 de \\\"de Finibus Bonorum et Malorum\\\" de Cicero tamb\\u00e9m foram reproduzidas abaixo em sua forma exata original, acompanhada das vers\\u00f5es para o ingl\\u00eas da tradu\\u00e7\\u00e3o feita por H. Rackham em 1914.<\\/p>\\r\\n<\\/div>\\r\\n<div>\\r\\n<h2>Onde posso consegu\\u00ed-lo?<\\/h2>\\r\\n<p>Existem muitas varia\\u00e7\\u00f5es dispon\\u00edveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de altera\\u00e7\\u00e3o, seja por inser\\u00e7\\u00e3o de passagens com humor, ou palavras aleat\\u00f3rias que n\\u00e3o parecem nem um pouco convincentes. Se voc\\u00ea pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que n\\u00e3o h\\u00e1 algo embara\\u00e7oso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir peda\\u00e7os predefinidos conforme necess\\u00e1rio, fazendo deste o primeiro gerador de Lorem Ipsum aut\\u00eantico da internet. Ele usa um dicion\\u00e1rio com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com apar\\u00eancia razo\\u00e1vel, livre de repeti\\u00e7\\u00f5es, inser\\u00e7\\u00f5es de humor, palavras n\\u00e3o caracter\\u00edsticas, etc.<\\/p>\\r\\n<\\/div>\",\"fulltext\":\"\",\"state\":\"1\",\"catid\":\"8\",\"created\":\"2019-09-06 11:36:05\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-06 11:36:05\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2019-09-06 11:36:05\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{}\",\"urls\":\"{}\",\"attribs\":\"{\\\"helix_ultimate_image\\\":\\\"\\\",\\\"helix_ultimate_article_format\\\":\\\"standard\\\",\\\"helix_ultimate_audio\\\":\\\"\\\",\\\"helix_ultimate_gallery\\\":\\\"\\\",\\\"helix_ultimate_video\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":null,\"note\":null}', 0),
(10, 8, 1, '', '2019-09-06 11:37:16', 932, 4738, '22b1419a63ea1afec9dadb190be3b909a7722155', '{\"id\":8,\"asset_id\":86,\"title\":\"O que \\u00e9 Lorem Ipsum?\",\"alias\":\"o-que-e-lorem-ipsum-2\",\"introtext\":\"<div>\\r\\n<p><strong>Lorem Ipsum<\\/strong>\\u00a0\\u00e9 simplesmente uma simula\\u00e7\\u00e3o de texto da ind\\u00fastria tipogr\\u00e1fica e de impressos, e vem sendo utilizado desde o s\\u00e9culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu n\\u00e3o s\\u00f3 a cinco s\\u00e9culos, como tamb\\u00e9m ao salto para a editora\\u00e7\\u00e3o eletr\\u00f4nica, permanecendo essencialmente inalterado. Se popularizou na d\\u00e9cada de 60, quando a Letraset lan\\u00e7ou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editora\\u00e7\\u00e3o eletr\\u00f4nica como Aldus PageMaker.<\\/p>\\r\\n<\\/div>\\r\\n<div>\\r\\n<h2>Porque n\\u00f3s o usamos?<\\/h2>\\r\\n<p>\\u00c9 um fato conhecido de todos que um leitor se distrair\\u00e1 com o conte\\u00fado de texto leg\\u00edvel de uma p\\u00e1gina quando estiver examinando sua diagrama\\u00e7\\u00e3o. A vantagem de usar Lorem Ipsum \\u00e9 que ele tem uma distribui\\u00e7\\u00e3o normal de letras, ao contr\\u00e1rio de \\\"Conte\\u00fado aqui, conte\\u00fado aqui\\\", fazendo com que ele tenha uma apar\\u00eancia similar a de um texto leg\\u00edvel. Muitos softwares de publica\\u00e7\\u00e3o e editores de p\\u00e1ginas na internet agora usam Lorem Ipsum como texto-modelo padr\\u00e3o, e uma r\\u00e1pida busca por \'lorem ipsum\' mostra v\\u00e1rios websites ainda em sua fase de constru\\u00e7\\u00e3o. V\\u00e1rias vers\\u00f5es novas surgiram ao longo dos anos, eventualmente por acidente, e \\u00e0s vezes de prop\\u00f3sito (injetando humor, e coisas do g\\u00eanero).<\\/p>\\r\\n<\\/div>\\r\\n<p>\\u00a0<\\/p>\\r\\n<div>\\r\\n<h2>De onde ele vem?<\\/h2>\\r\\n<p>Ao contr\\u00e1rio do que se acredita, Lorem Ipsum n\\u00e3o \\u00e9 simplesmente um texto rand\\u00f4mico. Com mais de 2000 anos, suas ra\\u00edzes podem ser encontradas em uma obra de literatura latina cl\\u00e1ssica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre cita\\u00e7\\u00f5es da palavra na literatura cl\\u00e1ssica, descobriu a sua indubit\\u00e1vel origem. Lorem Ipsum vem das se\\u00e7\\u00f5es 1.10.32 e 1.10.33 do \\\"de Finibus Bonorum et Malorum\\\" (Os Extremos do Bem e do Mal), de C\\u00edcero, escrito em 45 AC. Este livro \\u00e9 um tratado de teoria da \\u00e9tica muito popular na \\u00e9poca da Renascen\\u00e7a. A primeira linha de Lorem Ipsum, \\\"Lorem Ipsum dolor sit amet...\\\" vem de uma linha na se\\u00e7\\u00e3o 1.10.32.<\\/p>\\r\\n<p>O trecho padr\\u00e3o original de Lorem Ipsum, usado desde o s\\u00e9culo XVI, est\\u00e1 reproduzido abaixo para os interessados. Se\\u00e7\\u00f5es 1.10.32 e 1.10.33 de \\\"de Finibus Bonorum et Malorum\\\" de Cicero tamb\\u00e9m foram reproduzidas abaixo em sua forma exata original, acompanhada das vers\\u00f5es para o ingl\\u00eas da tradu\\u00e7\\u00e3o feita por H. Rackham em 1914.<\\/p>\\r\\n<\\/div>\\r\\n<div>\\r\\n<h2>Onde posso consegu\\u00ed-lo?<\\/h2>\\r\\n<p>Existem muitas varia\\u00e7\\u00f5es dispon\\u00edveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de altera\\u00e7\\u00e3o, seja por inser\\u00e7\\u00e3o de passagens com humor, ou palavras aleat\\u00f3rias que n\\u00e3o parecem nem um pouco convincentes. Se voc\\u00ea pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que n\\u00e3o h\\u00e1 algo embara\\u00e7oso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir peda\\u00e7os predefinidos conforme necess\\u00e1rio, fazendo deste o primeiro gerador de Lorem Ipsum aut\\u00eantico da internet. Ele usa um dicion\\u00e1rio com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com apar\\u00eancia razo\\u00e1vel, livre de repeti\\u00e7\\u00f5es, inser\\u00e7\\u00f5es de humor, palavras n\\u00e3o caracter\\u00edsticas, etc.<\\/p>\\r\\n<\\/div>\",\"fulltext\":\"\",\"state\":\"1\",\"catid\":\"8\",\"created\":\"2019-09-06 11:37:16\",\"created_by\":\"932\",\"created_by_alias\":\"\",\"modified\":\"2019-09-06 11:37:16\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2019-09-06 11:37:16\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{}\",\"urls\":\"{}\",\"attribs\":\"{\\\"helix_ultimate_image\\\":\\\"\\\",\\\"helix_ultimate_article_format\\\":\\\"standard\\\",\\\"helix_ultimate_audio\\\":\\\"\\\",\\\"helix_ultimate_gallery\\\":\\\"\\\",\\\"helix_ultimate_video\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":null,\"note\":null}', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_updates`
--

CREATE TABLE `blog_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT 0,
  `extension_id` int(11) DEFAULT 0,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT 0,
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

--
-- Extraindo dados da tabela `blog_updates`
--

INSERT INTO `blog_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 2, 0, 'Armenian', '', 'pkg_hy-AM', 'package', '', 0, '3.4.4.1', '', 'https://update.joomla.org/language/details3/hy-AM_details.xml', '', ''),
(2, 2, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.2', '', 'https://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(3, 2, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.9.7.1', '', 'https://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(4, 2, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.9.10.1', '', 'https://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(5, 2, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.8.0.1', '', 'https://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(6, 2, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(7, 2, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(8, 2, 0, 'Georgian', '', 'pkg_ka-GE', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/ka-GE_details.xml', '', ''),
(9, 2, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(10, 2, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(11, 2, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(12, 2, 0, 'Bengali', '', 'pkg_bn-BD', 'package', '', 0, '3.8.10.1', '', 'https://update.joomla.org/language/details3/bn-BD_details.xml', '', ''),
(13, 2, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.9.5.1', '', 'https://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(14, 2, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.9.6.1', '', 'https://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(15, 2, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.9.10.1', '', 'https://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(16, 2, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(17, 2, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.6.5.2', '', 'https://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(18, 2, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(19, 2, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(20, 2, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(21, 2, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(22, 2, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(23, 2, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(24, 2, 0, 'Esperanto', '', 'pkg_eo-XX', 'package', '', 0, '3.8.11.1', '', 'https://update.joomla.org/language/details3/eo-XX_details.xml', '', ''),
(25, 2, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.8.10.1', '', 'https://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(26, 2, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(27, 2, 0, 'Khmer', '', 'pkg_km-KH', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/km-KH_details.xml', '', ''),
(28, 2, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.8.9.1', '', 'https://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(29, 2, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.7.3.1', '', 'https://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(30, 2, 0, 'Lithuanian', '', 'pkg_lt-LT', 'package', '', 0, '3.9.6.1', '', 'https://update.joomla.org/language/details3/lt-LT_details.xml', '', ''),
(31, 2, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(32, 2, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.8.11.1', '', 'https://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(33, 2, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'https://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(34, 2, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(35, 2, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.9.6.1', '', 'https://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(36, 2, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.9.5.1', '', 'https://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(37, 2, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.9.8.1', '', 'https://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(38, 2, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(39, 2, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(40, 2, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(41, 2, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(42, 2, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.5.1', '', 'https://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(43, 2, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(44, 2, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(45, 2, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.9.4.1', '', 'https://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(46, 2, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.7.1.1', '', 'https://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(47, 2, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(48, 2, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.2', '', 'https://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(49, 2, 0, 'Basque', '', 'pkg_eu-ES', 'package', '', 0, '3.7.5.1', '', 'https://update.joomla.org/language/details3/eu-ES_details.xml', '', ''),
(50, 2, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.2', '', 'https://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(51, 2, 0, 'German DE', '', 'pkg_de-DE', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(53, 2, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(54, 2, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(55, 2, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(56, 2, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(57, 2, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.2', '', 'https://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(58, 2, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(59, 2, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(60, 2, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(61, 2, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(62, 2, 0, 'English CA', '', 'pkg_en-CA', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(63, 2, 0, 'French CA', '', 'pkg_fr-CA', 'package', '', 0, '3.6.5.1', '', 'https://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(64, 2, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(65, 2, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.2', '', 'https://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(66, 2, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.2', '', 'https://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(67, 2, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.5.0.2', '', 'https://update.joomla.org/language/details3/tk-TM_details.xml', '', ''),
(68, 2, 0, 'Irish', '', 'pkg_ga-IE', 'package', '', 0, '3.8.13.1', '', 'https://update.joomla.org/language/details3/ga-IE_details.xml', '', ''),
(69, 2, 0, 'Dzongkha', '', 'pkg_dz-BT', 'package', '', 0, '3.6.2.1', '', 'https://update.joomla.org/language/details3/dz-BT_details.xml', '', ''),
(70, 2, 0, 'Slovenian', '', 'pkg_sl-SI', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/sl-SI_details.xml', '', ''),
(71, 2, 0, 'Spanish CO', '', 'pkg_es-CO', 'package', '', 0, '3.9.10.1', '', 'https://update.joomla.org/language/details3/es-CO_details.xml', '', ''),
(72, 2, 0, 'German CH', '', 'pkg_de-CH', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/de-CH_details.xml', '', ''),
(73, 2, 0, 'German AT', '', 'pkg_de-AT', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/de-AT_details.xml', '', ''),
(74, 2, 0, 'German LI', '', 'pkg_de-LI', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/de-LI_details.xml', '', ''),
(75, 2, 0, 'German LU', '', 'pkg_de-LU', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/de-LU_details.xml', '', ''),
(76, 2, 0, 'English NZ', '', 'pkg_en-NZ', 'package', '', 0, '3.9.11.1', '', 'https://update.joomla.org/language/details3/en-NZ_details.xml', '', ''),
(77, 2, 0, 'Kazakh', '', 'pkg_kk-KZ', 'package', '', 0, '3.9.8.1', '', 'https://update.joomla.org/language/details3/kk-KZ_details.xml', '', ''),
(78, 2, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.9.11.2', '', 'https://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(80, 5, 0, 'shaper_helixultimate', 'Shaper Helixultimate', 'shaper_helixultimate', 'template', '', 0, '1.1.1', '', 'https://www.joomshaper.com/updates/shaper-helixultimate.xml', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_update_sites`
--

CREATE TABLE `blog_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT 0,
  `last_check_timestamp` bigint(20) DEFAULT 0,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Extraindo dados da tabela `blog_update_sites`
--

INSERT INTO `blog_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1567779410, ''),
(2, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 1567769188, ''),
(3, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1567769538, ''),
(4, 'System - Helix Ultimate Framework', 'extension', 'http://www.joomshaper.com/updates/plg-system-helixultimate.xml', 1, 1567769296, ''),
(5, 'shaper_helixultimate', 'extension', 'https://www.joomshaper.com/updates/shaper-helixultimate.xml', 1, 1567769297, ''),
(6, 'WebInstaller Update Site', 'extension', 'https://appscdn.joomla.org/webapps/jedapps/webinstaller.xml', 1, 1567782536, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_update_sites_extensions`
--

CREATE TABLE `blog_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT 0,
  `extension_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Extraindo dados da tabela `blog_update_sites_extensions`
--

INSERT INTO `blog_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 802),
(2, 10002),
(3, 28),
(4, 10004),
(5, 10005),
(6, 10006);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_usergroups`
--

CREATE TABLE `blog_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT 0 COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_usergroups`
--

INSERT INTO `blog_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_users`
--

CREATE TABLE `blog_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT 0,
  `sendEmail` tinyint(4) DEFAULT 0,
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT 0 COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_users`
--

INSERT INTO `blog_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(932, 'Super User', 'Opineasy', 'contato@opineasy.com.br', '$2y$10$AVLfgQivdnl69zAv/sQVNu3HN/h0YwoxAzyKeEJ9Zcd9doU3rZ6ZG', 0, 1, '2019-09-05 15:27:59', '2019-09-06 15:08:54', '0', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_user_keys`
--

CREATE TABLE `blog_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_user_notes`
--

CREATE TABLE `blog_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT 0,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_user_profiles`
--

CREATE TABLE `blog_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_user_usergroup_map`
--

CREATE TABLE `blog_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_user_usergroup_map`
--

INSERT INTO `blog_user_usergroup_map` (`user_id`, `group_id`) VALUES
(932, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_utf8_conversion`
--

CREATE TABLE `blog_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_utf8_conversion`
--

INSERT INTO `blog_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_viewlevels`
--

CREATE TABLE `blog_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `blog_viewlevels`
--

INSERT INTO `blog_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `blog_action_logs`
--
ALTER TABLE `blog_action_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_user_id_logdate` (`user_id`,`log_date`),
  ADD KEY `idx_user_id_extension` (`user_id`,`extension`),
  ADD KEY `idx_extension_item_id` (`extension`,`item_id`);

--
-- Índices para tabela `blog_action_logs_extensions`
--
ALTER TABLE `blog_action_logs_extensions`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `blog_action_logs_users`
--
ALTER TABLE `blog_action_logs_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `idx_notify` (`notify`);

--
-- Índices para tabela `blog_action_log_config`
--
ALTER TABLE `blog_action_log_config`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `blog_assets`
--
ALTER TABLE `blog_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Índices para tabela `blog_associations`
--
ALTER TABLE `blog_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Índices para tabela `blog_banners`
--
ALTER TABLE `blog_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_banner_clients`
--
ALTER TABLE `blog_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Índices para tabela `blog_banner_tracks`
--
ALTER TABLE `blog_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Índices para tabela `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_contact_details`
--
ALTER TABLE `blog_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Índices para tabela `blog_content`
--
ALTER TABLE `blog_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`),
  ADD KEY `idx_alias` (`alias`(191));

--
-- Índices para tabela `blog_contentitem_tag_map`
--
ALTER TABLE `blog_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Índices para tabela `blog_content_frontpage`
--
ALTER TABLE `blog_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Índices para tabela `blog_content_rating`
--
ALTER TABLE `blog_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Índices para tabela `blog_content_types`
--
ALTER TABLE `blog_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Índices para tabela `blog_extensions`
--
ALTER TABLE `blog_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Índices para tabela `blog_fields`
--
ALTER TABLE `blog_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_fields_categories`
--
ALTER TABLE `blog_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Índices para tabela `blog_fields_groups`
--
ALTER TABLE `blog_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_fields_values`
--
ALTER TABLE `blog_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Índices para tabela `blog_finder_filters`
--
ALTER TABLE `blog_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Índices para tabela `blog_finder_links`
--
ALTER TABLE `blog_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Índices para tabela `blog_finder_links_terms0`
--
ALTER TABLE `blog_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms1`
--
ALTER TABLE `blog_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms2`
--
ALTER TABLE `blog_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms3`
--
ALTER TABLE `blog_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms4`
--
ALTER TABLE `blog_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms5`
--
ALTER TABLE `blog_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms6`
--
ALTER TABLE `blog_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms7`
--
ALTER TABLE `blog_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms8`
--
ALTER TABLE `blog_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_terms9`
--
ALTER TABLE `blog_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termsa`
--
ALTER TABLE `blog_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termsb`
--
ALTER TABLE `blog_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termsc`
--
ALTER TABLE `blog_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termsd`
--
ALTER TABLE `blog_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termse`
--
ALTER TABLE `blog_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_links_termsf`
--
ALTER TABLE `blog_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Índices para tabela `blog_finder_taxonomy`
--
ALTER TABLE `blog_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Índices para tabela `blog_finder_taxonomy_map`
--
ALTER TABLE `blog_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Índices para tabela `blog_finder_terms`
--
ALTER TABLE `blog_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Índices para tabela `blog_finder_terms_common`
--
ALTER TABLE `blog_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Índices para tabela `blog_finder_tokens`
--
ALTER TABLE `blog_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Índices para tabela `blog_finder_tokens_aggregate`
--
ALTER TABLE `blog_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Índices para tabela `blog_finder_types`
--
ALTER TABLE `blog_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Índices para tabela `blog_languages`
--
ALTER TABLE `blog_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Índices para tabela `blog_menu`
--
ALTER TABLE `blog_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_menu_types`
--
ALTER TABLE `blog_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Índices para tabela `blog_messages`
--
ALTER TABLE `blog_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Índices para tabela `blog_messages_cfg`
--
ALTER TABLE `blog_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Índices para tabela `blog_modules`
--
ALTER TABLE `blog_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_modules_menu`
--
ALTER TABLE `blog_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Índices para tabela `blog_newsfeeds`
--
ALTER TABLE `blog_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Índices para tabela `blog_overrider`
--
ALTER TABLE `blog_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `blog_postinstall_messages`
--
ALTER TABLE `blog_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Índices para tabela `blog_privacy_consents`
--
ALTER TABLE `blog_privacy_consents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`);

--
-- Índices para tabela `blog_privacy_requests`
--
ALTER TABLE `blog_privacy_requests`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `blog_redirect_links`
--
ALTER TABLE `blog_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Índices para tabela `blog_schemas`
--
ALTER TABLE `blog_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Índices para tabela `blog_session`
--
ALTER TABLE `blog_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`),
  ADD KEY `client_id_guest` (`client_id`,`guest`);

--
-- Índices para tabela `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Índices para tabela `blog_template_styles`
--
ALTER TABLE `blog_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_client_id` (`client_id`),
  ADD KEY `idx_client_id_home` (`client_id`,`home`);

--
-- Índices para tabela `blog_ucm_base`
--
ALTER TABLE `blog_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Índices para tabela `blog_ucm_content`
--
ALTER TABLE `blog_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Índices para tabela `blog_ucm_history`
--
ALTER TABLE `blog_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Índices para tabela `blog_updates`
--
ALTER TABLE `blog_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Índices para tabela `blog_update_sites`
--
ALTER TABLE `blog_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Índices para tabela `blog_update_sites_extensions`
--
ALTER TABLE `blog_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Índices para tabela `blog_usergroups`
--
ALTER TABLE `blog_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Índices para tabela `blog_users`
--
ALTER TABLE `blog_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Índices para tabela `blog_user_keys`
--
ALTER TABLE `blog_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Índices para tabela `blog_user_notes`
--
ALTER TABLE `blog_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Índices para tabela `blog_user_profiles`
--
ALTER TABLE `blog_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Índices para tabela `blog_user_usergroup_map`
--
ALTER TABLE `blog_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Índices para tabela `blog_viewlevels`
--
ALTER TABLE `blog_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `blog_action_logs`
--
ALTER TABLE `blog_action_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de tabela `blog_action_logs_extensions`
--
ALTER TABLE `blog_action_logs_extensions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de tabela `blog_action_log_config`
--
ALTER TABLE `blog_action_log_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `blog_assets`
--
ALTER TABLE `blog_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT de tabela `blog_banners`
--
ALTER TABLE `blog_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_banner_clients`
--
ALTER TABLE `blog_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `blog_contact_details`
--
ALTER TABLE `blog_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_content`
--
ALTER TABLE `blog_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `blog_content_types`
--
ALTER TABLE `blog_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000;

--
-- AUTO_INCREMENT de tabela `blog_extensions`
--
ALTER TABLE `blog_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10007;

--
-- AUTO_INCREMENT de tabela `blog_fields`
--
ALTER TABLE `blog_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_fields_groups`
--
ALTER TABLE `blog_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_finder_filters`
--
ALTER TABLE `blog_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_finder_links`
--
ALTER TABLE `blog_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `blog_finder_taxonomy`
--
ALTER TABLE `blog_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `blog_finder_terms`
--
ALTER TABLE `blog_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1261;

--
-- AUTO_INCREMENT de tabela `blog_finder_types`
--
ALTER TABLE `blog_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `blog_languages`
--
ALTER TABLE `blog_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `blog_menu`
--
ALTER TABLE `blog_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT de tabela `blog_menu_types`
--
ALTER TABLE `blog_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `blog_messages`
--
ALTER TABLE `blog_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_modules`
--
ALTER TABLE `blog_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de tabela `blog_newsfeeds`
--
ALTER TABLE `blog_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_overrider`
--
ALTER TABLE `blog_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT de tabela `blog_postinstall_messages`
--
ALTER TABLE `blog_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `blog_privacy_consents`
--
ALTER TABLE `blog_privacy_consents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_privacy_requests`
--
ALTER TABLE `blog_privacy_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_redirect_links`
--
ALTER TABLE `blog_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `blog_template_styles`
--
ALTER TABLE `blog_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `blog_ucm_content`
--
ALTER TABLE `blog_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_ucm_history`
--
ALTER TABLE `blog_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `blog_updates`
--
ALTER TABLE `blog_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT de tabela `blog_update_sites`
--
ALTER TABLE `blog_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `blog_usergroups`
--
ALTER TABLE `blog_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `blog_users`
--
ALTER TABLE `blog_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=933;

--
-- AUTO_INCREMENT de tabela `blog_user_keys`
--
ALTER TABLE `blog_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_user_notes`
--
ALTER TABLE `blog_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `blog_viewlevels`
--
ALTER TABLE `blog_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
